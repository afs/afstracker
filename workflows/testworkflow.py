#!/usr/bin/python3
import unittest


import site
import os
import tempfile
import subprocess
import sqlite3
import datetime
from afsutil.volutil import Volutil

class TestWorkflow(unittest.TestCase):
    dbfile = None

    def setUp(self):
        site.addsitedir('..')
        os.environ['PYTHONPATH'] = '..'
        if not TestWorkflow.dbfile or not os.path.exists(TestWorkflow.dbfile):
            _, TestWorkflow.dbfile = tempfile.mkstemp(suffix='.sqlite', prefix='afstrack_workflow_')
            os.environ['DATABASE_URL'] = 'sqlite:///'+ TestWorkflow.dbfile
            from afstracker import db
            db.create_all()
            print("DEBUG: needed to recreate DB {}".format(TestWorkflow.dbfile))

        self.volnames = 's.afs.test'
        self.user = os.environ.get('USER')
        afs_homedir = '/afs/cern.ch/user/'+self.user[0:1]+'/'+self.user
        if os.path.isdir(afs_homedir):
            self.volmountdir = afs_homedir+'/tmp/test_afstrack_workflows'
        else:
            self.volmountdir = '/afs/cern.ch/project/afs/var/tmp/test_afstrack_workflows'
        try:
            os.mkdir(self.volmountdir)
        except OSError:
            pass
        self.workflow = None
        self.project = 'testproject'
        self.conn = sqlite3.connect(TestWorkflow.dbfile)

    def check_volume_is_present(self, vol):
        try:
            if vol in subprocess.check_output(['vos', 'exa', vol],
                                              stderr=subprocess.STDOUT,
                                              universal_newlines=True):
                return True
        except subprocess.CalledProcessError:
            return False

    def check_mountpoint_is_present(self, vol, mnt=None):
        if not mnt:
            mnt = self.get_testdir(vol)
        try:
            if mnt in subprocess.check_output(['fs', 'lsmount', mnt],
                                              stderr=subprocess.STDOUT,
                                              universal_newlines=True):
                return True
        except subprocess.CalledProcessError:
            pass
        return False

    def volcreate_if_missing(self, vol):
        if not self.check_volume_is_present(vol=vol):
            Volutil.create_volume(vol)

    def voldelete_if_present(self, vol):
        if self.check_volume_is_present(vol=vol):
            Volutil.delete_volume(vol)

    def check_mountperms(self, mnt):
        try:
            if ' '+self.user+' rlidwka' in subprocess.check_output(['fs', 'listacl', '-path', mnt],
                                                                   stderr=subprocess.STDOUT,
                                                                   universal_newlines=True):
                return True
        except subprocess.CalledProcessError as e:
            if not 'have the required access' in e.output:
                raise
        return False

    def flush_mount(self, mnt):
        Volutil.flush_mount(mnt)

    def flush_all(self):
        Volutil.flush_all()

    def create_mount(self, mnt, vol):
        Volutil.create_mount(dirname=mnt, volname=vol, local=True)

    def volmount_if_missing(self, vol, mnt=None):
        if not mnt:
            mnt = self.get_testdir(vol)
        if not self.check_mountpoint_is_present(vol=vol, mnt=mnt):
            Volutil.create_mount(dirname=mnt, volname=vol, local=True)
            ## local since afs_admin insists on project area mounts, we need homedir
            self.flush_mount(mnt=mnt)

    def volrmmount_if_present(self, vol, mnt=None):
        if not mnt:
            mnt = self.get_testdir(vol)
        if self.check_mountpoint_is_present(vol=vol, mnt=mnt):
            Volutil.delete_mount(dirname=mnt, local=False)
            try:
                # workaround for slow caches
                Volutil.delete_mount(dirname=mnt, local=True)
            except subprocess.CalledProcessError:
                pass

    def volaccess_if_missing(self, vol, mnt=None):
        if not mnt:
            mnt = self.get_testdir(vol)
        if not self.check_mountperms(mnt=mnt):
            self.flush_mount(mnt=mnt)
            Volutil.set_acl(mnt, self.user, 'rlidwka')

    def add_vol_to_db(self,
                      vol,
                      mountpoint=None,
                      workflow=None,
                      params='unset_replaceme',
                      activehost=None,
                      nextdate=None,
                      destname=None,
                      sourcevol=None,
	              owner='unset_replaceme'):
        if not workflow:
            workflow = self.workflow
        if not nextdate:
            nextdate = "'"+str(datetime.datetime.now())+"'"
        if not mountpoint:
            mountpoint=self.get_testdir(vol)
        if params == 'unset_replaceme':  # since 'None' is valid
            params=self.project
        if owner == 'unset_replaceme':  # since 'None' is valid
            owner=self.user
        curs = self.conn.cursor()
        curs.execute("""
    INSERT OR IGNORE INTO
    volume_state (volname,
        workflow_name,
        workflow_params,
        pathname,
        destname,
        workflow_state,
        workflow_activehost,
        owner,
        workflow_defaultnextsec)
        VALUES (?,?,?,?,?,?,?,?,?)""",
                     [vol,
                      workflow,
                      params,
                      mountpoint,
                      destname,
                      'initial',
                      activehost,
                      owner,
                      0])
        curs.execute("""
        UPDATE volume_state SET workflow_next = {} WHERE volname = '{}'""".format(
            str(nextdate), vol))

        self.conn.commit()
        curs.close()

    def check_vol_is_in_db(self, vol, state=None):
        curs = self.conn.cursor()
        found = False
        if state:
            curs.execute("SELECT volname FROM volume_state WHERE volname = ? AND workflow_state LIKE ?", [vol, state+'%'])
        else:
            curs.execute("SELECT volname FROM volume_state WHERE volname = ?", [vol])
        if curs.fetchone():
            found = True
        curs.close()
        return found

    def delete_vol_from_db_maybe(self, vol):
        curs = self.conn.cursor()
        curs.execute("DELETE FROM volume_state WHERE volname = ?", [vol])
        curs.close()

    def dump_db(self):
        curs = self.conn.cursor()
        curs.execute("SELECT * FROM volume_state")
        for row in curs:
            print('|'.join(str(x) for x in row))
        curs.close()

    def get_testdir(self, vol):
        return self.volmountdir+'/'+vol

    def get_testfile(self, vol):
        return self.get_testdir(vol)+'/testfile.txt'

    def create_testfile_if_missing(self, vol, fname=None, text='default content'):
        if not fname:
            fname = self.get_testfile(vol)
        if not os.path.exists(fname):
            with open(fname, 'w') as f:
                f.write(text)

    def read_testfile(self, vol, fname=None):
        if not fname:
            fname = self.get_testfile(vol)
        with open(fname, 'r') as f:
            return f.read()

    def chmod_testfile(self, vol, mode, fname=None):
        if not fname:
            fname = self.get_testfile(vol)
        os.chmod(fname, mode)

    def cleanup_testfile(self, vol, fname=None):
        if not fname:
            fname = self.get_testfile(vol)
        try: 
            os.unlink(fname)
        except OSError:
            pass

    def cleanup_datacopy_if_present(self, vol, mnt=None):
        if not mnt:
            mnt = self.get_testdir(vol)
        if os.path.isdir(mnt):
            try: 
                subprocess.check_call(['rm', '-r', mnt],
                                      stderr=subprocess.STDOUT)
            except subprocess.CalledProcessError:
                pass

    class Args(object):
        def __init__(self):
            self.debug = False
            self.waitsec = None
            self.noaction = None
            self.nowaitloop = None
            self.syslog = None
            self.noadmin = None

