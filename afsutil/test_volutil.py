#!/usr/bin/python3

import unittest
import os
import subprocess
import time
from .volutil import Volutil

testmountdir= '/afs/cern.ch/user/i/iven/tmp/s.afs.testemptyhome'
testmountvol='s.afs.testemptyhome'

class TestVolutil(unittest.TestCase):
  @classmethod
  def setUpClass(cls):
    try:
      subprocess.check_call(['afs_admin', 'help'], stdout=subprocess.DEVNULL)
    except FileNotFoundError:
      raise unittest.SkipTest('these tests needs the "afs_admin" command')
    try:
      subprocess.call(['volume_mount_scan.pl', '-h'], stdout=subprocess.DEVNULL) # exit code 1 also on help
    except FileNotFoundError:
      raise unittest.SkipTest('these tests needs the "volume_mount_scan.pl" command')

  def test_check_mount_existing(self):
    result = Volutil.check_mount(volname='user.iven',
                                         dirname='/afs/cern.ch/user/i/iven')
    self.assertTrue(result)

  def test_check_mount_nosuchdir(self):
    result = Volutil.check_mount(volname='user.iven',
                                         dirname='/afs/cern.ch/NOuser/i/iven')
    self.assertFalse(result)

  def test_check_mount_notamount(self):
    result = Volutil.check_mount(volname='user.iven',
                                         dirname='/afs/cern.ch/user/i')
    self.assertFalse(result)

  def test_check_mount_nonmatchingmount(self):
    result = Volutil.check_mount(volname='user.jan',
                                         dirname='/afs/cern.ch/user/i/iven')
    self.assertFalse(result)

  def test_create_mount_replicate_unmount_delete_volume(self):
    testvol='s.afs.testtest'
    mountdir='/afs/cern.ch/project/afs/var/tmpmounts/s.afs.testtest'
    try:
      Volutil.delete_volume(volname=testvol)
      Volutil.delete_mount(dirname=mountdir)
    except Exception:
      pass
    Volutil.create_volume(volname=testvol)
    Volutil.create_mount(dirname=mountdir, volname=testvol)
    renamedir=mountdir+'_rename'
    Volutil.create_replica(volname=testvol)
    Volutil.rename_mount(olddirname=mountdir, newdirname=renamedir)
    Volutil.delete_mount(dirname=renamedir)
    Volutil.delete_volume(volname=testvol)

  def test_local_create_mount_unmount_delete_volume(self):
    testvol='s.afs.testtest'
    mountdir='/afs/cern.ch/project/afs/var/tmpmounts/s.afs.testtest'
    try:
      Volutil.delete_volume(volname=testvol)
      Volutil.delete_mount(dirname=mountdir)
    except Exception:
      pass
    Volutil.create_volume(volname=testvol)
    Volutil.create_mount(dirname=mountdir, volname=testvol, local=True)
    ## cannot use afs_admin here..
    Volutil.delete_mount(dirname=mountdir, local=True)
    Volutil.delete_volume(volname=testvol)

  def test_running_as_admin(self):
    result = Volutil.running_as_admin()
    self.assertFalse(result)

  def mountcheck_testmount(self):
    try:
        mtime = os.path.getmtime(testmountdir)
        if time.time() - mtime < 86400:
            self.skipTest('testmount is too recent to show in volume_mount_scan')
    except OSError:
        self.skipTest('testmount is missing')

  def test_get_mounts_under_dir(self):
    self.mountcheck_testmount()
    result = Volutil.get_mounts_under_dir(dirname='/afs/cern.ch/user/i/iven')
    self.assertTrue(testmountdir in result)

  def test_get_volumes_under_dir(self):
    self.mountcheck_testmount()
    result = Volutil.get_volumes_under_dir(dirname='/afs/cern.ch/user/i/iven')
    self.assertTrue(testmountvol in result)

  def test_get_volumes_mounts_under_dir(self):
    self.mountcheck_testmount()
    result = Volutil.get_volumes_or_mounts_under_dir(dirname='/afs/cern.ch/user/i/iven', want_volumes=True, want_mounts=True)
    self.assertTrue([testmountvol, testmountdir] in result)

if __name__ == '__main__':
    unittest.main()
