#!/usr/bin/python3

import site
site.addsitedir('..')

from datetime import datetime, timedelta
import afstracker.models as am
from argparse import ArgumentParser
import sys

workflow_name='missing_volumes'
workflow_state='initial'
owner='afsmigr'

if __name__ == "__main__":
    parser = ArgumentParser(usage="add a dangling mountpoint for automatic cleanup")
    parser.add_argument("--debug",
                        help="more verbose output",
                        action='store_true',
                    )
    parser.add_argument("--waitsecs",
                        type=int,
                        default=60,
                        help="how long [seconds] to wait between actions for a single volume",
                    )
    parser.add_argument("--volname",
                        required=True,  # mostly for logging and such
                        help="volume name",
                    )
    parser.add_argument("--mountpoint",
                        required=True,
                        help="the dangling mountpoint",
                    )
    args = parser.parse_args()

    if not args.mountpoint.startswith('/afs/cern.ch/'):
        print("Error: mountpoint does not start with '/afs/cern.ch/'")
        sys.exit(1)

    now = datetime.now()

    next = now + timedelta(seconds=int(args.waitsecs))

    dbvol = am.VolumeState(volname=args.volname,
                           workflow_name=workflow_name,
                           workflow_params = None,
                           workflow_state = workflow_state,
                           workflow_since = now,
                           workflow_next = next,
                           pathname = args.mountpoint,
                           owner = owner,
                       )
    am.db.session.add(dbvol)
    am.db.session.commit()
