#!/usr/bin/python3
# pylint: disable=C0301
# copy a AFS subtree to an new AFS volume
# or: replace a symlink with an AFS volume, and the orginal directory with a symlink
##
import sys
import os
import socket
from datetime import datetime, timedelta
import time
import random
import subprocess

from afsutil.volutil import Volutil

default_wait = timedelta(days=30)
max_sleep = 5*60

this_workflow = 'tree2vol'
this_host = socket.gethostname()
this_process = os.getpid()
soft_exit_file = '/var/run/'+this_workflow+'.soft_exit'

tmpmounts = '/afs/cern.ch/project/afs/var/tmpmounts'

class WarnNothingToDo(Exception):
    pass

class ErrorPrecheck(Exception):
    pass

def soft_exit_perhaps(log):
    if os.path.exists(soft_exit_file):
        log.info("soft exit for workflow processor {}".format(this_workflow))
        try:
            os.remove(soft_exit_file)
        except OSError:
            pass
        sys.exit(33)  # error, so that systemd restarts us

def run_periodic(log, args, am):
    cur_sleep = 0
    log.info("workflow processor for {} started".format(this_workflow))
    while True:
        soft_exit_perhaps(log)
        now = datetime.now()
        am.db.session.commit()  # to get fresh objects
        volstate = am.VolumeState.query.filter(am.VolumeState.workflow_name == this_workflow,
                                               am.VolumeState.workflow_activehost.is_(None),
                                               am.VolumeState.workflow_next <= now).first()
        if not volstate:
            if args.nowaitloop:
                return

            cur_sleep = min(max_sleep, cur_sleep * 2) + random.randint(0, 5)
            log.debug('no data found, sleeping for {}secs'.format(cur_sleep))
            time.sleep(cur_sleep)
            continue

        cur_sleep = 0

        # lock the record for us. Need to push to DB, and check for races
        identifier = ':'.join([this_host, str(this_process)])
        volname = volstate.volname # might be gone afterwards..
        volstate.workflow_activehost = identifier
        am.db.session.commit()
        volstate = am.VolumeState.query.filter(am.VolumeState.workflow_name == this_workflow,
                                               am.VolumeState.volname == volname,
                                               am.VolumeState.workflow_activehost == identifier
                                              ).first()
        if not volstate:
            log.warn("could not lock record for {}".format(volname))
            continue
        log.debug("record for {} now locked by {}.".format(volstate.volname, identifier))

        project = volstate.workflow_params
        log.info("looking at volume '{}' from project '{}' in state '{}'".format(volstate.volname,
                                                                                 project,
                                                                                 volstate.workflow_state))

        # see whether there are ongoing workflows for our source volume
        # if so, push back and wait (for a few hours)
        volstate_other = am.VolumeState.query.filter(am.VolumeState.workflow_name == this_workflow,
                                                     am.VolumeState.volname != volstate.volname,
                                                     am.VolumeState.sourcevol.is_not(None),
                                                     am.VolumeState.sourcevol == volstate.sourcevol,
                                                     am.VolumeState.workflow_activehost.is_not(None)
                                                    ).first()
        if volstate_other:
            log.info("'{}' is blocked by '{}' running from same '{}', on '{}', will wait'".format(volstate.volname,
                                                                                                  volstate_other.volname,
                                                                                                  volstate_other.sourcevol,
                                                                                                  volstate_other.workflow_activehost))
            volstate.workflow_next = now + timedelta(seconds=int(args.waitblocksecs))
            volstate.workflow_activehost = None
            continue

        if volstate.workflow_state == 'initial':
            if volstate.destname:
                log.debug("{} will be copied from '{}' as new volume '{}' and symlinked to '{}'.".format(volstate.pathname,
                                                                                                         volstate.sourcevol,
                                                                                                         volstate.volname,
                                                                                                         volstate.destname))
            else:
                log.debug("{} will be copied from '{}' as new volume '{}'.".format(volstate.pathname,
                                                                                   volstate.sourcevol,
                                                                                   volstate.volname))

            # Here is all the real action
            action_split_tree_to_volume(log=log, volstate=volstate)
            # goes to ERROR or 'copy_done'
        elif volstate.workflow_state == 'copy_done':
            log.info("copy from {} into {} done. Over.".format(volstate.pathname,
                                                               volstate.volname))
            am.db.session.delete(volstate)
            continue
        else:
            assert(False), "unexpected workflow state {} for volume {}".format(volstate.workflow_state,
                                                                               volstate.volname)


        # push back changed data for this volume to DB
        am.db.session.commit()


#################################################

# main function
def action_split_tree_to_volume(log, volstate):
    #project = volstate.workflow_params
    afsroot = str(volstate.pathname) # mark as non-unicode, we might append random junk later
    afssymlink = None
    if volstate.destname is not None:
        afssymlink = str(volstate.destname)
    step = 'initializing'
    out = ''

    try:
        step = 'checking AFS sourcedir'
        if not afsroot.startswith('/afs'):
            raise ErrorPrecheck('source directory {} ({}) does not start with /afs'.format(afsroot,
                                                                                           volstate.volname))
        if not os.path.exists(afsroot):
            raise ErrorPrecheck('AFS source directory {} ({}) does not exist'.format(afsroot,
                                                                                     volstate.volname))
        if not os.path.isdir(afsroot):
            raise ErrorPrecheck('AFS source {} ({}) is not a directory'.format(afsroot,
                                                                               volstate.volname))
        if os.path.islink(afsroot):
            raise ErrorPrecheck('AFS source {} ({}) is a symlink'.format(afsroot,
                                                                         volstate.volname))

        have_already_mountpoint = False
        try:
            fslsm_cmd = ['fs', 'lsmount', afsroot]
            out = subprocess.check_output(fslsm_cmd,
                                          stderr=subprocess.STDOUT,
                                          universal_newlines=True)
            have_already_mountpoint = True
        except subprocess.CalledProcessError:
            pass
        if have_already_mountpoint:
            raise ErrorPrecheck('AFS source {} ({}) is already a mountpoint'.format(afsroot,
                                                                                    volstate.volname))

        if not os.listdir(afsroot):
            raise WarnNothingToDo('AFS source directory {} ({}) is empty, ignoring'.format(afsroot,
                                                                                           volstate.volname))

        step = 'checking AFS target volume'
        have_already_volume = False
        redo_mode = False  # should be an argument
        try:
            vosexa_cmd = ['vos', 'examine', volstate.volname]
            out = subprocess.check_output(vosexa_cmd,
                                          stderr=subprocess.STDOUT,
                                          universal_newlines=True)
            have_already_volume = True
        except subprocess.CalledProcessError:
            pass
        if have_already_volume and not redo_mode:
            raise ErrorPrecheck('target AFS volume {} already exists'.format(volstate.volname))

        step = 'checking AFS .old_toberemoved'
        toberemoved_dir = afsroot+'.old_toberemoved'
        if os.path.exists(toberemoved_dir):
            raise ErrorPrecheck('AFS rename directory {} ({}) already exists'.format(toberemoved_dir,
                                                                                     volstate.volname))
        # filter out those mountpoints that are in our source subtree.
        #   for now we just bomb out if there are any (don't want to copy these into new volume)
        # Could reverse sort (means subdirectories come before parent
        # directories) and use for rsync exclusion list (in file), then remount inside new volume.

        step = 'check for submounts'
        submounts = Volutil.get_volumes_or_mounts_under_dir(afsroot, want_mounts=True)
        if submounts:
            raise ErrorPrecheck('AFS tree {} ({}) contains submounts, cannot handle'.format(afsroot,
                                                                                            volstate.volname))
        # extra checks for the symlink2tree2vol case
        if afssymlink:
            step = 'checking symlink'
            if not afssymlink.startswith('/afs'):
                raise ErrorPrecheck('symlink {} ({}) does not start with /afs'.format(afssymlink,
                                                                                      volstate.volname))

            if not os.path.islink(afssymlink):
                raise ErrorPrecheck('AFS symlink {} ({}) is NOT a symlink'.format(afssymlink,
                                                                                  volstate.volname))

            step = 'checking LINK.old_toberemoved'
            toberemoved_link = afssymlink+'.old_toberemoved'
            if os.path.exists(toberemoved_link):
                raise ErrorPrecheck('AFS renamed link {} ({}) already exists'.format(toberemoved_link,
                                                                                     volstate.volname))

            step = 'collecting symlink chains'
            links_to_change = []
            this_link = afssymlink
            while os.path.islink(this_link):
                # prevent loops
                if len(links_to_change) > 10:
                    raise ErrorPrecheck('too many links below {} ({})'.format(afssymlink,
                                                                              volstate.volname))
                if this_link != afssymlink: # since that will become the mountpoint, not a link
                    log.debug("existing symlink '{}' will point to new volume '{}'.".format(this_link,
                                                                                            volstate.volname))
                links_to_change.append(this_link)
                target = os.readlink(this_link)
                if target.startswith('/'):
                    this_link = os.path.abspath(target)
                    continue
                this_dir = os.path.dirname(this_link)
                this_link = os.path.abspath(os.path.join(this_dir, target))

            step = 'checking found symlink target(s)'
            for this_link in links_to_change:
                if os.path.realpath(this_link) != afsroot:
                    raise ErrorPrecheck('AFS symlink {} ({}) does not point to {}'.format(this_link,
                                                                                          volstate.volname, afsroot))

        # all checks OK.
        step = 'get size of data'
        du_cmd = ['du', '-sk', '-BM', afsroot]
        out = subprocess.check_output(du_cmd, stderr=subprocess.STDOUT,
                                      universal_newlines=True)
        size_mb_old, _ = out.split('M', 1)
        size_mb = int(int(size_mb_old) * 130/100)
        log.info("old tree has size {}MB, new '{}' will have {}MB quota".format(size_mb_old,
                                                                                volstate.volname,
                                                                                size_mb))

        step = 'prepare new volume'
        Volutil.create_volume(volstate.volname)

        step = 'cleanup old tmp mountpoint'
        tmpmount = tmpmounts+'/'+volstate.volname
        try:
            Volutil.delete_mount(dirname=tmpmount,
                                 local=Volutil.running_as_admin())
            Volutil.flush_vol(tmpmounts)
        except subprocess.CalledProcessError:
            pass
        step = 'prepare new tmp mountpoint'
        Volutil.create_mount(dirname=tmpmount,
                             volname=volstate.volname,
                             local=Volutil.running_as_admin())
        Volutil.flush_vol(tmpmount)

        if Volutil.running_as_admin():
            step = 'set quota'
            fssq_cmd = ['fs', 'setquota',
                        '-path', tmpmount,
                        '-max', str(size_mb * 1024)]
            subprocess.check_call(fssq_cmd) # might have to wait & do via "afs_admin set_quota" - but vol creation propagation is slow
        else:
            log.warning("not admin: not setting target AFS volume quota on {}".format(volstate.volname))

        if not Volutil.running_as_admin(): # during tests: new volume has no ACLs
            step = 'set initial ACL'
            log.warning("({}) not admin: setting initial ACLs".format(volstate.volname))
            Volutil.set_acl(dirname=tmpmount, account=volstate.owner, acl='rlidwk')

        step = "copy top-level ACLs"
        fscopyacltop_cmd = ["fs listacl -cmd " + afsroot +" | "+
                            "sed -e 's/^fs setacl -dir .* -acl / /' | "+
                            "xargs afs_admin set_acl " + tmpmount]
        subprocess.check_call(fscopyacltop_cmd, shell=True)

        # actual data move
        step = 'copy data, first pass'
        rsync_opts = ['--recursive',
                      '--links', '--perms', '--times', '--whole-file',
                      '--update', '--quiet', '--numeric-ids',
                      '--one-file-system', '--stats']
        if Volutil.running_as_admin(): # no rights to change owner during tests
            rsync_opts += ['--super', '--group', '--owner', '--numeric-ids']
        else:
            log.warning("not admin: rsync will not preserve ownership")
        # could exclude submounts via file: rsync_cmd= .. --exclude-from=${my_mounts}"
        # this is (very) slow - we will ignore "rsync warning: some files vanished before they could be transferred"
        # which corresponds to dedicated exit code 24
        rsync_cmd = ['rsync'] + rsync_opts + [afsroot + '/', tmpmount + '/']
        rsync_ret = subprocess.run(rsync_cmd,
                                   stdout = subprocess.PIPE, # capture_output is too new, 3.7
                                   stderr = subprocess.STDOUT,
                                   universal_newlines = True)
        if rsync_ret.returncode not in [0, 24]:
            raise subprocess.CalledProcessError(rsync_ret.returncode, cmd=rsync_ret.args, output=rsync_ret.stdout)

        log.info("data copy from {} into {} (volume {}) done".format(afsroot, tmpmount, volstate.volname))

        step = 'copy ACLs'
        olddir = os.getcwd()
        os.chdir(tmpmount)
        findacl_cmd = ['find', '.', '-type', 'd', '-exec',
                       'fs', 'copyacl',
                       '-fromdir', afsroot+'/{}',
                       '-todir', './{}', '-clear', ';']
        subprocess.check_call(findacl_cmd)
        os.chdir(olddir)
        log.info("ACL copy from {} into {} (volume {}) done".format(afsroot, tmpmount, volstate.volname))

        # could re-create all mountpoints here?

        if not afssymlink:
            step = 'move away original dir'
            mv_cmd = ['mv', afsroot, toberemoved_dir]
            subprocess.check_call(mv_cmd)

            step = 'mount new volume in place'
            Volutil.create_mount(dirname=afsroot,
                                 volname=volstate.volname,
                                 local=Volutil.running_as_admin())
            Volutil.flush_vol(tmpmount)

            # re-run rsync, in case something changed in between
            step = 'copy data, second pass'
            rsync_cmd = ['rsync'] + rsync_opts + [toberemoved_dir + '/', afsroot + '/']
            out = subprocess.check_output(rsync_cmd, stderr=subprocess.STDOUT,
                                          universal_newlines=True)
            log.info("second data copy from {} into {} (volume {}) done".format(toberemoved_dir, afsroot, volstate.volname))
        else: # symlink2tree2vol
            step = 'move away original symlink'
            mv_cmd = ['mv', afssymlink, toberemoved_link]
            subprocess.check_call(mv_cmd)
            log.info("moved away original symlink {} (volume {})".format(afssymlink, volstate.volname))

            step = 'mount new volume in place of symlink'
            Volutil.create_mount(dirname=afssymlink,
                                 volname=volstate.volname,
                                 local=Volutil.running_as_admin())
            Volutil.flush_vol(afssymlink)
            log.info("mounted on {} (volume {})".format(afssymlink, volstate.volname))

            step = 'move away original directory'
            mv_cmd = ['mv', afsroot, toberemoved_dir]
            subprocess.check_call(mv_cmd)
            log.info("moved away original directory {} (volume {})".format(afsroot, volstate.volname))

            step = 'symlink new mountpoint to original directory (RO)'
            afssymlink_ro = afssymlink.replace('/afs/.cern.ch/', '/afs/cern.ch', 1)
            ln_cmd = ['ln', '-s', afssymlink_ro, afsroot]
            subprocess.check_call(ln_cmd)
            log.info("symlinked original dir {} to new mount {} (volume {})".format(afsroot, afssymlink_ro, volstate.volname))

            # re-run rsync, in case something changed in between. Again ignore "vanishing files" = error 24
            step = 'copy data, second pass, symlink'
            rsync_cmd = ['rsync'] + rsync_opts + [toberemoved_dir + '/', afssymlink + '/']
            rsync_ret = subprocess.run(rsync_cmd,
                                       stdout = subprocess.PIPE, # capture_output is too new, 3.7
                                       stderr = subprocess.STDOUT,
                                       universal_newlines = True)
            if rsync_ret.returncode not in [0, 24]:
                raise subprocess.CalledProcessError(rsync_ret.returncode, cmd=rsync_ret.args, output=rsync_ret.stdout)

            log.info("second data copy from {} into {} (volume {}) done".format(toberemoved_dir, afssymlink, volstate.volname))

        step = 'cleanup tmp mount'
        Volutil.delete_mount(dirname=tmpmount,
                             local=Volutil.running_as_admin())

        step = 'delete original dir'
        rm_cmd = ['rm', '-rf', toberemoved_dir]
        subprocess.check_call(rm_cmd)
        log.info("removed original dir {} (volume {})".format(toberemoved_dir, volstate.volname))

        if afssymlink:
            step = 'cleanup old symlink'
            rm_cmd = ['rm', toberemoved_link]
            subprocess.check_call(rm_cmd)
            log.info("removed original link {} (volume {})".format(toberemoved_link, volstate.volname))

            step = 'point symlink chains to new mountpoint'
            for this_link in links_to_change:
                if this_link == afssymlink:
                    continue
                tmp_link = this_link + '.new_toberenamed'
                ln_cmd = ['ln', '-s', afssymlink_ro, tmp_link]
                subprocess.check_call(ln_cmd)
                mvT_cmd = ['mv', '-T', tmp_link, this_link]  # os.rename: atomically overwrites old link
                subprocess.check_call(mvT_cmd)
                log.info("symlinked intermediate link {} to new mount {} (volume {})".format(this_link, afssymlink_ro, volstate.volname))

    except WarnNothingToDo as e:
        log.warn('warn while {} for {}: {}'.format(step, volstate.volname, e))
    except ErrorPrecheck as e:
        volstate.workflow_state = 'ERROR:' + volstate.workflow_state
        volstate.sourcevol = None   # to not block other sub-areas of the same source volume
        volstate.workflow_next = None
        log.error('error while {} for {}: {}'.format(step, volstate.volname, e))
        return
    except subprocess.CalledProcessError as e:
        volstate.workflow_state = 'ERROR:' + volstate.workflow_state
        volstate.sourcevol = None   # to not block other sub-areas of the same source volume
        volstate.workflow_next = None
        log.error('error while {} for {}: {}'.format(step, volstate.volname, e.output))
        return
    volstate.workflow_state = 'copy_done'
    #keep volstate.workflow_next, no need to delay
    volstate.workflow_activehost = None
    return



if __name__ == "__main__":
    usage = """Looks for work in the DB, copy subtree into new AFS volume"""
    import logging
    import logging.handlers
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    from argparse import ArgumentParser
    parser = ArgumentParser(usage=usage)
    parser.add_argument("--debug",
                        help="more verbose output",
                        action='store_true')
    parser.add_argument("--syslog",
                        help="also log to syslog",
                        action='store_true')
    parser.add_argument("--nowaitloop",
                        help="if set, do not wait for new data to arrive - just exit after processing all",
                        action='store_true')
    parser.add_argument("--waitsecs",
                        type=int,
                        default=-1,
                        help="how long [seconds] to wait between actions")
    parser.add_argument("--waitblocksecs",
                        type=int,
                        default=600,
                        help="how long [seconds] to wait when being blocked by another task")
    args = parser.parse_args()
    if args.debug:
        console = logging.StreamHandler(sys.stdout)
        console.setLevel(logging.DEBUG)
        logger.addHandler(console)
    else:
        import dblogger
        dblog = dblogger.DBHandler()
        dblog.setLevel(logging.INFO)
        logger.addHandler(dblog)

    if args.syslog:
        syslog = logging.handlers.SysLogHandler(address='/dev/log',
                                                facility=logging.handlers.SysLogHandler.LOG_DAEMON)
        syslog.setLevel(logging.INFO)
        logger.addHandler(syslog)

    import afstracker.models as am
    run_periodic(logger, args, am)
