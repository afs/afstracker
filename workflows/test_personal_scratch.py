#!/usr/bin/python3

import unittest
from testworkflow import TestWorkflow

class TestPersonalScratch(TestWorkflow):
    def setUp(self):
        TestWorkflow.setUp(self)
        self.workflow = 'personal_scratch'

    # main entry point
    def run_personal_scratch_periodic(self):
        import logging
        import sys
        logger = logging.getLogger()
        logger.setLevel(logging.DEBUG)
        logger.handlers = []  # remove leftovers from previous runs
        console = logging.StreamHandler(sys.stderr)
        console.setLevel(logging.DEBUG)
        logger.addHandler(console)

        args = TestPersonalScratch.Args()
        args.nowaitloop=True
        args.waitsecs = 0
        args.debug=True
        args.noadmin=True
        import personal_scratch as ps
        import afstracker.models as am

        ps.run_periodic(logger, args, am)


    def test_add_other_to_db(self):
        self.add_vol_to_db("test.leaveme.other.notmyworkflow", workflow = 'other_workflow')
        self.add_vol_to_db("test.leaveme.other.notmyhost", activehost = 'otherhost.cern.ch:1234')
        self.add_vol_to_db("test.leaveme.other.notyet", nextdate ="DATETIME('now','+7 days')")
        
    def test_nonexisting(self):
        testvol=self.volnames+'miss'
        self.add_vol_to_db(vol=testvol)

        self.run_personal_scratch_periodic()

        self.assertFalse(self.check_vol_is_in_db(vol=testvol))

    def test_exist_unmounted(self):
        testvol=self.volnames+'umnt'
        self.volcreate_if_missing(vol=testvol)
        self.add_vol_to_db(vol=testvol)

        self.run_personal_scratch_periodic()

        self.assertFalse(self.check_volume_is_present(vol=testvol))
        self.assertFalse(self.check_vol_is_in_db(vol=testvol))
  

    def test_mounted_empty(self):
        testvol=self.volnames+'empty'
        self.volcreate_if_missing(vol=testvol)
        self.volmount_if_missing(vol=testvol)
        self.add_vol_to_db(vol=testvol)

        self.run_personal_scratch_periodic()

        self.assertFalse(self.check_mountpoint_is_present(vol=testvol))
        self.assertFalse(self.check_volume_is_present(vol=testvol))
        self.assertFalse(self.check_vol_is_in_db(vol=testvol))

    def test_mounted_data(self):
        testvol=self.volnames+'data'
        if not self.check_mountpoint_is_present(vol=testvol):
            self.cleanup_datacopy_if_present(vol=testvol)
        self.volcreate_if_missing(vol=testvol)
        self.volmount_if_missing(vol=testvol)
        self.volaccess_if_missing(vol=testvol)
        content = 'Really Important content'
        self.create_testfile_if_missing(vol=testvol, text=content)

        self.assertEqual(self.read_testfile(vol=testvol), content)  # pre-check, above seems to fail on afsarc?

        self.add_vol_to_db(vol=testvol)
        self.run_personal_scratch_periodic()

        self.assertEqual(self.read_testfile(vol=testvol), content)
        self.assertFalse(self.check_mountpoint_is_present(vol=testvol))
        self.assertFalse(self.check_volume_is_present(vol=testvol))
        self.assertFalse(self.check_vol_is_in_db(vol=testvol))
        self.cleanup_testfile(vol=testvol)

    @unittest.skip("need to run mountscan - else 'find -perm 0' will bomb out with 'resource deadlock avoided'")
    def test_mounted_data_withmountpoint(self):
        testvol=self.volnames+'loop'
        if not self.check_mountpoint_is_present(vol=testvol):
            self.cleanup_datacopy_if_present(vol=testvol)
        self.volcreate_if_missing(vol=testvol)
        self.volmount_if_missing(vol=testvol)
        self.volaccess_if_missing(vol=testvol)
        content = 'Not Important content'
        self.create_testfile_if_missing(vol=testvol, text=content)
        self.volmount_if_missing(vol=testvol,
                                 mnt=self.volmountdir+'/'+testvol+'/loopmount')
        # the mountscan would need to run so that we can skip this..
        # else might go for infinite 'rsync'?

        self.add_vol_to_db(vol=testvol)

        self.run_personal_scratch_periodic()

        self.assertEqual(self.read_testfile(vol=testvol), content)
        self.assertFalse(self.check_mountpoint_is_present(vol=testvol))
        self.assertFalse(self.check_volume_is_present(vol=testvol))
        self.assertFalse(self.check_vol_is_in_db(vol=testvol))
        self.cleanup_testfile(vol=testvol)

    def test_mounted_data_rsyncfailfix(self):
        testvol=self.volnames+'noread'
        if not self.check_mountpoint_is_present(vol=testvol):
            self.cleanup_datacopy_if_present(vol=testvol)
        self.volcreate_if_missing(vol=testvol)
        self.volmount_if_missing(vol=testvol)
        self.volaccess_if_missing(vol=testvol)
        content = 'Really Important but unreadable content. rsync will fail here'
        self.create_testfile_if_missing(vol=testvol, text=content)
        self.chmod_testfile(vol=testvol, mode=0000)
        self.add_vol_to_db(vol=testvol)

        self.run_personal_scratch_periodic()

        self.assertEqual(self.read_testfile(vol=testvol), content)
        self.assertFalse(self.check_mountpoint_is_present(vol=testvol))
        self.assertFalse(self.check_volume_is_present(vol=testvol))
        self.assertFalse(self.check_vol_is_in_db(vol=testvol))


        self.cleanup_testfile(vol=testvol)

    def test_check_other_in_db(self):
        self.assertTrue(self.check_vol_is_in_db(vol="test.leaveme.other.notmyworkflow"))
        self.assertTrue(self.check_vol_is_in_db(vol="test.leaveme.other.notmyhost"))
        self.assertTrue(self.check_vol_is_in_db(vol="test.leaveme.other.notyet"))





if __name__ == '__main__':
    unittest.main()
