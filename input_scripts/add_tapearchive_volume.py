#!/usr/bin/python3

import site
site.addsitedir('..')

from datetime import datetime, timedelta
import afstracker.models as am
from argparse import ArgumentParser
import sys
import re

workflow_name='tape_archive'
workflow_state='initial'

initial_wait = 60
if __name__ == "__main__":
    parser = ArgumentParser(usage="add a volume to be archived on CTA, and then removed")
    parser.add_argument("--debug",
                        help="more verbose output",
                        action='store_true',
                    )
    parser.add_argument("--waitsecs",
                        type=int,
                        help="how long [seconds] to wait between actions for a single volume",
                    )
    group = parser.add_mutually_exclusive_group()

    group.add_argument("--volscan",
                       required=False,
                       action='store_true',
                       help="""take STDIN input in 'volume_mount_scan.pl' format:
VOLNAME (VOLID) -> MOUNTPOINT
""",
                    )
    group.add_argument("--mountpoint",
                        required=False,
                        help="the volume mountpoint",
                    )

    parser.add_argument("--volname",
                        required=False,
                        help="to-be-archived volume name",
                    )
    parser.add_argument("--owner",
                        help="service account that will own the data on CTA. Must be in 'def-cg'. default is 'archive4PROJECT'",
                    )
    parser.add_argument("--project",
                        required=True,
                        help="AFS project that had allocated this space",
                    )
    args = parser.parse_args()

    now = datetime.now()

    next = now + timedelta(seconds=int(initial_wait))

    if not args.owner:
        args.owner = 'archive4'+args.project

    if args.owner.lower() != args.owner:
        print("Error: owner needs to be lowercase, otherwise will not match LDAP")
        sys.exit(1)

    from afsutil.phonebook import phonebook as Phonebook
    gid = Phonebook.get_gid(args.owner)
    if gid != 2766:   # 'def-cg'
        # cannot run nschown otherwise
        print("Error: owner needs to exist and be in group 'def-cg' (2766)")
        sys.exit(1)

    if args.volscan:
        for line in sys.stdin:
            m = re.search('^(\S+) \((\d+)\) -> (/afs.*)', line)
            if not m:
                print("WARN: skipping {}".format(line))
                continue
            line_volname = m.group(1)
            line_volid = m.group(2)  # not used here, but prevents matchin #REMOTE# #MISSING#
            line_mountpoint = m.group(3)

            volstate = am.VolumeState.query.filter(am.VolumeState.volname == line_volname).first()
            if volstate:
                print("WARN: skipping, volume {} already in DB".format(line_volname))
                continue

            dbvol = am.VolumeState(volname = line_volname,
                                   pathname = line_mountpoint,
                                   workflow_name = workflow_name,
                                   workflow_state = workflow_state,
                                   workflow_since = now,
                                   workflow_next = next,
                                   workflow_params = args.project,
                                   owner = args.owner,
                                   workflow_defaultnextsec = args.waitsecs,
            )
            am.db.session.add(dbvol)
        am.db.session.commit()
    else:
        if not ( args.owner and args.mountpoint and args.volname):
            print("Error: need owner, mountpoint and volume name, unless reading from stdin")
            sys.exit(1)
        if not args.mountpoint.startswith('/afs/cern.ch/'):
            print("Error: mountpoint does not start with '/afs/cern.ch/'")
            sys.exit(1)

        volstate = am.VolumeState.query.filter(am.VolumeState.volname == args.volname).first()
        if volstate:
            print("Error: skipping, volume {} already in DB".format(line_volname))
            sys.exit(1)
        dbvol = am.VolumeState(volname=args.volname,
                               workflow_name = workflow_name,
                               workflow_state = workflow_state,
                               workflow_since = now,
                               workflow_next = next,
                               workflow_defaultnextsec = args.waitsecs,
                               pathname = args.mountpoint,
                               workflow_params = args.project,
                               owner = args.owner,
        )
        am.db.session.add(dbvol)
        am.db.session.commit()
