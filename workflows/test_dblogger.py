#!/usr/bin/python3

import unittest
import logging
import tempfile
import os
import sqlite3

import dblogger

class TestDBlogger(unittest.TestCase):
    dbfile = None
    def setUp(self):
        if not TestDBlogger.dbfile or not os.path.exists(TestDBlogger.dbfile):
            _, TestDBlogger.dbfile = tempfile.mkstemp(suffix='.sqlite', prefix='afstrack_workflow_')
            os.environ['DATABASE_URL'] = 'sqlite:///'+ TestDBlogger.dbfile
            from afstracker import db
            db.create_all()
            print("DEBUG: needed to recreate DB {}".format(TestDBlogger.dbfile))
        self.conn = sqlite3.connect(TestDBlogger.dbfile)

    def check_message_is_in_db(self, message):
        curs = self.conn.cursor()
        curs.execute("SELECT message FROM action_log WHERE message = ?", [message])
        result = False
        db_mess = curs.fetchone()
        if db_mess:
          if db_mess[0] == message:
            result = True
        curs.close()
        return result

    def check_volname_message_is_in_db(self, volname, message):
        curs = self.conn.cursor()
        curs.execute("SELECT volname, message FROM action_log WHERE volname = ? AND message = ?", [volname, message])
        result = False
        db_mess = curs.fetchone()
        if db_mess:
          if db_mess[0] == volname and db_mess[1] == message :
            result = True
        curs.close()
        return result

    def dump_db(self):
        curs = self.conn.cursor()
        curs.execute("SELECT * FROM action_log")
        print(curs.fetchall())
        curs.close()

    ################################################
    def test_init(self):
      dbh = dblogger.DBHandler()
      self.assertIsNotNone(dbh.log_dbsession)
      self.assertIsNotNone(dbh.activehost)

    def test_setvol(self):
      dbh = dblogger.DBHandler()
      volname = 'test.nosuchvolume'
      dbh.set_activevolume(volname)
      self.assertEqual(dbh.volname, volname)

    def test_log(self):
      logger = logging.getLogger(__name__)
      logger.setLevel(logging.DEBUG)
      logger.handlers = []
      dbh = dblogger.DBHandler()
      dbh.setLevel(logging.INFO)
      logger.addHandler(dbh)

      test_message="FuNnY'test entry\"; DROP TABLE little bobby"
      logger.info(test_message)
      dbh.log_dbsession.commit()   # flush

      self.assertTrue(self.check_message_is_in_db(message=test_message))

    def test_log_dbhvolume(self):
      logger = logging.getLogger(__name__)
      logger.setLevel(logging.DEBUG)
      logger.handlers = []
      dbh = dblogger.DBHandler()
      dbh.setLevel(logging.INFO)
      logger.addHandler(dbh)

      test_message="message with volume"
      test_volume="test_volname"
      dbh.set_activevolume(test_volume)

      logger.info(test_message)
      dbh.log_dbsession.commit()   # flush

      self.assertTrue(self.check_volname_message_is_in_db(volname=test_volume, message=test_message))

    def test_log_extradict_volume(self):
      logger = logging.getLogger(__name__)
      logger.setLevel(logging.DEBUG)
      logger.handlers = []
      dbh = dblogger.DBHandler()
      dbh.setLevel(logging.INFO)
      logger.addHandler(dbh)

      test_message="message with volume as arg"
      test_volume="test_volmess"
      test_defvolume="test_default_vol" # should not get this
      dbh.set_activevolume(test_defvolume)
      logger.info(test_message, extra={'volume': test_volume})
      dbh.log_dbsession.commit()   # flush

      self.assertTrue(self.check_volname_message_is_in_db(volname=test_volume, message=test_message))

  
if __name__ == '__main__':
    unittest.main()
