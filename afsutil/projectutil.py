#!/usr/bin/python3
#
import subprocess

class Projectutil(object):

    @classmethod
    def afsproject_to_contacts(cls, projectname):
        """ return the admin accounts for a given AFS project, or 'empty set' """
        contacts =[]
        cmd = ['pts', 'membership', '-nameorid', '_{}_'.format(projectname), '-expandgroups']
        out = subprocess.check_output(cmd,
                                      stderr=subprocess.STDOUT,
                                      universal_newlines=True)
        # no 'bad' return code on errors?
        for line in out.splitlines():
            if 'Permission denied' in line:
                raise subprocess.CalledProcessError(returncode=1, cmd=cmd, output=out)
            if 'User or group doesn' in line:
                break
            if line.startswith('Members of '):
                continue
            if line.startswith('Expanded Members of '):
                continue
            if line.startswith(' '):
                contacts.append(line.strip())
            else:
                assert(False), "unexpected output line '{}' from pts".format(line)

        return contacts
