FROM cern/cc7-base
MAINTAINER jan.iven@cern.ch

# rest uses PIP. In particular 'flask-sso' is not available as RPM
RUN yum install -y --debuglevel=0 --errorlevel=0 python3-pip

# We copy just the requirements.txt first to leverage Docker cache
COPY ./requirements.txt /app/requirements.txt
WORKDIR /app
# all python deps are in here
RUN pip3 install --disable-pip-version-check --requirement requirements.txt

COPY . /app
ENTRYPOINT [ "python3" ]
CMD [ "afstracker.py" ]
