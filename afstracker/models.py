## from https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-iv-database

from afstracker import db

class VolumeState(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    volname = db.Column(db.String(64), index=True, unique=True)
    workflow_name = db.Column(db.String(128))
    workflow_params = db.Column(db.String(256))
    workflow_state = db.Column(db.String(32))
    workflow_since = db.Column(db.DateTime(timezone=True))
    workflow_next = db.Column(db.DateTime(timezone=True), index=True)
    workflow_activehost = db.Column(db.String(32))
    pathname = db.Column(db.String(4096))
    owner = db.Column(db.String(32))
    workflow_defaultnextsec = db.Column(db.Integer)
    destname = db.Column(db.String(4096))
    sourcevol = db.Column(db.String(64))

    def __repr__(self):
        return '<Volume {} ({}) handled by {} on {}, in state {} since {} until {}>'.format(self.volname,
                                                                                            self.id,
                                                                                            self.workflow_name,
                                                                                            self.workflow_activehost,
                                                                                            self.workflow_state,
                                                                                            self.workflow_since,
                                                                                            self.workflow_next)

class ActionLog(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime(timezone=True), index=True)
    loglevel = db.Column(db.String(32))
    workflow_name = db.Column(db.String(128))
    workflow_activehost = db.Column(db.String(32))
    volname = db.Column(db.String(64), index=True)
    message = db.Column(db.String(4096))

if __name__ == '__main__':
    import pdb
    pdb.set_trace()
    db.create_all()

