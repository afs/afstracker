#!/usr/bin/python3

import unittest
import subprocess
from .phonebook import phonebook

class TestPhonebook(unittest.TestCase):
  @classmethod
  def setUpClass(cls):
    try:
      subprocess.call(['phonebook'], stdout=subprocess.DEVNULL) # exit code 1 even on --help
    except FileNotFoundError:
      raise unittest.SkipTest('these tests needs the "phonebook" command')

  def test_getexisting(self):
    first,last,email = phonebook.get_name_email('iven')
    self.assertEqual(first, 'Jan')

  def test_gettype_primary(self):
    t = phonebook.get_accounttype('iven')
    self.assertEqual(t,'Primary')	

  def test_gettype_secondary(self):
    t = phonebook.get_accounttype('jan')
    self.assertEqual(t,'Secondary')	

  def test_gettype_service(self):
    t = phonebook.get_accounttype('afsmigr')
    self.assertEqual(t,'Service')	

  def test_getnonexisting(self):
    first,last,email = phonebook.get_name_email('NoSuchUserEverIHope')
    self.assertIsNone(first)

  def test_isisabled_yes(self):
    t = phonebook.is_disabled('jiven')
    self.assertTrue(t)

  def test_isisabled_no(self):
    t = phonebook.is_disabled('iven')
    self.assertFalse(t)

  def test_accounts_to_emails_empty(self):
    t = phonebook.accounts_to_emails([])
    self.assertFalse(t)

  def test_accounts_to_emails_mix(self):
    t = phonebook.accounts_to_emails(['iven',  # valid, primary
                                      'NoSuchUserEverIHope',
                                      'jan',  # valid, service
                                      'NoSuchUserEverIHope2',
                                      'jiven' # secondary, disabled
                                    ])
    self.assertTrue('Jan.Iven@cern.ch' in t)
    self.assertTrue('jan@cern.ch' in t)
    self.assertTrue(len(t) == 2)

  def test_get_gid_exists(self):
    t = phonebook.get_gid('iven')
    self.assertTrue(t == 1028)

  def test_get_gid_notexists(self):
    t = phonebook.get_gid('NoSuchUserEverIHope')
    self.assertTrue(t is None)

if __name__ == '__main__':
    unittest.main()
