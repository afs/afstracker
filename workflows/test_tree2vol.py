#!/usr/bin/python3

import unittest
import os
from testworkflow import TestWorkflow

class TestTree2Vol(TestWorkflow):
    def setUp(self):
        TestWorkflow.setUp(self)
        self.workflow = 'tree2vol'

    # main entry point
    def run_tree2vol_periodic(self):
        import logging
        import sys
        logger = logging.getLogger()
        logger.setLevel(logging.DEBUG)
        logger.handlers = []  # remove leftovers from previous runs
        console = logging.StreamHandler(sys.stderr)
        console.setLevel(logging.DEBUG)
        logger.addHandler(console)

        args = TestTree2Vol.Args()
        args.nowaitloop=True
        args.waitsecs = 0
        args.waitblocksecs = 10
        args.debug=True
        args.noadmin=True
        import tree2vol as t2v
        import afstracker.models as am

        t2v.run_periodic(logger, args, am)


    def test_aaa_add_other_to_db(self):
        self.add_vol_to_db("test.leaveme.other.notmyworkflow", workflow = 'other_workflow')
        self.add_vol_to_db("test.leaveme.other.notmyhost", activehost = 'otherhost.cern.ch:1234')
        self.add_vol_to_db("test.leaveme.other.notyet", nextdate ="DATETIME('now','+7 days')")
        
    def test_nonexistingdir(self):
        # fail, dir must exist
        testvol=self.volnames+'nosuchdir'
        tree='/afs/cern.ch/project/afs/nosuchdir'
        self.add_vol_to_db(vol=testvol, mountpoint=tree)
        self.run_tree2vol_periodic()

        self.assertTrue(self.check_vol_is_in_db(vol=testvol, state='ERROR'))

    def test_existingvol(self):
        # fail, don't want to clobber existing volumes
        testvol=self.volnames+'present'
        tree='/afs/cern.ch/project/afs'
        self.volcreate_if_missing(vol=testvol)
        self.add_vol_to_db(vol=testvol, mountpoint=tree)
        self.run_tree2vol_periodic()

        self.assertTrue(self.check_vol_is_in_db(vol=testvol, state='ERROR')) 

    def test_empty(self):
        # silent fail, won't create volume for empty dirs
        testvol=self.volnames+'empty'
        tree=self.get_testdir(testvol)
        self.volrmmount_if_present(vol=testvol, mnt=tree)
        try:
            os.mkdir(tree)
        except OSError:
            pass
        
        self.add_vol_to_db(vol=testvol, mountpoint=tree)

        self.run_tree2vol_periodic()
        self.assertFalse(self.check_vol_is_in_db(vol=testvol))
        os.rmdir(tree)


    def test_data(self):
        # OK: we should get a new volume
        testvol=self.volnames+'new'
        tree='/afs/cern.ch/project/afs/var/tmp/test_tree2vol_'+testvol  # can use afs_admin in here
        self.volrmmount_if_present(vol=testvol, mnt=tree)
        self.voldelete_if_present(vol=testvol)
        self.flush_all() # as we keep getting stale mountpoints
        try:
            os.mkdir(tree)
        except OSError:
            pass
        content = 'Really Important content'
        self.create_testfile_if_missing(vol=testvol, fname=tree+'/testfile.txt', text=content)

        self.assertEqual(self.read_testfile(vol=testvol, fname=tree+'/testfile.txt'), content)  # pre-check, above seems to fail on afsarc?

        self.add_vol_to_db(vol=testvol, mountpoint=tree)
        self.run_tree2vol_periodic()

        self.assertEqual(self.read_testfile(vol=testvol,fname=tree+'/testfile.txt'), content)
        self.assertTrue(self.check_mountpoint_is_present(vol=testvol, mnt=tree))
        self.assertTrue(self.check_volume_is_present(vol=testvol))
        self.assertFalse(self.check_vol_is_in_db(vol=testvol))

        self.volrmmount_if_present(vol=testvol, mnt=tree)
        self.voldelete_if_present(vol=testvol)


    def test_data_symlink(self):
        # OK: we should get a new volume
        testvol=self.volnames+'newlink'
        tree='/afs/cern.ch/project/afs/var/tmp/test_tree2vol_'+testvol  # can use afs_admin in here
        syml='/afs/cern.ch/project/afs/var/tmp/test_symlink_tree2vol_'+testvol
        try:
            os.remove(syml)
        except OSError:
            pass
        self.volrmmount_if_present(vol=testvol, mnt=syml)
        try:
            os.remove(tree)
        except OSError:
            pass
        self.volrmmount_if_present(vol=testvol, mnt=tree)
        self.voldelete_if_present(vol=testvol)
        self.flush_all() # as we keep getting stale mountpoints
        try:
            os.mkdir(tree)
        except OSError:
            pass
        content = 'Really Important content'
        self.create_testfile_if_missing(vol=testvol, fname=tree+'/testfile.txt', text=content)

        self.assertEqual(self.read_testfile(vol=testvol, fname=tree+'/testfile.txt'), content)  # pre-check, above seems to fail on afsarc?

        os.symlink(tree, syml)

        self.add_vol_to_db(vol=testvol, mountpoint=tree, destname=syml)
        self.run_tree2vol_periodic()

        self.assertEqual(os.readlink(tree), syml)
        self.assertEqual(self.read_testfile(vol=testvol,fname=syml+'/testfile.txt'), content)
        self.assertTrue(self.check_mountpoint_is_present(vol=testvol, mnt=syml))
        self.assertTrue(self.check_volume_is_present(vol=testvol))
        self.assertFalse(self.check_vol_is_in_db(vol=testvol))

        self.volrmmount_if_present(vol=testvol, mnt=syml)
        self.voldelete_if_present(vol=testvol)
        self.cleanup_testfile(tree) # i.e the symlink


    def test_data_symlinkchain(self):
        # OK: we should get a new volume
        testvol=self.volnames+'newchain'
        syml_main='/afs/cern.ch/project/afs/var/tmp/test_symlinkchain_tree2vol_'+testvol+'_main'  # the main entry point (becomes the volume)
        syml_int='/afs/cern.ch/project/afs/var/tmp/test_symlinkchain_tree2vol_'+testvol+'_int'    # goes though some intermediate links
        syml_rel='/afs/cern.ch/project/afs/var/tmp/test_symlinkchain_tree2vol_'+testvol+'_rel'    # some of which will be relative
        tree='/afs/cern.ch/project/afs/var/tmp/test_tree2vol_'+testvol                            # to the actuall data directory
        # cleanup
        for l in [syml_main, syml_int, syml_rel, tree]:
            try:
                os.remove(l)
            except OSError:
                pass
            self.volrmmount_if_present(vol=testvol, mnt=l)
        self.voldelete_if_present(vol=testvol)
        self.flush_all() # as we keep getting stale mountpoints
        # prepare
        try:
            os.mkdir(tree)
        except OSError:
            pass
        content = 'Really Important content'
        self.create_testfile_if_missing(vol=testvol, fname=tree+'/testfile.txt', text=content)
        self.assertEqual(self.read_testfile(vol=testvol, fname=tree+'/testfile.txt'), content)  # pre-check, above seems to fail on afsarc?

        os.symlink(tree, syml_rel)
        os.symlink('../../var/tmp/test_symlinkchain_tree2vol_'+testvol+'_rel', syml_int)
        os.symlink(syml_int, syml_main)

        self.add_vol_to_db(vol=testvol, mountpoint=tree, destname=syml_main)
        self.run_tree2vol_periodic()

        self.assertEqual(os.readlink(tree), syml_main)
        self.assertEqual(os.readlink(syml_int), syml_main)
        self.assertEqual(os.readlink(syml_rel), syml_main)
        self.assertEqual(self.read_testfile(vol=testvol,fname=syml_main+'/testfile.txt'), content)
        self.assertTrue(self.check_mountpoint_is_present(vol=testvol, mnt=syml_main))
        self.assertTrue(self.check_volume_is_present(vol=testvol))
        self.assertFalse(self.check_vol_is_in_db(vol=testvol))

        #cleanup
        self.volrmmount_if_present(vol=testvol, mnt=syml_main)
        self.voldelete_if_present(vol=testvol)
        self.cleanup_testfile(tree) # i.e the symlink
        self.cleanup_testfile(syml_int)
        self.cleanup_testfile(syml_rel)


    def test_zzz_check_other_in_db(self):
        self.assertTrue(self.check_vol_is_in_db(vol="test.leaveme.other.notmyworkflow"))
        self.assertTrue(self.check_vol_is_in_db(vol="test.leaveme.other.notmyhost"))
        self.assertTrue(self.check_vol_is_in_db(vol="test.leaveme.other.notyet"))





if __name__ == '__main__':
    unittest.main()
