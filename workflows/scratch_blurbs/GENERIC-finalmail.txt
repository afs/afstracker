Dear {first} {last},

As announced in a previous mail, the personal scratch space you have received from the '{project}' project admins has now been incorporated into your personal AFS space, under {path}
The corresponding volume '{volume}' is now unmounted and will be deleted after {nextdate}.
{errors}
{remarks}
Regards,
  jan iven (AFS phaseout)
