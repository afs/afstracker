#!/usr/bin/python3
import unittest
from testworkflow import TestWorkflow

class TestTapeArchive(TestWorkflow):
    tapeserver = 'eosctapublicdisk'
    tapeprefix = '/eos/ctapublicdisk/archive/afsmigration/tests_cta'

    def setUp(self):
        TestWorkflow.setUp(self)
        self.workflow = 'tape_archive'

    def tape_deletefile(self, tape_file, ignore_miss=False):
        import subprocess
        cmd = ['xrdfs', self.tapeserver, 'rm',  self.tapeprefix + '/' + self.project + '/'+ tape_file]
        try:
            out = subprocess.check_output(cmd, stderr=subprocess.STDOUT,
                                          universal_newlines=True)
        except subprocess.CalledProcessError as e:
            if ignore_miss and 'No such file' in e.output:
                pass
            else:
                raise

    def tape_checkfile(self, tape_file):
        import subprocess
        import re
        cmd = ['xrdfs', self.tapeserver, 'stat',  self.tapeprefix + '/' + self.project + '/'+ tape_file]
        out = subprocess.check_output(cmd, stderr=subprocess.STDOUT,
                                      universal_newlines=True)
        m = re.search(r'Size:\s+(\d+)', out)
        if not m or int(m.group(1)) == 0:
            raise subprocess.CalledProcessError(returncode=1, cmd='xrdfs stat', output='CTA file does not exist or has zero size')

    def tape_deletedir(self, tape_dir):
        import subprocess
        cmd = ['xrdfs', self.tapeserver, 'rm', self.tapeprefix + '/' + self.project + '/'+ tape_dir]
        out = subprocess.check_output(cmd, stderr=subprocess.STDOUT,
                                      universal_newlines=True)


    # main entry point
    def run_workflow_periodic(self):
        import logging
        import sys
        logger = logging.getLogger()
        logger.setLevel(logging.DEBUG)
        logger.handlers = []  # remove leftovers from previous runs
        console = logging.StreamHandler(sys.stderr)
        console.setLevel(logging.DEBUG)
        logger.addHandler(console)

        args = TestTapeArchive.Args()
        args.nowaitloop = True
        args.waitsecs = 0
        args.debug = True
        args.noadmin = True
        args.tapeprefix = self.tapeprefix

        import tape_archive as workflow
        import afstracker.models as am

        workflow.run_periodic(logger, args, am)

    # keep me first
    def test_add_other_to_db(self):
        self.add_vol_to_db("test.leaveme.other.notmyworkflow", workflow = 'other_workflow')
        self.add_vol_to_db("test.leaveme.other.notmyhost", activehost = 'otherhost.cern.ch:1234')
        self.add_vol_to_db("test.leaveme.other.notyet", nextdate ="DATETIME('now','+7 days')")
        
    def test_nonexisting(self):
        testvol=self.volnames+'miss'
        self.add_vol_to_db(vol=testvol)
        self.run_workflow_periodic()
        self.assertFalse(self.check_vol_is_in_db(vol=testvol))



    def test_mounted_data(self):
        testvol=self.volnames+'data'
        if not self.check_mountpoint_is_present(vol=testvol):
            self.cleanup_datacopy_if_present(vol=testvol)
        self.volcreate_if_missing(vol=testvol)
        self.volmount_if_missing(vol=testvol)
        self.volaccess_if_missing(vol=testvol)
        content = 'Really Important content'
        self.create_testfile_if_missing(vol=testvol, text=content)
        self.create_testfile_if_missing(vol=testvol, fname=self.get_testdir(testvol)+'/ignoreME_archived_toberemoved', text='already archived content')


        self.tape_deletefile(tape_file=f'afs_cern.ch_user_{self.user[0]}_{self.user}_tmp_test_afstrack_workflows_s.afs.testdata.tar', ignore_miss=True)
        self.tape_deletefile(tape_file=f'afs_cern.ch_user_{self.user[0]}_{self.user}_tmp_test_afstrack_workflows_s.afs.testdata.afsacls', ignore_miss=True)

        self.add_vol_to_db(vol=testvol, owner=self.user)  # with owner set, will try nschown. Which fails.

        self.run_workflow_periodic()

        self.tape_checkfile(tape_file=f'afs_cern.ch_user_{self.user[0]}_{self.user}_tmp_test_afstrack_workflows_s.afs.testdata.tar')
        self.tape_checkfile(tape_file=f'afs_cern.ch_user_{self.user[0]}_{self.user}_tmp_test_afstrack_workflows_s.afs.testdata.afsacls')

        self.assertFalse(self.check_mountpoint_is_present(vol=testvol))
        self.assertFalse(self.check_volume_is_present(vol=testvol))
        self.assertFalse(self.check_vol_is_in_db(vol=testvol))

        self.cleanup_testfile(vol=testvol)

    # keep me last
    def test_check_other_in_db(self):
        self.assertTrue(self.check_vol_is_in_db(vol="test.leaveme.other.notmyworkflow"))
        self.assertTrue(self.check_vol_is_in_db(vol="test.leaveme.other.notmyhost"))
        self.assertTrue(self.check_vol_is_in_db(vol="test.leaveme.other.notyet"))





if __name__ == '__main__':
    unittest.main()
