#!/usr/bin/python3
#
## log message to a MySQL DB, via SQLAlchemy

import socket
from datetime import datetime
import logging

class DBHandler (logging.Handler):
    def __init__(self):
        logging.Handler.__init__(self)
        import afstracker.models as am # import this late, so that users can set DATABASE_URL before
        self.model = am
        self.log_dbsession = am.db.session()  # should give a new one?
        self.activehost = socket.gethostname()
        self.volname = None

    def set_activevolume(self, volname):
        self.volname = volname

    def emit(self, record):
        if len(record.msg) > 4095:
            record.msg = record.msg[:4000]+'[..truncated..]'
        logrecord = self.model.ActionLog(
            timestamp = datetime.fromtimestamp(record.created),
            loglevel = record.levelname,
            workflow_name = record.module, 
            workflow_activehost = self.activehost+':'+str(record.process),
            volname = self.volname,
            message = record.msg  )
        try:
            logrecord.volname = record.volume
        except AttributeError:
            pass
        self.log_dbsession.add(logrecord)


    def flush(self):
        if getattr(self, 'log_dbsession', None):
            self.log_dbsession.close()
