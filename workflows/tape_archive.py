#!/usr/bin/python3
#
# copy a AFS mountpoint+subtree to CTA, as single archive
## size limits, submounts need to be resolved 'before'
# unmount
# wait
# delete (all subvolumes)

import os
import socket
from datetime import datetime, timedelta
import time
import random
import re
import subprocess
import sys

import afsutil.volinfo as Volinfo
from afsutil.volutil import Volutil

default_wait = timedelta(days=30)
max_sleep = 5*60

tapeserver='eosctapublicdisk.cern.ch'

this_workflow = 'tape_archive'
this_host = socket.gethostname()
this_process = os.getpid()
soft_exit_file = '/var/run/'+this_workflow+'.soft_exit'

def soft_exit_perhaps(log):
    if os.path.exists(soft_exit_file):
        log.info("soft exit for workflow processor {}".format(this_workflow))
        try:
            os.remove(soft_exit_file)
        except OSError:
            pass
        sys.exit(33)  # error, so that systemd restarts us

def run_periodic(log, args, am):
    cur_sleep = 0
    log.info("workflow processor for {} started".format(this_workflow))
    if not Volinfo.Volinfo('root.cell'):
        log.error('error on startup - cannot look at "root.cell", bailing out')
        sys.exit(1)

    while True:
        soft_exit_perhaps(log)
        now = datetime.now()
        am.db.session.commit()  # to get fresh objects
        volstate = am.VolumeState.query.filter(am.VolumeState.workflow_name == this_workflow,
                                               am.VolumeState.workflow_activehost == None,
                                               am.VolumeState.workflow_next <= now).first()
        if not volstate:
            if args.nowaitloop:
                return

            cur_sleep = min(max_sleep, cur_sleep * 2) + random.randint(0, 5)
            log.debug('no data found, sleeping for {}secs'.format(cur_sleep))
            time.sleep(cur_sleep)
            continue

        cur_sleep = 0

        # lock the record for us. Need to push to DB, and check for races
        identifier = ':'.join([this_host, str(this_process)])
        volname = volstate.volname # might be gone afterwards..
        volstate.workflow_activehost = identifier
        am.db.session.commit()
        volstate = am.VolumeState.query.filter(am.VolumeState.workflow_name == this_workflow,
                                               am.VolumeState.volname == volname,
                                               am.VolumeState.workflow_activehost == identifier).first()
        if not volstate:
            log.warning("could not lock record for {}".format(volname))
            continue
        log.debug("record for {} now locked by {}.".format(volstate.volname, identifier))

        project = volstate.workflow_params
        log.info("looking at volume '{}' from project '{}' in state '{}'".format(volstate.volname, project, volstate.workflow_state))

        next_actiondate = now + default_wait
        if volstate.workflow_defaultnextsec:
            next_actiondate = now + timedelta(seconds=volstate.workflow_defaultnextsec)
        if args.waitsecs >= 0:
            next_actiondate = now + timedelta(seconds=int(args.waitsecs))

        # see whether there are still outstanding workflows for any still-mounted subdirectories.
        # if so, push back and wait (for a few hours)
        volstate_child = am.VolumeState.query.filter(am.VolumeState.workflow_name == this_workflow,
                                                     am.VolumeState.pathname.like(volstate.pathname+'/%'),
                                                     ~am.VolumeState.workflow_state.in_(['delete_subtree_after_wait','unmount_wait'])).first()
        if volstate_child:
            log.info("{} is blocked by '{}' in state '{}, will wait'".format(volstate.volname, volstate_child.volname, volstate_child.workflow_state))
            volstate.workflow_next = now + timedelta(seconds=int(args.waitblocksecs))
            volstate.workflow_activehost = None
            continue

        # this acesses AFS and might take a while
        v = Volinfo.Volinfo(volstate.volname, initial_mountpoints=[volstate.pathname])

        # terminal state - volume is gone, also remove from state DB
        if not v:
            log.info("volume {} does not exist (anymore) in VLDB. Over.".format(volstate.volname))
            am.db.session.delete(volstate)
            continue

        if volstate.workflow_state == 'unmount_wait':
            mount = '(undef)'
            all_subvols_mounts = Volutil.get_volumes_or_mounts_under_dir(volstate.pathname,
                                                                         want_volumes=True,
                                                                         want_mounts=True)  # would only work if the mount DB has been updated in between
            try:
                for mount in v.get_verified_mountpoints():
                    try:
                        Volutil.delete_mount(mount)
                    except subprocess.CalledProcessError as e:
                        # seen cases where the parent was mounted twice, so first unmount removed  actually several mountpoints..
                        if 'not existing or unreadable' in e.output:
                            log.warning("could not remove mountpoint '{}' for {}:{}.".format(mount, volstate.volname, e))
                            pass
                        elif 'Volume is locked for a release operation' in e.output:
                            # several children have been removed (independendly) at the same time.. the locking 'vos release' will remove all.
                            log.info("concurrent 'vos release' when removing mountpoint '{}' for {}.".format(mount, volstate.volname))
                            pass
                        else:
                            raise

                    log.info("removed mountpoint '{}' for {}.".format(mount, volstate.volname))
                log.debug("no more verified mountpoints for {}. Now waiting until volume deletion.".format(volstate.volname))
                if all_subvols_mounts:
                    # record that the sub-volumes need to be cleaned up as well
                    log.info('{} for {} had {} sub-mounts that will be removed.'.format(volstate.pathname, volstate.volname, len(all_subvols_mounts)))
                    for subvol,submount in all_subvols_mounts:
                        # we ignore entries in any state, not just in 'delete_subtree_after_wait'
                        volstate_child = am.VolumeState.query.filter(am.VolumeState.workflow_name == this_workflow,
                                                                     am.VolumeState.volname == subvol).first()
                        if volstate_child:
                            log.info("'{}': cleanup entry for '{}' submount already exists, skipping".format(volstate.volname, subvol))
                        else:
                            log.info("'{}': adding cleanup entry for '{}' submount".format(volstate.volname, subvol))
                            dbvol = am.VolumeState(volname = subvol,
                                                   pathname = submount,
                                                   workflow_name = this_workflow,
                                                   workflow_state = 'delete_subtree_after_wait',
                                                   workflow_since = now,
                                                   workflow_next = next_actiondate,
                                                   workflow_params = project,
                                                   owner = volstate.owner,
                                                   workflow_defaultnextsec = volstate.workflow_defaultnextsec,
                                               )
                            am.db.session.add(dbvol)
                volstate.workflow_next = next_actiondate
                volstate.workflow_state = 'delete_subtree_after_wait'
                volstate.workflow_activehost = None
            except subprocess.CalledProcessError as e:
                volstate.workflow_state = 'ERROR:' + volstate.workflow_state
                volstate.workflow_next = None
                log.error('error while removing mountpoint {} for {}: {}'.format(mount, volstate.volname, e.output))

        elif volstate.workflow_state == 'delete_subtree_after_wait':

            # how to keep track of all subvolumes, once we have unmounted? they store their own path - if they got added at same time.
            try:
                Volutil.delete_volume(volstate.volname)
                log.info("INFO: deleted volume '{}' ({}) in VLDB. Over.".format(volstate.volname, v.id))
                am.db.session.delete(volstate)
                continue
            except subprocess.CalledProcessError as e:
                volstate.workflow_state = 'ERROR:' + volstate.workflow_state
                volstate.workflow_next = None
                log.error('error while deleting {}: {}'.format(volstate.volname, e.output))

        elif volstate.workflow_state == 'initial':
            if volstate.pathname not in v.get_verified_mountpoints():
                volstate.workflow_state = 'ERROR:' + volstate.workflow_state
                volstate.workflow_next = None
                log.error('{} is not a mountpoint for {}'.format(volstate.pathname, volstate.volname))
                continue
            all_subvols = Volutil.get_volumes_under_dir(volstate.pathname)
            if all_subvols:
                log.info('{} for {} has {} sub-mounts.'.format(volstate.pathname, volstate.volname, len(all_subvols)))
                for submount in all_subvols:
                    if not re.search(r'.\.'+project+'\.', submount):
                        volstate.workflow_state = 'ERROR:' + volstate.workflow_state
                        volstate.workflow_next = None
                        log.error('{} for {} has non-{} volume {} inside. Cannot handle'.format(volstate.pathname, volstate.volname, project, submount))
                        continue
            log.debug("volume '{}' will be archived to CTA.".format(volstate.volname))
            action_tar_data_to_tape_and_rename(log=log, args=args, volstate=volstate, next_actiondate=next_actiondate)
            # goes to ERROR or 'unmount_wait'

        else:
            assert(False),"unexpected workflow state {} for volume {}".format(volstate.workflow_state, volstate.volname)


        # push back changed data for this volume to DB
        am.db.session.commit()


#################################################

def action_tar_data_to_tape_and_rename (log, args, volstate, next_actiondate):
    project = volstate.workflow_params
    afspath = volstate.pathname
    step = 'initializing'
    out =''

    tape_dir = args.tapeprefix + '/'+ project
    tape_filename = re.sub(r'[^-a-zA-Z0-9_.:]','_',afspath[1:])
    tape_fullpath = tape_dir + '/' + tape_filename + '.tar'
    eos_server='root://'+tapeserver
    tape_url=eos_server+'/'+tape_fullpath
    rename_tag = '_archived_toberemoved'
    afspath_renamed = afspath+rename_tag

    tape_acldump_fullpath = tape_dir + '/' + tape_filename + '.afsacls'
    tape_acldump_url=eos_server+'/'+tape_acldump_fullpath

    if not afspath.startswith('/afs/cern.ch/'):
        volstate.workflow_state = 'ERROR:' + volstate.workflow_state
        volstate.workflow_next = None
        log.error("non-'/afs/cern.ch/' path {} for volume '{}'".format(afspath_renamed, volstate.volname))
        return

    if os.path.exists(afspath_renamed):
        volstate.workflow_state = 'ERROR:' + volstate.workflow_state
        volstate.workflow_next = None
        log.error("archive path {} already exists for volume '{}'".format(afspath_renamed, volstate.volname))
        return

    try:

        step = 'creating CTA directory'
        cmd = ['xrdfs', tapeserver, 'mkdir','-p', '-mrwx------', tape_dir]
        out = subprocess.check_output(cmd, stderr=subprocess.STDOUT,
                                      universal_newlines=True)

        m = None
        # dump AFS ACLs to CTA, unless the file exists
        try:
            step = 'pre-checking AFS ACL dump'
            cmd = ['xrdfs', tapeserver, 'stat', tape_acldump_fullpath]
            out = subprocess.check_output(cmd, stderr=subprocess.STDOUT,
                                          universal_newlines=True)
            m = re.search(r'Size:\s+(\d+)', out)
        except subprocess.CalledProcessError:
            pass
        if not m:
            step = 'dumping AFS ACLs'

            find_la_cmd = ['find', afspath]
            # ignore already-archived subtrees, run "fs la" on directories
            find_la_cmd += ['-name', '*'+rename_tag, '-prune', '-o', '-type', 'd', '-exec', 'fs', 'listacl', '-path', '{}', ';']
            # pipe output directly to 'xrdcp'
            xrdcp_la_cmd = ['xrdcp','--silent','--posc', '-', tape_acldump_url]

            find_la_process = subprocess.Popen(find_la_cmd, stdout=subprocess.PIPE)

            xrdcp_la_process = subprocess.Popen(xrdcp_la_cmd, stdin=find_la_process.stdout, stdout=subprocess.PIPE,
                                                stderr=subprocess.STDOUT,
                                                env=dict(os.environ, XRD_WRITERECOVERY='false',
                                                      XRD_STREAMTIMEOUT='86400',
                                                      XRD_REQUESTTIMEOUT='90000',
                                                      XRD_DATASERVERTTL='93600',
                                                      XRD_TCPKEEPALIVE='1',
                                                      XRD_TCPKEEPALIVEINTERVAL='120',
                                                      XRD_TCPKEEPALIVEPROBES='20'),
                                                universal_newlines=True)
            find_la_process.stdout.close()
            xrdcp_la_output, err_unused = xrdcp_la_process.communicate()
            xrdcp_la_process.wait()
            find_la_process.wait()
            if '[ERROR]' in xrdcp_la_output or xrdcp_la_process.returncode != 0 or find_la_process.returncode != 0:
                raise subprocess.CalledProcessError(returncode=xrdcp_la_process.returncode, cmd='find ... exec fs la | xrdcp ... {}'.format(tape_acldump_url), output=xrdcp_la_output)


        m = None
        # the copy to CTA is expensive (can take hours). skip if already present
        try:
            step='pre-checking CTA file'
            cmd = ['xrdfs', tapeserver, 'stat', tape_fullpath]
            out = subprocess.check_output(cmd, stderr=subprocess.STDOUT,
                                          universal_newlines=True)
            m = re.search(r'Size:\s+(\d+)', out)
        except subprocess.CalledProcessError:
            pass
        if not m:
            # fixup files without permissions (as these cause xrdcp to fail). Slow.
            # assume that we have none or few
            step="finding unreadable files"
            find_cmd = ['find', afspath]
            # could add submounts to exclude here
            # at least ignore already-archived subtrees
            find_cmd += ['-name', '*'+rename_tag, '-prune', '-o', '!', '-perm', '-u=r', '-print0']
            find_out = subprocess.check_output(find_cmd, stderr=subprocess.STDOUT,
                                               universal_newlines=True)
            step="fixing unreadable files"
            for unreadable in find_out.split('\000'):
                if unreadable:
                    out = subprocess.check_output(['chmod', 'u+r', unreadable], stderr=subprocess.STDOUT,
                                                  universal_newlines=True)
                    log.warning("made file '{}' readable".format(unreadable))

            step = 'tar and xrdcp'
            # cd to /, treat rest as relative. else get "tar: Removing leading `/' from member names"
            # print something every 1GB (unit is 'record': 512bytes * default blocking factor = 20)
            # print totals at end
            # no info messages about sparse files
            tar_cmd = ['tar','--create', '--file=-',
                       '--warning=no-file-shrank',
                       '--checkpoint=100000',
                       '--totals',
                       '--exclude=*'+rename_tag,
                       '--directory','/',  afspath[1:]]
            xrdcp_cmd = ['xrdcp','--silent','--posc', '-', tape_url]  # expect this to fail if the destination exists

            tar_process = subprocess.Popen(tar_cmd, stdout=subprocess.PIPE)
            xrdcp_process = subprocess.Popen(xrdcp_cmd, stdin=tar_process.stdout, stdout=subprocess.PIPE,
                                             stderr=subprocess.STDOUT,
                                             env=dict(os.environ, XRD_WRITERECOVERY='false',
                                                      XRD_STREAMTIMEOUT='86400',
                                                      XRD_REQUESTTIMEOUT='90000',
                                                      XRD_DATASERVERTTL='93600',
                                                      XRD_TCPKEEPALIVE='1',
                                                      XRD_TCPKEEPALIVEINTERVAL='120',
                                                      XRD_TCPKEEPALIVEPROBES='20'),
                                             universal_newlines=True)
            tar_process.stdout.close()
            xrdcp_output, err_unused = xrdcp_process.communicate()
            xrdcp_process.wait()
            tar_process.wait()
            if '[ERROR]' in xrdcp_output or xrdcp_process.returncode != 0 or tar_process.returncode != 0:
                raise subprocess.CalledProcessError(returncode=xrdcp_process.returncode, cmd='tar ... | xrdcp ... {}'.format(tape_url), output=xrdcp_output)

        # remaining steps are fast/metadata-only. Do anyway


        # check 'nsls'.
        step='checking CTA file permission, ownership and size'
        cmd = ['xrdfs', tapeserver, 'ls', '-l', tape_fullpath] #replace nsls
        out = subprocess.check_output(cmd, stderr=subprocess.STDOUT, env=dict(os.environ),
                                      universal_newlines=True)
        m = re.search(r'^([-mrwx]+)\s+\d+-\d+-\d+\s\d+:\d+:\d+\s+(\d+)', out)
        if not m:
            raise subprocess.CalledProcessError(returncode=1, cmd='xrdfs stat', output='CTA "xrdfs ls -l" does not match our pattern')
        current_mode = m.group(1)
        ## currentowner = m.group(2)
        ## currentgroup =  m.group(3)
        nsls_size = int(m.group(2))

        EMPTY_TAR_SIZE = 10240
        MAXDEPTH_CHECKS = 4
        AFS_DIRSIZE = 2048
        # seems to indicate an empty .tar file; but has false positives for 'small' projects and near-empty volumes
        if  nsls_size == EMPTY_TAR_SIZE:
            # run some check whether we really have a mini-volume

            # next problem: each directory (even empty) on AFS is reported at 2k size, so a handful of these will be "bigger" on AFS than on CTA
            step='small CTA tarfile - checking size on AFS'
            cmd = ['du', '--bytes', '--total',
                   '--max-depth='+str(MAXDEPTH_CHECKS),  # so we only get a lower bound for the AFS size
                   '--one-file-system', '--exclude=*'+rename_tag,
                   afspath]
            out = subprocess.check_output(cmd, stderr=subprocess.STDOUT,
                                          universal_newlines=True)
            m = re.search(r'(\d+)\s+total', out)
            if not m:
                raise subprocess.CalledProcessError(returncode=1, cmd=cmd, output='"du" does not return expected format')
            afs_size = m.group(1)

            step='small CTA tarfile - counting directories on AFS'
            cmd = ['afind', '--maxdepth='+str(MAXDEPTH_CHECKS),
                   '--type=d',
                   afspath]
            out = subprocess.check_output(cmd, stderr=subprocess.STDOUT,
                                          universal_newlines=True)
            dircount = out.count(afspath)  # since all subdirectories start with this
            #  this might include some mountpoints patterns that we ignore
            afs_size_corrected = int(afs_size) - AFS_DIRSIZE * dircount # dont care even if this goes negative
            if afs_size_corrected >= nsls_size:
                volstate.workflow_state = 'ERROR:' + volstate.workflow_state
                volstate.workflow_next = None
                log.error("CTA tar file seems empty (size={}b), AFS size is >= {}b, for volume '{}', CTA: {}".format(nsls_size, afs_size_corrected, volstate.volname, tape_url))
                return

        step='changing CTA permissions for tar file'
        cmd = ['eos', eos_server, 'chmod', '400', tape_fullpath]
        out = subprocess.check_output(cmd, stderr=subprocess.STDOUT,
                                          universal_newlines=True)  # unused

        # 'chown' is privileged and will fail during tests
        if volstate.owner:
            step='eos chown CTA tar file to {}'.format(volstate.owner)
            cmd = ['eos', eos_server, 'chown', volstate.owner+':def-cg', tape_fullpath]
            out = subprocess.check_output(cmd, stderr=subprocess.STDOUT,
                                          env=dict(os.environ),
                                          universal_newlines=True)
        else:
            log.info('skipping "eos chown" on CTA for {}, desired owner is {}'.format(volstate.volname, volstate.owner))

        # also need chmod/chown for the ACL file.
        # we didn't check stats/size above, just do it unconditionally.. can cause errors, will ignore
        step='changing CTA permissions for ACL file'
        cmd = ['eos', eos_server, 'chmod', '400', tape_acldump_fullpath]
        try:
            out = subprocess.check_output(cmd, stderr=subprocess.STDOUT,
                                          universal_newlines=True)  # unused
        except subprocess.CalledProcessError:
            log.info('ignoring "xrdfs chmod" error on CTA for {}'.format(tape_acldump_fullpath))

        if volstate.owner:
            step='eos chown CTA ACL file to {}'.format(volstate.owner)
            cmd = ['eos', eos_server, 'chown', volstate.owner+':def-cg', tape_acldump_fullpath]
            out = subprocess.check_output(cmd, stderr=subprocess.STDOUT,
                                          env=dict(os.environ),
                                          universal_newlines=True)
        else:
            log.info('skipping "eos chown" on CTA for {}, desired owner is {}'.format(tape_acldump_fullpath, volstate.owner))



        step = 'renaming mount from {} to {} via mv'.format(afspath, afspath_renamed)
        try:
            cmd = ['mv', afspath, afspath_renamed]   # can use same exception, unlike for os.rename
            out = subprocess.check_output(cmd, stderr=subprocess.STDOUT,
                                          universal_newlines=True)
        except subprocess.CalledProcessError:
            step = 'renaming mount from {} to {} via afs_admin'.format(afspath, afspath_renamed)
            Volutil.rename_mount(afspath, afspath_renamed)
        volstate.pathname = afspath_renamed # make DB aware of new mountpoint

        log.info("tree {} for top volume '{}' has been archived to '{}' ({}bytes)".format(afspath, volstate.volname,  tape_url, nsls_size))
        log.info("volume '{}' is now mounted under {}".format(volstate.volname,  afspath_renamed))

    except subprocess.CalledProcessError as e:
        volstate.workflow_state = 'ERROR:' + volstate.workflow_state
        volstate.workflow_next = None
        log.error('error while {} for {}: {}'.format(step, volstate.volname, e.output))
        return

    volstate.workflow_state = 'unmount_wait'
    volstate.workflow_next = next_actiondate
    volstate.workflow_activehost = None
    return


if __name__ == "__main__":
    usage = """Look at DB, copy single-volume mountpoints to CTA."""
    import logging
    import logging.handlers
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    from argparse import ArgumentParser 
    parser = ArgumentParser(usage=usage)
    parser.add_argument("--debug",
                        help="more verbose output",
                        action='store_true',
                    )
    parser.add_argument("--syslog",
                        help="also log to syslog",
                        action='store_true',
                    )
    parser.add_argument("--nowaitloop",
                        help="if set, do not wait for new data to arrive - just exit after processing all",
                        action='store_true',
                    )
    parser.add_argument("--waitsecs",
                        type = int,
                        default = -1,
                        help="how long [seconds] to wait between actions",
                    )
    parser.add_argument("--waitblocksecs",
                        type = int,
                        default = 7200,
                        help="how long [seconds] to wait when being blocked by a sub-volume",
                    )
    parser.add_argument("--tapeprefix",
                        default = '/eos/ctapublicdisk/archive/afsmigration/restricted',
                        help="where on CTA will the files end up",
                    )
    args = parser.parse_args()
    if args.debug:
        console = logging.StreamHandler(sys.stdout)
        console.setLevel(logging.DEBUG)
        logger.addHandler(console)
    else:
        import dblogger
        dblog = dblogger.DBHandler()
        dblog.setLevel(logging.INFO)
        logger.addHandler(dblog)

    if args.syslog:
        syslog = logging.handlers.SysLogHandler(address='/dev/log',
                                            facility=logging.handlers.SysLogHandler.LOG_DAEMON)
        syslog.setLevel(logging.INFO)
        logger.addHandler(syslog)
    
    import afstracker.models as am
    run_periodic(logger, args, am)

