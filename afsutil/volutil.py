import subprocess
import re
import os
import time

class Volutil(object):

    VOLUTIL_SLEEP = 5  # some operations take a while to propagate
    @classmethod
    def set_vol_quota(cls, volname, newquota):
        subprocess.check_call(['vos',
                               'setfields',
                               '-maxquota', str(newquota),
                               '-id', volname], stderr=subprocess.STDOUT)

    @classmethod
    def get_volumes_or_mounts_under_dir(cls, dirname, want_volumes=False, want_mounts=False):
        """ anything mounted deeper down, excluding the argument. returns mountpoints or volumes"""
        mounts = []
        out = subprocess.check_output(['volume_mount_scan.pl',
                                       '--nowarn',
                                       '--exclude', '^SoMeStringThaTNev3ratchesIh0pe', ## default is to ignore Y.user
                                       '--regex',
                                       dirname+'/'],
                                      env={'PATH': os.getenv('PATH')+':/afs/cern.ch/project/afs/cernafs/etc'},
                                      universal_newlines=True)
        for line in out.splitlines():
            m = re.search('^((\S+)\s+\(\d+\) ->)?\s+(/afs\S+)', line)
            if m:
                if want_volumes and want_mounts:
                    mounts.append([m.group(2), m.group(3)])
                elif want_volumes:
                    mounts.append(m.group(2))
                elif want_mounts:
                    mounts.append(m.group(3))
                else:
                    raise RuntimeError('say whether you want volumes or mountpoints or both')
        return mounts

    @classmethod
    def get_mounts_under_dir(cls, dirname):
        return cls.get_volumes_or_mounts_under_dir(dirname, want_volumes=False, want_mounts=True)

    @classmethod
    def get_volumes_under_dir(cls, dirname):
        return cls.get_volumes_or_mounts_under_dir(dirname, want_volumes=True, want_mounts=False)

    @classmethod
    def get_mounts_for_volume(cls, volname):
        # FIXME: implement native method
        mounts = []
        out = subprocess.check_output(['volume_mount_scan.pl',
                                       '--nowarn',
                                       '--volume',
                                       volname],
                                      env={'PATH': os.getenv('PATH')+':/afs/cern.ch/project/afs/cernafs/etc'},
                                      universal_newlines=True)
        for line in out.splitlines():
            m = re.search('^((\S+)\s+\(\d+\) ->)?\s+(/afs\S+)', line)
            if m:
                if(m.group(2) and m.group(2) != volname):
                    print("ERROR: 'volume_mount_scan.pl --volume {}'"\
                        " returned wrong vol '{}':{}".format(volname, m.group(2), out))
                    continue
                mounts.append(m.group(3))
        return mounts

    @classmethod
    # use 'afs_admin' for extra logging (and handling RO mounts etc).
    # Problem: return codes are unreliable
    def check_mount(cls, dirname, volname):
        is_mount = False
        cmd = ['fs', 'lsmount', '-dir', dirname ]
        try:
            # cannot use 'exists()' or similar for non-existing volumes
            out = subprocess.check_output(cmd,
                                          stderr=subprocess.STDOUT,
                                          universal_newlines=True)
            m = re.search("'([^']+)' is a mount point for volume '#(\S+)'", out)
            if m:
               if m.group(1) == dirname and\
                  m.group(2) == volname:
                   is_mount = True
        except subprocess.CalledProcessError:
            pass
        return  is_mount

    @classmethod
    # use 'afs_admin' for extra logging (and handling RO mounts etc).
    # Problem: return codes are unreliable
    def delete_mount(cls, dirname, local=False):
        cmd = ['afs_admin', 'delete_mount', dirname ]
        if local:
            cmd = ['fs', 'rmmount', '-dir', dirname]
        out = subprocess.check_output(cmd,
                                      stderr=subprocess.STDOUT,
                                      universal_newlines=True)
        if re.search('ERROR:|FATAL:', out) and \
            not re.search('vldb entry is already locked', out):
            raise subprocess.CalledProcessError(returncode=1, cmd=cmd, output=out)
        if not local:
            time.sleep(cls.VOLUTIL_SLEEP)

    @classmethod
    def create_mount(cls, dirname, volname, local=False):
        cmd = ['afs_admin', 'create_mount', dirname, volname ]
        if local:
            cmd = ['fs', 'mkmount',
                         '-dir', dirname,
                         '-vol', volname]
        out = subprocess.check_output(cmd,
                                      stderr=subprocess.STDOUT,
                                      universal_newlines=True)
        if re.search('cannot |ERROR:|FATAL:', out):
            raise subprocess.CalledProcessError(returncode=1, cmd=cmd, output=out)
        if not local:
            time.sleep(cls.VOLUTIL_SLEEP)

    @classmethod
    def rename_mount(cls, olddirname, newdirname):
        cmd = ['afs_admin', 'rename_mount', olddirname, newdirname ]
        out = subprocess.check_output(cmd,
                                      stderr=subprocess.STDOUT,
                                      universal_newlines=True)
        if re.search('cannot |ERROR:|FATAL:', out) and \
            not re.search('vldb entry is already locked', out):
            raise subprocess.CalledProcessError(returncode=1, cmd=cmd, output=out)
        time.sleep(cls.VOLUTIL_SLEEP)

    @classmethod
    def delete_volume(cls, volname):
        cmd = ['afs_admin', 'delete_volume', volname ]
        out = subprocess.check_output(cmd,
                                      stderr=subprocess.STDOUT,
                                      universal_newlines=True)
        if re.search('cannot |ERROR:|FATAL:', out):
            raise subprocess.CalledProcessError(returncode=1, cmd=cmd, output=out)
        time.sleep(cls.VOLUTIL_SLEEP)

    @classmethod
    def create_volume(cls, volname):
        cmd = ['afs_admin', 'create_volume', volname ]
        out = subprocess.check_output(cmd,
                                      stderr=subprocess.STDOUT,
                                      universal_newlines=True)
        if re.search('cannot |ERROR:|FATAL:', out):
            raise subprocess.CalledProcessError(returncode=1, cmd=cmd, output=out)
        time.sleep(cls.VOLUTIL_SLEEP)

    @classmethod
    def create_replica(cls, volname):
        cmd = ['afs_admin', 'create_replica', volname ]
        out = subprocess.check_output(cmd,
                                      stderr=subprocess.STDOUT,
                                      universal_newlines=True)
        if re.search('cannot |ERROR:|FATAL:', out):
            raise subprocess.CalledProcessError(returncode=1, cmd=cmd, output=out)
        time.sleep(cls.VOLUTIL_SLEEP)

    @classmethod
    def set_acl(cls, dirname, account, acl):
        cmd = ['afs_admin', 'set_acl', dirname, account, acl ]
        out = subprocess.check_output(cmd,
                                      stderr=subprocess.STDOUT,
                                      universal_newlines=True)
        if re.search('cannot |ERROR:|FATAL:', out):
            raise subprocess.CalledProcessError(returncode=1, cmd=cmd, output=out)
        # no sleep, is 'fs sa'

    @classmethod
    def running_as_admin(cls):
        out = subprocess.check_output(['pts',
                                       'membership',
                                       'system:administrators'],
                                      stderr=subprocess.STDOUT,
                                      universal_newlines=True)
        if re.search('Permission denied', out):
            return False
        return True

    @classmethod
    def flush_mount(cls, mnt):
        subprocess.call(['fs', 'flushmount', mnt],
                        stderr=subprocess.STDOUT)

    @classmethod
    def flush_vol(cls, dir):
        subprocess.call(['fs', 'flushvol', dir],
                        stderr=subprocess.STDOUT)

    @classmethod
    def flush_all(cls):
        subprocess.call(['fs', 'flushall'],
                        stderr=subprocess.STDOUT)
