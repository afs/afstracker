from flask import Flask
from .config import Config

# the main object
afstracker = Flask(__name__)
# FIXME remove me for production
afstracker.debug = True

# its config (if any)
afstracker.config.from_object(Config)

# SQL calls need to be bound within an app context
# This way we set the context globally (even if not ideal)
afstracker.app_context().push()

# and its DB
# for MySQL, may have to set
# SQLALCHEMY_ENGINE_OPTIONS = {'pool_pre_ping': True }
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy(afstracker)

# keep at end
from afstracker import routes, models
