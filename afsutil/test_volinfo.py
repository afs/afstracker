#!/usr/bin/python3

import unittest
import subprocess
from . import volinfo

class TestVolinfo(unittest.TestCase):
  @classmethod
  def setUpClass(cls):
    try:
      subprocess.check_call(['vos', 'help'], stdout=subprocess.DEVNULL)
    except FileNotFoundError:
      raise unittest.SkipTest('these tests needs the "vos" command')

  def test_init(self):
    v = volinfo.Volinfo('user.iven')
    self.assertEqual(v.name, 'user.iven')

  def test_is_really_empty_not(self):
    v = volinfo.Volinfo('user.iven')
    self.assertFalse(v.is_really_empty())

  def test_is_really_empty(self):
    # empty and readable
    v = volinfo.Volinfo('s.afs.testemptyhome')
    self.assertTrue(v.is_really_empty())

  def test_filecount(self):
    v = volinfo.Volinfo('user.iven')
    self.assertTrue(v.filecount > 1000)
    self.assertTrue(v.filecount < 10000000)

  def test_get_guessed_mountpoints(self):
    v = volinfo.Volinfo('user.iven')
    guess = v.get_guessed_mountpoints()
    self.assertTrue('/afs/cern.ch/user/i/iven' in guess)

  def test_get_volscan_mountpoints(self):
    v = volinfo.Volinfo('user.iven')
    scanned = v.get_volscan_mountpoints()
    self.assertTrue('/afs/cern.ch/user/i/iven' in scanned)

  def test_get_verified_mountpoints(self):
    v = volinfo.Volinfo('user.iven', initial_mountpoints=['/afs.cern.ch/user/j/jan',  # mountpoint but not this user
                                                          '/afs/cern.ch/work/i/iven', # mountpoint but different type
                                                          '/afs/cern.ch/user/NOSUCHdIR'] # not a mountpoint
    )
    verified = v.get_verified_mountpoints()
    self.assertTrue('/afs/cern.ch/user/i/iven' in verified)
    self.assertFalse('/afs.cern.ch/user/j/jan' in verified)
    self.assertFalse('/afs/cern.ch/work/i/iven' in verified)
    self.assertFalse('/afs/cern.ch/user/NOSUCHdIR' in verified)

  def test_changed_mountpoints(self):
    v = volinfo.Volinfo('user.iven')
    verified = v.get_verified_mountpoints()
    self.assertTrue('/afs/cern.ch/user/i/iven' in verified)

    v_cache = volinfo.Volinfo('user.iven')
    self.assertTrue('/afs/cern.ch/user/i/iven' in v_cache.mounts)
    self.assertFalse(v_cache._initial_mounts)

    v_nocache = volinfo.Volinfo('user.iven',['/afs/cern.ch/user/j/jan'])
    self.assertFalse(v_nocache.mounts)  # empty list
    self.assertTrue('/afs/cern.ch/user/j/jan' in v_nocache._initial_mounts)

  def test_is_unmounted_no(self):
    v = volinfo.Volinfo('p.afs.root')
    self.assertFalse(v.is_unmounted())

  def test_is_unmounted_yes(self):
    v = volinfo.Volinfo('s.afs.testunmounted')
    self.assertTrue(v.is_unmounted())

if __name__ == '__main__':
    unittest.main()
