#!/usr/bin/python3

import site
site.addsitedir('..')

from datetime import datetime, timedelta
import afstracker.models as am
from argparse import ArgumentParser
import sys
import os
import stat
import subprocess
import re

workflow_name = 'tree2vol'
state = 'initial'

if __name__ == "__main__":
    parser = ArgumentParser(usage="split out an AFS tree into a new volume")
    parser.add_argument("--debug",
                        help="more verbose output",
                        action='store_true',
                    )
    parser.add_argument("--waitsecs",
                        type=int,
                        help="how long [seconds] to wait between actions for a single volume",
                    )
    parser.add_argument("--waitinitial",
                        type=int,
                        default=60,
                        help="how long [seconds] to wait before the first action",
                    )
    parser.add_argument("--directory",
                        required=True,
                        help="the AFS tree",
                    )

    parser.add_argument("--symlink",
                        required=False,
                        help="a symlink to the AFS tree, will become new mountpoint",
                    )

    parser.add_argument("--newvolname",
                        required=True,
                        help="the new volume name",
                    )
    parser.add_argument("--oldvolname",
                        required=False,
                        help="the existing volume name that contains 'directory'. Will guess if missing",
                    )
    parser.add_argument("--project",
                        required=True,
                        help="AFS project that owns the tree",
                    )
    args = parser.parse_args()

    now = datetime.now()

    next = now + timedelta(seconds=int(args.waitinitial))

    if not args.directory.startswith('/afs/cern.ch/'):
        print("Error: directory does not start with '/afs/cern.ch/'")
        sys.exit(1)
    if len(args.newvolname) > 20:  # official is 22 but CERN uses "R." to restore
        print("Error: new volume name is too long")
        sys.exit(1)
    s = os.stat(args.directory)
    if not stat.S_ISDIR(s.st_mode):
        print("Error: {} is not a directory".format(args.directory))
        sys.exit(1)
    if (s.st_ino % 2 == 0):
        print("Error: {} seems to be an AFS mountpoint".format(args.directory))
        sys.exit(1)
    if args.symlink:
        if not os.path.islink(args.symlink):
            print("Error: {} is not a symlink".format(args.symlink))
            sys.exit(1)
    if not args.oldvolname:
        fsexa_cmd = ['fs', 'examine','-path', args.directory]
        out = subprocess.check_output(fsexa_cmd,
                                      stderr=subprocess.STDOUT,
                                      universal_newlines=True)
        args.oldvolname = re.search(' named (.+?)\n', out).group(1)

    volstate = am.VolumeState.query.filter(am.VolumeState.volname == args.newvolname).first()
    if volstate:
        print("Error: skipping, new volume {} already in DB".format(args.volname))
        sys.exit(1)
    dbvol = am.VolumeState(volname=args.newvolname,
                           workflow_name = workflow_name,
                           workflow_state = state,
                           workflow_since = now,
                           workflow_next = next,
                           workflow_defaultnextsec = args.waitsecs,
                           pathname = args.directory,
                           destname = args.symlink,
                           workflow_params = args.project,
                           sourcevol = args.oldvolname,
    )
    am.db.session.add(dbvol)
    am.db.session.commit()
