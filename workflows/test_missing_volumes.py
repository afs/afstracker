#!/usr/bin/python3

import unittest
from testworkflow import TestWorkflow

class TestMissingVolumes(TestWorkflow):
    def setUp(self):
        TestWorkflow.setUp(self)
        self.workflow = 'missing_volumes'

    # main entry point
    def run_missing_volumes_periodic(self):
        import logging
        import sys
        logger = logging.getLogger()
        logger.setLevel(logging.DEBUG)
        logger.handlers = []  # remove leftovers from previous runs
        console = logging.StreamHandler(sys.stderr)
        console.setLevel(logging.DEBUG)
        logger.addHandler(console)

        args = TestWorkflow.Args()
        args.nowaitloop=True
        args.debug=True
        args.noadmin=True
        import missing_volumes as mv
        import afstracker.models as am

        mv.run_periodic(logger, args, am)



    def test_add_other_to_db(self):
        self.add_vol_to_db("test.leaveme.other.notmyworkflow", workflow = 'other_workflow')
        self.add_vol_to_db("test.leaveme.other.notmyhost", activehost = 'otherhost.cern.ch:1234')
        self.add_vol_to_db("test.leaveme.other.notyet", nextdate = "DATETIME('now','+7 days')")
        
    def test_nonexisting(self):
        testvol='NoSuchVolinAfs'
        self.volmount_if_missing(vol=testvol)  # should give a warning
        self.add_vol_to_db(vol=testvol)

        self.run_missing_volumes_periodic()

        self.assertFalse(self.check_mountpoint_is_present(vol=testvol))
        self.assertFalse(self.check_vol_is_in_db(vol=testvol))

    def test_exist_mounted(self):
        testvol=self.volnames+'empty'
        self.volcreate_if_missing(vol=testvol)
        self.volmount_if_missing(vol=testvol)
        self.add_vol_to_db(vol=testvol)

        self.run_missing_volumes_periodic()

        self.assertTrue(self.check_mountpoint_is_present(vol=testvol))
        self.assertTrue(self.check_volume_is_present(vol=testvol))
        self.assertTrue(self.check_vol_is_in_db(vol=testvol))  # in ERROR

    def test_check_other_in_db(self):
        self.assertTrue(self.check_vol_is_in_db(vol="test.leaveme.other.notmyworkflow"))
        self.assertTrue(self.check_vol_is_in_db(vol="test.leaveme.other.notmyhost"))
        self.assertTrue(self.check_vol_is_in_db(vol="test.leaveme.other.notyet"))




if __name__ == '__main__':
    unittest.main()
