#!/usr/bin/python3
import unittest
from testworkflow import TestWorkflow

class TestEosSimpleCopy(TestWorkflow):

    def setUp(self):
        TestWorkflow.setUp(self)
        self.workflow = 'eos_simple_copy'

    # main entry point
    def run_workflow_periodic(self):
        import logging
        import sys
        logger = logging.getLogger()
        logger.setLevel(logging.DEBUG)
        logger.handlers = []  # remove leftovers from previous runs
        console = logging.StreamHandler(sys.stderr)
        console.setLevel(logging.DEBUG)
        logger.addHandler(console)

        args = TestEosSimpleCopy.Args()
        args.nowaitloop = True
        args.waitsecs = 0
        args.debug = True
        args.noadmin = True

        import eos_simple_copy as workflow
        import afstracker.models as am

        workflow.run_periodic(logger, args, am)

    # keep me first
    def test_add_other_to_db(self):
        self.add_vol_to_db("test.leaveme.other.notmyworkflow", workflow = 'other_workflow')
        self.add_vol_to_db("test.leaveme.other.notmyhost", activehost = 'otherhost.cern.ch:1234')
        self.add_vol_to_db("test.leaveme.other.notyet", nextdate ="DATETIME('now','+7 days')")      

    def test_mounted_data(self):
        testvol=self.volnames+'data'
        if not self.check_mountpoint_is_present(vol=testvol):
            self.cleanup_datacopy_if_present(vol=testvol)
        self.volcreate_if_missing(vol=testvol)
        self.volmount_if_missing(vol=testvol)
        self.volaccess_if_missing(vol=testvol)
        content = 'Really Important content'
        self.create_testfile_if_missing(vol=testvol, text=content)

        publicread_dir = self.get_testdir(vol=testvol)+'/publicread'
        publicwrite_dir = self.get_testdir(vol=testvol)+'/publicwrite'  # shock, horror
        groupread_dir = self.get_testdir(vol=testvol)+'/mygroupread'
        groupwrite_dir = self.get_testdir(vol=testvol)+'/mygroupwrite'
        gwpr_dir = self.get_testdir(vol=testvol)+'/mygroupwritepublicread'
        protect_parent_dir=self.get_testdir(vol=testvol)+'/parent'
        protect_child_dir=self.get_testdir(vol=testvol)+'/parent/child'
        import os
        for d in (publicread_dir, groupread_dir,  publicwrite_dir,  groupwrite_dir, gwpr_dir,
                  protect_parent_dir, protect_child_dir):
            try:
                os.mkdir(d)
            except OSError:
                pass
        import pwd
        import grp 
        me = os.environ.get('USER')
        gid=pwd.getpwnam(me).pw_gid
        groupname=grp.getgrgid(gid).gr_name
        afs_groupname='cern:'+groupname
        import subprocess
        subprocess.check_call(['fs','setacl','-dir', self.get_testdir(vol=testvol), '-acl', 'system:anyuser', 'l'])
        subprocess.check_call(['fs','setacl','-dir', publicread_dir, '-acl', 'system:anyuser', 'read'])
        subprocess.check_call(['fs','setacl','-dir', publicwrite_dir,'-acl', 'cern:nodes', 'write'])
        subprocess.check_call(['fs','setacl','-dir', groupread_dir,'-acl', afs_groupname, 'read'])
        subprocess.check_call(['fs','setacl','-dir', groupwrite_dir, '-acl', afs_groupname, 'write'])
        subprocess.check_call(['fs','setacl','-dir', gwpr_dir, '-acl', 'system:anyuser', 'read'])
        subprocess.check_call(['fs','setacl','-dir', gwpr_dir,'-acl', afs_groupname, 'write'])
        subprocess.check_call(['fs','setacl','-dir', protect_parent_dir, '-acl', afs_groupname, 'read'])
        subprocess.check_call(['fs','setacl','-dir', protect_child_dir, '-acl', 'system:anyuser', 'read'])

        destdir = '/eos/home-' + self.user[0] + '/' + self.user + '/tmp/eoscopy_test'

        self.add_vol_to_db(vol=testvol, owner=None, destname=destdir)

        self.run_workflow_periodic()

        destfile = destdir +'/'+ 'testfile.txt'

        self.assertEqual(self.read_testfile(vol=testvol, fname=destfile), content)
        self.assertTrue(self.check_mountpoint_is_present(vol=testvol))
        self.assertTrue(self.check_volume_is_present(vol=testvol))
        self.assertFalse(self.check_vol_is_in_db(vol=testvol))

        import stat

        top_mode = oct(stat.S_IMODE(os.stat(destdir).st_mode))
        publicread_mode = oct(stat.S_IMODE(os.stat(destdir+'/publicread').st_mode))
        publicwrite_mode = oct(stat.S_IMODE(os.stat(destdir+'/publicwrite').st_mode))
        groupread_mode = oct(stat.S_IMODE(os.stat(destdir+'/mygroupread').st_mode))
        gwpr_mode = oct(stat.S_IMODE(os.stat(destdir+'/mygroupwrite').st_mode))
        protect_child_mode = oct(stat.S_IMODE(os.stat(destdir+'/parent/child').st_mode))

        # do cleanup early, since we skip() below
        self.cleanup_testfile(vol=testvol)
        self.cleanup_testfile(vol=testvol, fname=destfile)

        self.assertEqual(top_mode, '0o700')

        #self.skipTest("cannot test chmod() on EOSHOME")
        self.assertEqual(publicread_mode, '0o755')
        self.assertEqual(publicwrite_mode, '0o755')
        self.assertEqual(groupread_mode, '0o750')
        self.assertEqual(gwpr_mode, '0o770')
        self.assertEqual(protect_child_mode, '0o700')


    # keep me last
    def test_check_other_in_db(self):
        self.assertTrue(self.check_vol_is_in_db(vol="test.leaveme.other.notmyworkflow"))
        self.assertTrue(self.check_vol_is_in_db(vol="test.leaveme.other.notmyhost"))
        self.assertTrue(self.check_vol_is_in_db(vol="test.leaveme.other.notyet"))





if __name__ == '__main__':
    unittest.main()
