#!/usr/bin/python3
#
# workflow for backup volumes mounted in weird places
# user.X.backup in 'ubackup' would be OK
# normal users homedirs/workspaces: remove silently
# project areas or service accounts: mail

import os
import socket
from datetime import datetime, timedelta
import time
import random
import sys
import re
# EMail
import smtplib
from email.mime.text import MIMEText

from argparse import ArgumentParser 
import subprocess
import logging
import logging.handlers
import dblogger

import afsutil.volinfo as Volinfo
from afsutil.volutil import Volutil

from afsutil.phonebook import phonebook as Phonebook
from afsutil.projectutil import Projectutil

default_wait = timedelta(days=30)
override_wait = None

max_sleep = 5*60
adminaccount = 'afsmigr'  # gets mail in case something blows up
this_workflow = 'backup_volumes'
this_host = socket.gethostname()
this_process = os.getpid() 
soft_exit_file = '/var/run/'+this_workflow+'.soft_exit'

def soft_exit_perhaps(log):
    if os.path.exists(soft_exit_file):
        log.info("soft exit for workflow processor {}".format(this_workflow))
        try:
            os.remove(soft_exit_file)
        except OSError:
            pass
        import sys
        sys.exit(33)  # error, so that systemd restarts us

def run_periodic(log, args, am):
    cur_sleep = 0
    log.info("workflow processor for {} started".format(this_workflow))
    while True:
        soft_exit_perhaps(log)
        now = datetime.now()
        am.db.session.commit()  # to get fresh objects
        volstate = am.VolumeState.query.filter(am.VolumeState.workflow_name == this_workflow,
                                               am.VolumeState.workflow_activehost == None,
                                               am.VolumeState.workflow_next <= now).first()
        if not volstate:
            if args.nowaitloop:
                return

            cur_sleep = min(max_sleep, cur_sleep * 2) + random.randint(0, 5)
            log.debug('no data found, sleeping for {}secs'.format(cur_sleep))
            time.sleep(cur_sleep)
            continue

        cur_sleep = 0

        # lock the record for us. Need to push to DB, and check for races
        identifier = ':'.join([this_host, str(this_process)])
        volname = volstate.volname # might be gone afterwards..
        volstate.workflow_activehost = identifier
        am.db.session.commit()
        volstate = am.VolumeState.query.filter(am.VolumeState.workflow_name == this_workflow,
                                               am.VolumeState.volname == volname,
                                               am.VolumeState.workflow_activehost == identifier).first()
        if not volstate:
            log.warning("could not lock record for {}".format(volname))
            continue
        log.debug("record for {} now locked by {}.".format(volstate.volname, identifier))

        logextra={'volname': volstate.volname}  # used by DB logging
        log.info("looking at volume '{}' in state '{}' mounted (perhaps) on '{}'".format(volstate.volname, volstate.workflow_state, volstate.pathname), extra=logextra)

        # mandatory arguments: mountpoint and volume
        if not volstate.volname or not volstate.pathname:
            volstate.workflow_state = 'ERROR'
            volstate.workflow_next = None
            am.db.session.commit()
            assert(False),"DB has no volumename {} or no mountpoint {}".format(volstate.volname, volstate.pathname)

        v = Volinfo.Volinfo(volstate.volname, initial_mountpoints=[volstate.pathname])

        # terminal state - volume is gone, also remove from state DB
        if not v:
            log.info("volume {} does not exist (anymore) in VLDB. Over.".format(volstate.volname))
            if not args.noaction:
                am.db.session.delete(volstate)
            continue

        # no wait states etc
        if volstate.workflow_state != 'initial':
            volstate.workflow_state = 'ERROR'
            volstate.workflow_next = None
            am.db.session.commit()
            assert(False),"unexpected workflow state {} for volume {}".format(volstate.workflow_state, volstate.volname)

        # specific to backup volumes
        if not volstate.volname.endswith('.backup'):
            volstate.workflow_state = 'ERROR'
            volstate.workflow_next = None
            am.db.session.commit()
            assert(False),"unexpected non-backup volume {}".format(volstate.volname)

        # use canonical path
        if not volstate.pathname.startswith('/afs/cern.ch/'):
            volstate.workflow_state = 'ERROR'
            volstate.workflow_next = None
            am.db.session.commit()
            assert(False),"unexpected mountpoint for {} at {}".format(volstate.volname, volstate.pathname)

        # there is one area where 'user.X.backups' are legitimately mounted at CERN
        if volstate.volname.startswith('user.') and volstate.pathname.startswith('/afs/cern.ch/ubackup/'):
            volstate.workflow_state = 'ERROR'
            volstate.workflow_next = None
            am.db.session.commit()
            assert(False),"trying to remove user backup volume {} from ubackup path {}".format(volstate.volname, volstate.pathname)

        # state machine is simple: remove mountpoint.
        # but perhaps need to mail somebody (but not for stuff mounted in Primary/Secondary account personal areas)
        need_mail = True
        mail_recipients = []
        afsproject = None
        m = re.match('^/afs/cern.ch/(user|work)/[a-z]/([^/]+)/', volstate.pathname)
        if m:
            account = m.group(2)
            accounttype = Phonebook.get_accounttype(account)
            if accounttype in ['Primary', 'Secondary']:
                need_mail = False
            else:
                mail_recipients.append(account)
        elif volstate.workflow_params:
            afsproject = volstate.workflow_params
            mail_recipients.extend(Projectutil.afsproject_to_contacts(afsproject))
        else:
            m = re.match('^/afs/cern.ch/([^/]+)/([^/]+)/', volstate.pathname)
            assert(m),"weird too-short AFS path {}".format( volstate.pathname)
            if m.group(1) in ['project','eng','exp']:
                afsproject = m.group(2)
            else:
                afsproject = m.group(1)
            mail_recipients.extend(Projectutil.afsproject_to_contacts(afsproject))
        mail_recipients =  Phonebook.accounts_to_emails(mail_recipients)

        try:
            if Volutil.check_mount(dirname=volstate.pathname, volname=volstate.volname):
                Volutil.delete_mount(volstate.pathname)
                log.info("removed backup mountpoint '{}' for '{}'.".format(volstate.pathname, volstate.volname))
                if need_mail:
                    if mail_recipients:
                        if afsproject:
                            send_mail_project(mountpoint=volstate.pathname,
                                              project=afsproject,
                                              volname=volstate.volname,
                                              to=mail_recipients)
                        else:
                            send_mail_serviceaccount(mountpoint=volstate.pathname,
                                                     volname=volstate.volname,
                                                     to=mail_recipients)
                        log.info("sent notification mail for backup mountpoint '{}' to '{}'".format(volstate.pathname,
                                                                                                    ','.join(mail_recipients)))
                    else:
                        log.warning("could not send notification mail for backup mountpoint '{}': no recipients found".format(volstate.pathname))

            else:
                log.warning("did not find mountpoint '{}' for '{}'.".format(volstate.pathname, volstate.volname))
                    
            am.db.session.delete(volstate)
            log.info("removed tracking entry for volume '{}'.".format(volstate.volname))
        except subprocess.CalledProcessError as e:
            volstate.workflow_state = 'ERROR'
            volstate.workflow_next = None
            am.db.session.commit()
            log.error('error while removing mountpoint {} for {}: {}'.format(volstate.pathname, volstate.volname, e.output))

        # push back changed data for this volume to DB
        am.db.session.commit()

def send_mail_project(mountpoint, project, volname, to):
    blurb="""Dear AFS admins of project '{project}',

we have found that the AFS backup volume '{volname}' was still mounted under '{mountpoint}'.

This mountpoint has now been removed, since it would affect automatic data copying later during the AFS phaseout at CERN.
Please see https://cern.service-now.com/service-portal/view-outage.do?n=OTG0048727 for why permanently mounted backups can cause issues.
If you need to restore files from such a backup volume, please mount and unmount it every time. In case this is not an option for your use case, please open a ticket via
 https://cern.service-now.com/service-portal/report-ticket.do?name=request&fe=AFS

Regards,
  jan iven (AFS phaseout) 
"""
    subject = 'AFS project "{project}": spurious backup mountpoint removed'.\
              format(project=project)
    body = ( blurb + "\n-- \nsent by '{script}' from '{host}'").\
           format(project=project,
                  volname=volname,
                  mountpoint=mountpoint,
                  script=this_workflow,
                  host=this_host
           )
    
    send_generic_mail(subject=subject,
                      tos=to,
                      body=body)

def send_mail_serviceaccount(mountpoint, volname, to):
    account = to[0]
    blurb="""Dear owner of service account '{account}',

we have found that the AFS backup volume '{volname}' was still mounted under '{mountpoint}'.

This mountpoint has now been removed, since it would affect automatic data copying later during the AFS phaseout at CERN.
Please see https://cern.service-now.com/service-portal/view-outage.do?n=OTG0048727 for why permanently mounted backups can cause issues.
If you need to restore files from such a backup volume, please mount and unmount it every time. In case this is not an option for your use case, please open a ticket via
 https://cern.service-now.com/service-portal/report-ticket.do?name=request&fe=AFS

Regards,
  jan iven (AFS phaseout) 
"""
    subject = 'AFS account "{account}": spurious backup mountpoint removed'.\
              format(account=account)
    body = ( blurb + "\n-- \nsent by '{script}' from '{host}'").\
           format(account=account,
                  volname=volname,
                  mountpoint=mountpoint,
                  script=this_workflow,
                  host=this_host
           )
    
    send_generic_mail(subject=subject,
                      tos=[account],
                      body=body)

# FIXME extract
def send_generic_mail(subject, tos, body, sender='afs.migration@cern.ch'):
    msg = MIMEText(body)
    msg['Subject'] = subject
    msg['From'] = sender
    msg['To'] = ', '.join(tos)

    s = smtplib.SMTP('localhost')
    s.sendmail( msg['From'], tos, msg.as_string())
    s.quit()

if __name__ == "__main__":
    usage = """Look at DB, figure out next per-volume actions for scratch spaces."""
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    parser = ArgumentParser(usage=usage)
    parser.add_argument("--debug",
                        help="more verbose output",
                        action='store_true',
                    )
    parser.add_argument("--syslog",
                        help="also log to syslog",
                        action='store_true',
                    )
    parser.add_argument("--nowaitloop",
                        help="if set, do not wait for new data to arrive - just exit after processing all",
                        action='store_true',
                    )
    parser.add_argument("--noaction",
                        help="suppress sending mails, do not modify DB",
                        action='store_true',
                    )
    parser.add_argument("--waitsecs",
                        type=int,
                        help="how long [seconds] to wait between actions for a single volume",
                    )
    parser.add_argument("--noadmin",
                        action='store_true',
                        help="skip steps that need full AFS admin powers, beyond 'afs_admin'",
                    )
    args = parser.parse_args()
    if args.debug:
        console = logging.StreamHandler(sys.stdout)
        console.setLevel(logging.DEBUG)
        logger.addHandler(console)
    else:
        dblog = dblogger.DBHandler()
        dblog.setLevel(logging.INFO)
        logger.addHandler(dblog)

    if args.syslog:
        syslog = logging.handlers.SysLogHandler(address='/dev/log',
                                            facility=logging.handlers.SysLogHandler.LOG_DAEMON)
        syslog.setLevel(logging.INFO)
        logger.addHandler(syslog)
    
    if args.waitsecs:
        override_wait = timedelta(seconds=int(args.waitsecs))

    import afstracker.models as am
    run_periodic(logger, args, am)

