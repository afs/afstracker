from afstracker import afstracker
from flask import render_template

@afstracker.route("/")
def main():
    return render_template('index.html')

@afstracker.route("/login")
def login():
    return render_template('login.html')

@afstracker.route("/onevolume/<int:volid>/")
def onevolume(volid):
    vol={'volname':'scratch.iven'}
    return render_template('volume.html',volume=vol)

@afstracker.route("/myvolumes")
def myvolumes():
    user={'login':'iven', 'fullname':'Jan I'}
    vols=({'volname':'scratch.iven'},
          {'volname':'user.iven'},
     )
    return render_template('myvolumes.html',user=user,volumes=vols)

