# how to prepare databases

Default is a local sqlite3 DB

Production should use  MySQL, in DBoD.


$ mysql --host dbod-afstrack.cern.ch --port 5501 --user admin  --password 
CREATE DATABASE afstracker;

then
$ export DATABASE_URL="mysql://admin:PASSWD@dbod-afstrack.cern.ch:5501/afstracker"
$ ipython
from afstracker import db
db.create_all()


In "mysql", continue with

CREATE USER 'workflow' IDENTIFIED BY 'changeme' PASSWORD EXPIRE;
GRANT DELETE,INSERT,SELECT,UPDATE ON afstracker.volume_state to 'workflow';


CREATE USER 'web' IDENTIFIED BY 'changeme' PASSWORD EXPIRE;
GRANT SELECT,UPDATE ON afstracker.volume_state to 'web';

CREATE USER 'scripts' IDENTIFIED BY 'changeme' PASSWORD EXPIRE;
GRANT SELECT,INSERT ON afstracker.volume_state to 'scripts';

delimiter //
CREATE PROCEDURE reset_error(IN vn CHAR(64), IN state CHAR(32)) BEGIN   UPDATE volume_state SET workflow_state = state, workflow_activehost = NULL,workflow_next = NOW()   WHERE workflow_state = CONCAT('ERROR:', state) AND volname = vn; END//
delimiter ;


##### How to add columns
(as we are not using "migration")

## sqlite3 afstracker/afstracker.db
#sqlite> alter table volume_state add column workflow_activehost type VARCHAR(32);
#sqlite> .q

MySQL [afstracker]> alter table volume_state add column sourcevol VARCHAR(64);
## (slow operation!)
