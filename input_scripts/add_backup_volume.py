#!/usr/bin/python3

import site
site.addsitedir('..')

from datetime import datetime, timedelta
import afstracker.models as am
from argparse import ArgumentParser
import sys
import re

workflow_name='backup_volumes'
workflow_state='initial'

if __name__ == "__main__":
    parser = ArgumentParser(usage="add backup volume mountpoints for automatic cleanup")
    parser.add_argument("--debug",
                        help="more verbose output",
                        action='store_true',
                    )
    parser.add_argument("--waitsecs",
                        type=int,
                        default=60,
                        help="how long [seconds] to wait between actions for a single volume",
                    )
    group = parser.add_mutually_exclusive_group()

    group.add_argument("--volscan",
                       required=False,
                       action='store_true',
                       help="""take STDIN input in 'volume_mount_scan.pl' format:
VOLNAME (VOLID) -> MOUNTPOINT
""",
                    )
    group.add_argument("--mountpoint",
                       required=False,
                       help="the dangling mountpoint",
                    )
    # the ones below actually make sense for single-vol only.
    parser.add_argument("--volname",
                        required=False,  # True: mostly for logging and such
                        help="volume name",
                    )
    parser.add_argument("--owner",
                        help="optional: contact or owner",
                    )
    parser.add_argument("--project",
                        help="optional: project",
                    )
    args = parser.parse_args()
    now = datetime.now()

    next = now + timedelta(seconds=int(args.waitsecs))
    if args.volscan:
        for line in sys.stdin:
            m = re.search('^(\S+) \((\d+)\) -> (/afs.*)', line)
            if not m:
                print("WARN: skipping {}".format(line))
                continue
            line_volname = m.group(1)
            line_volid = m.group(2)  # not used here, but prevents matchin #REMOTE# #MISSING#
            line_mountpoint = m.group(3)
            volstate = am.VolumeState.query.filter(am.VolumeState.volname == line_volname).first()
            if volstate:
                print("WARN: skipping, volume {} already in DB".format(line_volname))
                continue
            dbvol = am.VolumeState(volname = line_volname,
                                   pathname = line_mountpoint,
                                   workflow_name = workflow_name,
                                   workflow_state = workflow_state,
                                   workflow_since = now,
                                   workflow_next = next,
                                   workflow_params = args.project,
                                   owner = args.owner,
                                   )
            am.db.session.add(dbvol)
        am.db.session.commit()
    else:
        if not ( args.mountpoint and args.volname):
            print("Error: need both mountpoint and volume name, unless reading from stdin")
            sys.exit(1)
        if not args.mountpoint.startswith('/afs/cern.ch/'):
            print("Error: mountpoint does not start with '/afs/cern.ch/'")
            sys.exit(1)
        volstate = am.VolumeState.query.filter(am.VolumeState.volname == args.volname).first()
        if volstate:
            print("Error: skipping, volume {} already in DB".format(line_volname))
            sys.exit(1)
        dbvol = am.VolumeState(volname=args.volname,
                               workflow_name = workflow_name,
                               workflow_state = workflow_state,
                               workflow_since = now,
                               workflow_next = next,
                               pathname = args.mountpoint,
                               workflow_params = args.project,
                               owner = args.owner,
                               workflow_defaultnextsec = args.waitsecs,
                           )
        am.db.session.add(dbvol)
        am.db.session.commit()
