#!/usr/bin/python3

import site
site.addsitedir('..')

from datetime import datetime, timedelta
import afstracker.models as am
from argparse import ArgumentParser
import sys
import re
import random

workflow_name='personal_scratch'

if __name__ == "__main__":
    parser = ArgumentParser(usage="add a personal scratch volume for automatic cleanup")
    parser.add_argument("--debug",
                        help="more verbose output",
                        action='store_true',
                    )
    parser.add_argument("--waitsecs",
                        type=int,
                        help="how long [seconds] to wait between actions for a single volume",
                    )
    parser.add_argument("--waitinitial",
                        type=int,
                        default=60,
                        help="how long [seconds] to wait before the first action",
                    )
    parser.add_argument("--waitsame",
                        type=int,
                        default=3*86400,
                        help="how long [seconds] to wait between volumes for the same user",
                    )
    group = parser.add_mutually_exclusive_group()

    group.add_argument("--volscan",
                       required=False,
                       action='store_true',
                       help="""take STDIN input in 'volume_mount_scan.pl' format:
VOLNAME (VOLID) -> MOUNTPOINT
""",
                    )
    group.add_argument("--mountpoint",
                        required=False,
                        help="the scratchvol mountpoint",
                    )

    parser.add_argument("--volname",
                        required=False,
                        help="scratch volume name",
                    )
    parser.add_argument("--owner",
                        required=False,
                        help="user or owner",
                    )
    parser.add_argument("--project",
                        required=True,
                        help="AFS project that had allocated this space",
                    )
    parser.add_argument("--state",
                        choices=['initial', 'copy_perhaps_after_wait', 'unmount_after_wait'],
                        default='initial',
                        help="At which stage the workflow should start (default:initial)",
                    )
    args = parser.parse_args()

    now = datetime.now()

    next = now + timedelta(seconds=int(args.waitinitial))

    if args.volscan:
        last_for_account = dict()
        for line in sys.stdin:
            m = re.search('^(\S+) \((\d+)\) -> (/afs.*)', line)
            if not m:
                print("WARN: skipping {}".format(line))
                continue
            line_volname = m.group(1)
            line_volid = m.group(2)  # not used here, but prevents matchin #REMOTE# #MISSING#
            line_mountpoint = m.group(3)
            line_account = None
            m = re.search('^/afs/cern.ch/(user|work)/./([^/]+)/', line_mountpoint)
            if not m:
                print("WARN: skipping non-user|non-work {}".format(line_mountpoint))
                continue
            line_account = m.group(2)

            volstate = am.VolumeState.query.filter(am.VolumeState.volname == line_volname).first()
            if volstate:
                print("WARN: skipping, volume {} already in DB".format(line_volname))
                continue

            vol_next = next
            if line_account in last_for_account:
                # idea is to stagger out operations into the same account, without making everybody else wait
                vol_next = last_for_account[line_account] +  timedelta(seconds=int(args.waitsame))

            dbvol = am.VolumeState(volname = line_volname,
                                   pathname = line_mountpoint,
                                   workflow_name = workflow_name,
                                   workflow_state = args.state,
                                   workflow_since = now,
                                   workflow_next = vol_next,
                                   workflow_params = args.project,
                                   owner = line_account,
                                   workflow_defaultnextsec = args.waitsecs,
            )
            last_for_account[line_account] = vol_next
            next += timedelta(seconds=random.randint(1,59))
            am.db.session.add(dbvol)
        am.db.session.commit()
    else:
        if not ( args.owner and args.mountpoint and args.volname):
            print("Error: need owner, mountpoint and volume name, unless reading from stdin")
            sys.exit(1)
        if not args.mountpoint.startswith('/afs/cern.ch/'):
            print("Error: mountpoint does not start with '/afs/cern.ch/'")
            sys.exit(1)
        volstate = am.VolumeState.query.filter(am.VolumeState.volname == args.volname).first()
        if volstate:
            print("Error: skipping, volume {} already in DB".format(line_volname))
            sys.exit(1)
        dbvol = am.VolumeState(volname=args.volname,
                               workflow_name = workflow_name,
                               workflow_state = args.state,
                               workflow_since = now,
                               workflow_next = next,
                               workflow_defaultnextsec = args.waitsecs,
                               pathname = args.mountpoint,
                               workflow_params = args.project,
                               owner = args.owner,
        )
        am.db.session.add(dbvol)
        am.db.session.commit()
