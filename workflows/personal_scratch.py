#!/usr/bin/python3
#
# generic workflow for "personal" AFS scratch/user/work space granted from a project quota
## DB 'workflow_params' is the project name
## per-project mail texts with format placeholders can be in a subdirectory ./scratch_blurbs/$project.txt 
## else a generic text will be used

import os
import socket
from datetime import datetime, timedelta
import time
import random
import sys
import re
# EMail
import smtplib
from email.mime.text import MIMEText

from argparse import ArgumentParser 
import subprocess
import tempfile
import logging
import logging.handlers
import dblogger

import afsutil.volinfo as Volinfo
from afsutil.phonebook import phonebook as Phonebook
from afsutil.volutil import Volutil

default_wait = timedelta(days=30)

debug = False

max_sleep = 5*60
adminaccount = 'afsmigr'  # gets mail in case something blows up

this_workflow = 'personal_scratch'
this_host = socket.gethostname()
this_process = os.getpid() 
soft_exit_file = '/var/run/'+this_workflow+'.soft_exit'

def soft_exit_perhaps(log):
    if os.path.exists(soft_exit_file):
        log.info("soft exit for workflow processor {}".format(this_workflow))
        try:
            os.remove(soft_exit_file)
        except OSError:
            pass
        import sys
        sys.exit(33)  # error, so that systemd restarts us

def run_periodic(log, args, am):
    cur_sleep = 0
    log.info("workflow processor for {} started".format(this_workflow))
    while True:
        soft_exit_perhaps(log)
        now = datetime.now()
        am.db.session.commit()  # to get fresh objects
        volstate = am.VolumeState.query.filter(am.VolumeState.workflow_name == this_workflow,
                                               am.VolumeState.workflow_activehost == None,
                                               am.VolumeState.workflow_next <= now).first()
        if not volstate:
            if args.nowaitloop:
                return

            cur_sleep = min(max_sleep, cur_sleep * 2) + random.randint(0, 5)
            log.debug('no data found, sleeping for {}secs'.format(cur_sleep))
            time.sleep(cur_sleep)
            continue

        cur_sleep = 0

        # lock the record for us. Need to push to DB, and check for races
        identifier = ':'.join([this_host, str(this_process)])
        volname = volstate.volname # might be gone afterwards..
        volstate.workflow_activehost = identifier
        am.db.session.commit()
        volstate = am.VolumeState.query.filter(am.VolumeState.workflow_name == this_workflow,
                                               am.VolumeState.volname == volname,
                                               am.VolumeState.workflow_activehost == identifier).first()
        if not volstate:
            log.warning("could not lock record for {}".format(volname))
            continue
        log.debug("record for {} now locked by {}.".format(volstate.volname, identifier))

        project = volstate.workflow_params
        log.info("looking at volume '{}' from project '{}' in state '{}'".format(volstate.volname, project, volstate.workflow_state))

        next_actiondate = now + default_wait
        if volstate.workflow_defaultnextsec:
            next_actiondate = now + timedelta(seconds=volstate.workflow_defaultnextsec)
        if args.waitsecs >= 0:
            next_actiondate = now + timedelta(seconds=int(args.waitsecs))

        v = Volinfo.Volinfo(volstate.volname, initial_mountpoints=[volstate.pathname])

        # terminal state - volume is gone, also remove from state DB
        if not v:
            log.info("volume {} does not exist (anymore) in VLDB. Over.".format(volstate.volname))
            am.db.session.delete(volstate)
            continue

        if volstate.workflow_state == 'unmount_after_wait':
            mount = '(undef)'
            try:
                for mount in v.get_verified_mountpoints():
                    # fs rmmount -dir ...
                    Volutil.delete_mount(mount)
                    log.info("removed mountpoint '{}' for {}.".format(mount, volstate.volname))
                log.debug("no more verified mountpoints for {}. Now waiting until volume deletion.".format(volstate.volname))
                volstate.workflow_next = next_actiondate
                volstate.workflow_state = 'delete_after_wait'
                volstate.workflow_activehost = None
            except subprocess.CalledProcessError as e:
                volstate.workflow_state = 'ERROR'
                volstate.workflow_next = None
                log.error('error while removing mountpoint {} for {}: {}'.format(mount, volstate.volname, e.output))

        elif volstate.workflow_state == 'delete_after_wait':
            try:
                Volutil.delete_volume(volstate.volname)
                log.info("INFO: deleted volume '{}' in VLDB. Over.".format(volstate.volname))
                am.db.session.delete(volstate)
                continue
            except subprocess.CalledProcessError as e:
                volstate.workflow_state = 'ERROR'
                volstate.workflow_next = None
                log.error('error while deleting {}: {}'.format(volstate.volname, e.output))

        # can get to here directly from start, or after first MAIL
        # with volstate.workflow_state = 'copy_perhaps_after_wait'
        elif v.is_unmounted() and volstate.workflow_state in ['initial',  'copy_perhaps_after_wait']:
            state  = []
            if v.is_empty():
                state.append('empty')
            if v.is_unmounted():
                state.append('already unmounted')
            state_text = ' and '.join(state)
            log.info("volume '{}' is {}. Now waiting until volume deletion.".format(volstate.volname, state_text))

            volstate.workflow_next = next_actiondate
            volstate.workflow_state = 'delete_after_wait'
            volstate.workflow_activehost = None

        elif  v.is_empty() and volstate.workflow_state in ['initial',  'copy_perhaps_after_wait']:
            log.info("volume '{}' is empty. Proceed directly to unmount.".format(volstate.volname))

            volstate.workflow_next = datetime.now()
            volstate.workflow_state = 'unmount_after_wait'
            volstate.workflow_activehost = None

        # not empty and still mounted. Have we talked to the user before?
        elif volstate.workflow_state == 'initial':
            # need first mail, unless the account is blocked
            log.debug("volume '{}' needs a first mail to be sent".format(volstate.volname))
            action_send_firstmail(log=log, volume=v, volstate=volstate, nextdate=next_actiondate)
            # can result in state 'copy_perhaps_after_wait' or 'ERROR'

        elif volstate.workflow_state == 'copy_perhaps_after_wait':
            # this is the timeout after the first MAIL
            # the volume might be gone or empty in the meantime (handled above), hence "maybe"

            log.debug("volume '{}' will be incorporated into owner homedir.".format(volstate.volname))
            action_copy_data_to_home (log=log, volstate=volstate, scratchvol=v, nextdate=next_actiondate)
            # will go to 'delete_after_wait' (or 'ERROR')

        else:
            assert(False),"unexpected workflow state {} for volume {}".format(volstate.workflow_state, volstate.volname)


        # push back changed data for this volume to DB
        am.db.session.commit()


#################################################
def send_generic_mail(subject, to, body, sender='afs.migration@cern.ch'):
    msg = MIMEText(body)
    msg['Subject'] = subject
    msg['From'] = sender
    msg['To'] = to

    if debug:
        print(msg)
    s = smtplib.SMTP('localhost')
    s.sendmail( msg['From'], msg['To'], msg.as_string())
    s.quit()

def action_send_firstmail(log, volume, volstate, nextdate):
    # send first MAIL to user
    ## who is the user? where is this mounted?
    ## assumptions here.

    project = volstate.workflow_params
    home_mount = [ m for m in volume.get_verified_mountpoints() if m.startswith('/afs/cern.ch/user/') or m.startswith('/afs/cern.ch/work/')].pop()
    account = home_mount.split('/')[5]
    first,last,email = Phonebook.get_name_email(account)

    if not home_mount or not account or not first or not last:
        log.error("cannot send mail for {}, missing info".format(volstate.volname))
        volstate.workflow_state = 'ERROR'
        return

    if Phonebook.is_disabled(account) and Phonebook.get_accounttype(account) == 'Primary':
        # no use mailing a 'disbaled' user, cannot act on this. Also no need to wait.
        # for service and secondary ccounts, the primary user might get the mail
        # and could reset the account
        log.warning("did not send initial mail to disabled primary account '{}' for volume '{}'".format(account, volstate.volname))
        volstate.workflow_next = datetime.now()
        volstate.workflow_state = 'copy_perhaps_after_wait'
        volstate.workflow_activehost = None
        return

    thisdir = os.path.dirname(__file__)
    try:
        with open(thisdir+'/scratch_blurbs/'+project+'-initialmail.txt') as blurbfile:
                blurb = blurbfile.read()
    except IOError:
        with open(thisdir+'/scratch_blurbs/GENERIC-initialmail.txt') as blurbfile:
            blurb = blurbfile.read()

    subject = '{project} personal AFS scratch space phaseout for "{account}"'.\
              format(project=project, account=account)
    body = ( blurb + "\n-- \nsent by '{script}' from '{host}' for '{volname}'").\
           format(first=first,
                  last=last,
                  account=account,
                  project=project,
                  quota=int(volume.maxquota/1000),   # text says 'MB'
                  nextdate=(nextdate - timedelta(days=1)).date(),  # text says "after DATE"
                  path=home_mount,
                  script=this_workflow,
                  host=this_host,
                  volname=volstate.volname
           )

    send_generic_mail(subject=subject,
                      to=email,
                      body=body)

    log.info("sent initial mail to '{}' for volume '{}'".format(account, volstate.volname))
    volstate.workflow_next = nextdate
    volstate.workflow_state = 'copy_perhaps_after_wait'
    volstate.workflow_activehost = None

def action_send_admin_error_mail(log, account, mailto, project, scratchvolname, path, errors, remarks):
    _,_,email = Phonebook.get_name_email(mailto)
    subject = 'ERROR: personal AFS scratch space workflow failed for "{account}" - {volume}'.\
              format(account=account, volume=scratchvolname)

    body = """The '{script}' workflow for
project= '{project}'
account= '{account}'
volume=  '{volname}'
path=    '{path}'
failed and needs manual action (workflow_state=ERROR):
{errors}
Remarks:
{remarks}
-- 
sent by '{script}' from '{host}' for '{volname}'""".\
    format(account=account,
           project=project,
           volume=scratchvolname,
           path=path,
           errors=errors,
           remarks=remarks,
           script=this_workflow,
           host=this_host,
           volname=scratchvolname
    )
    send_generic_mail(subject=subject,
                      to=email,
                      body=body)


def action_send_user_final_mail(log, account, project, scratchvolname, path, errors, remarks, nextdate):
    first,last,email = Phonebook.get_name_email(account)
    if Phonebook.is_disabled(account):
        log.warning("did not send final mail to disabled account '{}'".format(account))
        return

    thisdir = os.path.dirname(__file__)
    try:
        with open(thisdir+'/scratch_blurbs/'+project+'-finalmail.txt') as blurbfile:
                blurb = blurbfile.read()
    except IOError:
        with open(thisdir+'/scratch_blurbs/GENERIC-finalmail.txt') as blurbfile:
            blurb = blurbfile.read()

    subject = 'Done: {project} personal AFS scratch space for "{account}" migrated'.\
              format(project=project, account=account)
    if errors:
        subject = 'ERROR: {project} personal AFS scratch space for "{account}"'.\
              format(project=project, account=account)
        errors = "** Errors **\n"+errors

    body = ( blurb + "\n-- \nsent by '{script}' from '{host}' for '{volname}'").\
           format(first=first,
                  last=last,
                  account=account,
                  project=project,
                  volume=scratchvolname,
                  nextdate=(nextdate - timedelta(days=1)).date(),  # text says "after DATE"
                  path=path,
                  errors=errors,
                  remarks=remarks,
                  script=this_workflow,
                  host=this_host,
                  volname=scratchvolname
           )
    send_generic_mail(subject=subject,
                      to=email,
                      body=body)


def action_copy_data_to_home (log, volstate, scratchvol, nextdate):
    """DEFAULT_ACTION:
 * figure out the correct mountpoint (homedir)
 * get size on source
 * check whether user has workarea? use that (and create symlink in home). No.
 * perhaps: increase quota somewhat in homedir.
 * check for submounts in source, skip?
 * mkdir homedir/mountpount-newtmpname
 * cp -a  homedir/mountpoint homedir/mountpount-newtmpname
 * fs copyacl -fromdir omedir/mountpoint -todir homedirmountpount-newtmpname  -clear
 * umount mountpoint
 * mv mountpount-newtmpname mountpoint"""

    account = volstate.owner
    project = volstate.workflow_params
    scratchpath = volstate.pathname

    step = 'initializing'
    homevol = None
    homevol_mount = None
    if scratchpath.startswith('/afs/cern.ch/user/'):
        homevol = Volinfo.Volinfo('user.'+account)
        homevol_mount =  [ m for m in homevol.get_verified_mountpoints() if m.startswith('/afs/cern.ch/user/')].pop()
    elif scratchpath.startswith('/afs/cern.ch/work/'):
        homevol = Volinfo.Volinfo('work.'+account)
        homevol_mount =  [ m for m in homevol.get_verified_mountpoints() if m.startswith('/afs/cern.ch/work/')].pop()

    if( not homevol_mount or not homevol ):
        log.error("cannot find user or work mount for scratch volume {} mounted on {}".format(scratchvol.name, scratchpath))
        volstate.workflow_state = 'ERROR'
        return

    infomail = ''
    errormail = ''
    sub_mountpoints = []
    step = "starting"
    rsync_out = ''
    fscopyacl_out = ''

    try:
        # is the volume actually still mounted where we expect it? it is still mounted 'somewhere'
        # do we automatically change to the new path (which needs to be in the same tree?), or fail and hope for retry?
        # fail is safer.
        if not scratchpath in scratchvol.get_verified_mountpoints():
            log.error("cannot find scratch volume {} at specified {}, might be at {}".format(scratchvol.name, scratchpath, str(scratchvol.get_verified_mountpoints())))
            volstate.workflow_state = 'ERROR'
            return

        # how much quota do we need on destination? Options:
        # * max: just add scratch quota.            Bad: bigger homevolume; might be not needed
        # * keep: keep current free space.          Bad: not reacting gets the user a quota boost
        # * minpercent: make new fit, keep at least 10% free <---- will use this
        # * min: just make the new content fit in.  Bad: will run out of quota right afterwards (unhappy+ticket)

        homevol_minfree = int(homevol.maxquota * 0.1)
        homevol_required = homevol.diskused + scratchvol.diskused + homevol_minfree
        if (homevol.maxquota < homevol_required):
            if Volutil.running_as_admin():
                step="increasing the target AFS quota"
                log.info("{} on {} from {} to {}=({}+{}+{})".format(
                    step, homevol.name, homevol.maxquota,
                    homevol_required, homevol.diskused,
                    scratchvol.diskused, homevol_minfree ))
                Volutil.set_vol_quota(homevol.name, homevol_required)
            else:
                log.warning("not admin: not increasing target AFS quota on {} from {} to {}=({}+{}+{})".format(
                    homevol.name, homevol.maxquota, homevol_required,
                    homevol.diskused, scratchvol.diskused,
                    homevol_minfree ))
        else:
            log.info("quota on {} is OK: have {}, only need {}=({}+{}+{})".format(
                homevol.name, homevol.maxquota,
                homevol_required, homevol.diskused,
                scratchvol.diskused, homevol_minfree ))
        # any surprises in that tree?
        step="looking for contained mountpoints"
        sub_mountpoints = Volutil.get_mounts_under_dir(scratchpath)
        exclusion_filename = None
        exclusion_relative_dirs = []
        if sub_mountpoints:
            temp_fd = tempfile.NamedTemporaryFile(prefix='afs_scratch_exclude_', delete=False)
            exclusion_filename = temp_fd.name
            for mountpoint in sub_mountpoints:
                relative_mountpoint = mountpoint[len(scratchpath)+1:]
                exclusion_relative_dirs.append(relative_mountpoint)
                temp_fd.write("{}/\n".format(relative_mountpoint))
            temp_fd.close()
            log.info("{}: {} found {:d} mountpoints under {}".format(step, scratchvol.name, len(sub_mountpoints), scratchpath))

        # fixup files without permissions (as these cause rsync to fail). Slow.
        # assume that we have none or few
        step="finding unreadable files"
        find_cmd = ['find', scratchpath]
        if exclusion_relative_dirs:
            find_cmd.append('(')
            for arg in exclusion_relative_dirs:
                find_cmd += ['-path', arg, '-prune', '-o']
            find_cmd[-1] = ')'
        find_cmd += ['-type', 'f', '!', '-perm', '-400', '-print0']
        find_out = subprocess.check_output(find_cmd, stderr=subprocess.STDOUT,
                                           universal_newlines=True)
        step="fixing unreadable files"
        for unreadable in find_out.split('\000'):
            if unreadable:
                subprocess.check_call(['chmod', 'u+r', unreadable], stderr=subprocess.STDOUT)
                log.warning("made file '{}' readable".format(unreadable))

        # fix parent-level dir perms, if needed
        scratch_parent = os.path.dirname(scratchpath.rstrip('/'))
        step="check AFS admin rights on scratch mountpoint directory"
        fs_la_cmd=['fs','listacl','-path', scratch_parent]
        fs_la_out = subprocess.check_output(fs_la_cmd, stderr=subprocess.STDOUT,
                                            universal_newlines=True)
        if not re.search('system:administrators rlidwka', fs_la_out):
            if Volutil.running_as_admin():
                step="fix AFS admin rights on scratch mountpoint directory"
                log.info("{} on {}".format(step, scratch_parent))
                Volutil.set_acl(scratch_parent, 'system:administrators', 'rlidwka')
            else:
                log.warning("not admin: not fixing AFS ACLs on scratch mountpoint directory {}".format(scratch_parent))

        # actual data move
        # create the new directory
        step="creating the new (temporary) directory"
        temp_dir = tempfile.mkdtemp(prefix='scratch_', dir=homevol_mount)
        log.debug("will copy {} into {}".format( scratchpath, temp_dir))

        # copy shtuff
        step="copying the data"
        rsync_cmd = ['rsync', '--recursive', '--links', '--perms', '--times', '--whole-file',
                     '--update', '--quiet', '--numeric-ids', '--one-file-system',
                     '--stats', '--timeout=1800']
        if exclusion_filename:
            rsync_cmd += ['--exclude-from='+exclusion_filename]

        # copy the data, first attempt
        rsync_cmd += [scratchpath+'/', temp_dir+'/']
        try:
            rsync_out = subprocess.check_output(rsync_cmd, stderr=subprocess.STDOUT,
                                                universal_newlines=True)
        except subprocess.CalledProcessError as e:
            if 'some files/attrs were not transferred' in e.output:
                # workaround for temporary read errors on AFS
                step="copying the data, second attempt"
                rsync_out = subprocess.check_output(rsync_cmd, stderr=subprocess.STDOUT,
                                                    universal_newlines=True)
            else:
                raise

        log.info("data copy for {} done".format(scratchvol.name))

        # copy the ACLs
        step="copying the AFS ACLs"
        fscopyacl_cmd = [
            'afind', '.',  # we chwd() here
            '-t', 'd',
            '-e', 'fs copyacl -clear -fromdir {} -todir '+ temp_dir +'/{}']

        log.debug("will now copy ACLs for {} from {} into {}".format(scratchvol.name, scratchpath, temp_dir))
        fscopyacl_out = subprocess.check_output(fscopyacl_cmd, stderr=subprocess.STDOUT,
                                                cwd=scratchpath,
                                                universal_newlines=True)
        log.info("ACL copy for {} done".format(scratchvol.name))

        # rename the old mountpoint - this might clash?
        # prefer a 'named' mountpoint, but fall back to some temp junk - the old mountpoint needs to be out of the way
        step="renaming the old mountpoint"
        oldmountname = scratchpath+'_old'
        if os.path.exists(oldmountname):
            log.warning("renamed mountpoint for {} already found at {}, will use temp name".format(scratchvol.name, oldmountname))
            # this is 'insecure'/race condition, but the 'mv' will fail if we're already mounted there, or not on AFS
            oldmountname = tempfile.mktemp(prefix=scratchvol.name, suffix='.old',  dir=homevol_mount)
        log.debug("will now rename old mountpoint for {} from {} to {}".format(scratchvol.name, scratchpath, oldmountname))
        out = subprocess.check_output(['mv', scratchpath, oldmountname], stderr=subprocess.STDOUT,
                                      universal_newlines=True)

        # if the above succeeds, user might see errors from anything below

        # rename the new directory to old mountpoint name
        step="renaming the new directory"
        log.debug("will now rename new directory for {} from {} to {}".format(scratchvol.name, scratchpath, oldmountname))
        out = subprocess.check_output(['mv', temp_dir, scratchpath], stderr=subprocess.STDOUT,
                                      universal_newlines=True)

        # try to prevent new writes to scratchvol, by setting quota to "1" ("0" would mean no quota)
        # could do before, but this is user-visible.
        if Volutil.running_as_admin():
            step="resetting the AFS source quota"
            log.info("{} on {} to {}".format(step, scratchvol.name, 1))
            Volutil.set_vol_quota(scratchvol.name, 1)
        else:
            log.warning("not admin: not resetting source AFS volume quota")

        ### unmount old (or wait?)
        step="unmounting the scratch volume"
        log.debug("will now unmount {} for {}".format( oldmountname, scratchvol.name)) 
        Volutil.delete_mount(oldmountname)
        log.info("rename/unmount of {} done for {}".format( scratchpath, scratchvol.name))
        ## cleanup
        if exclusion_filename:
            os.unlink(exclusion_filename)

    except subprocess.CalledProcessError as e:
        errormail = "The automatic copy of {} into {} failed while {}.\nThis may need manual action - please review the errors below.\n".format(scratchvol.name,
                                                                                                                                                homevol_mount,
                                                                                                                                                step)
        errormail += str(e)+"\n"+str(e.output)+"\n"
        if rsync_out:
            errormail += "'rsync' output:\n"+rsync_out
        if fscopyacl_out:
            errormail += "'fs copyacl' output:\n"+ fscopyacl_out
        log.error("The automatic copy of {} into {} failed while {}: {} Output:{}".format(scratchvol.name,
                                                                                   homevol_mount,
                                                                                   step,
                                                                                   str(e),e.output))

    if sub_mountpoints:
        infomail += "Please note that this area contained AFS mountpoints for other volumes - they have not been copied.\n"
        infomail += "These were mounted at:\n"
        infomail += "\n".join(sub_mountpoints)

    #  outcomes:
    # - error: tell admin. Stop.
    # - no error: tell just the user.

    
    if errormail:
        action_send_admin_error_mail(log=log,
                                     account=volstate.owner,
                                     mailto=adminaccount,
                                     project=project,
                                     scratchvolname=scratchvol.name,
                                     path=volstate.pathname,
                                     errors=errormail,
                                     remarks=infomail)
        volstate.workflow_state = 'ERROR'
        volstate.workflow_next = None
        return
    
    # no errors?
    action_send_user_final_mail(log=log,
                                account=volstate.owner,
                                project=project,
                                scratchvolname=scratchvol.name,
                                path=volstate.pathname,
                                errors=errormail,
                                remarks=infomail,
                                nextdate=nextdate)

    volstate.workflow_next = nextdate
    volstate.workflow_state = 'delete_after_wait'
    volstate.workflow_activehost = None

if __name__ == "__main__":
    usage = """Look at DB, figure out next per-volume actions for scratch spaces."""
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    parser = ArgumentParser(usage=usage)
    parser.add_argument("--debug",
                        help="more verbose output",
                        action='store_true',
                    )
    parser.add_argument("--syslog",
                        help="also log to syslog",
                        action='store_true',
                    )
    parser.add_argument("--nowaitloop",
                        help="if set, do not wait for new data to arrive - just exit after processing all",
                        action='store_true',
                    )
    parser.add_argument("--waitsecs",
                        type = int,
                        default = -1,
                        help="how long [seconds] to wait between actions",
                    )
    args = parser.parse_args()
    if args.debug:
        console = logging.StreamHandler(sys.stdout)
        console.setLevel(logging.DEBUG)
        logger.addHandler(console)
    else:
        dblog = dblogger.DBHandler()
        dblog.setLevel(logging.INFO)
        logger.addHandler(dblog)

    if args.syslog:
        syslog = logging.handlers.SysLogHandler(address='/dev/log',
                                            facility=logging.handlers.SysLogHandler.LOG_DAEMON)
        syslog.setLevel(logging.INFO)
        logger.addHandler(syslog)

    import afstracker.models as am
    run_periodic(logger, args, am)

