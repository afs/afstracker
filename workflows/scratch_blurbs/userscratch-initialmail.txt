Dear {first} {last},

your account '{account}' has in the past received personal AFS 'scratch space', with a quota of {quota}MB.
This space is currently mounted under {path} and now needs to be removed as part of the overall AFS phaseout at CERN.

Could you please review the content in this area, and migrate anything that needs to be kept to either

* EOS/CERNBox (for online files), or
* your existing AFS homedirectory or AFS workspace (in case AFS access is still required), or
* CASTOR (as tarball, for archiving).

Once this area contains nothing more of interest, please simply unmount it via
 fs rmmount {path}

If after {nextdate} this area is found to be unmounted (or is empty), it will be removed.
By default the remaining content will be copied into your AFS home volume (with minimal quota extension to make it "fit"). Please note that this might cause temporary write errors.

In case you have questions on this phaseout procedure, please open a ticket:
    https://cern.service-now.com/service-portal/report-ticket.do?name=request&fe=AFS

Thanks in advance,
  jan iven (AFS phaseout) 

Further information:
* overall AFS phaseout is tracked at https://its.cern.ch/jira/browse/NOAFS
* phaseout for such 'user_scratch' areas is tracked at https://its.cern.ch/jira/browse/NOAFS-63
* background information on the phaseout:
  https://twiki.cern.ch/twiki/bin/view/IT/AfsPhaseout
* AFS 'user_scratch' spaces were used before CERN handed out "AFS workspaces" via self-service, see
  https://cern.service-now.com/service-portal/article.do?n=KB0001084
* personal EOS/CERNBox space can be requested by connecting to http://cernbox.cern.ch
  CERNBox Documentation is at http://cern.ch/cernbox-manual/
* AFS homedirectory or workspace quotas can be extended (within limits) via
  https://resources.web.cern.ch/resources/Manage/AFS/Settings.aspx?login={account}
* a CASTOR tutorial can be found at https://cern.service-now.com/service-portal/article.do?n=KB0001103

