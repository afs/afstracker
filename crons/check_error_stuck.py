#!/usr/bin/python3
## run me as a cronjob, prints to STDOUT

import site
site.addsitedir('..')

from datetime import datetime, timedelta
from argparse import ArgumentParser

MAX_STAGE_DURATION=86400*7


def check_error(args, am):
    errors = am.VolumeState.query.filter(am.VolumeState.workflow_state.like('ERROR%'))
    retval = []
    for volstate in errors:
        retval.append(str(volstate))
    return retval

def check_stuck(args, am):
    best_before = datetime.now() -  timedelta(seconds=int(args.maxstagesecs))
    stucked =  am.VolumeState.query.filter(am.VolumeState.workflow_activehost != None,
                                           am.VolumeState.workflow_next < best_before)
    retval = []
    for volstate in stucked:
        retval.append(str(volstate))
    return retval


if __name__ == "__main__":
    parser = ArgumentParser(usage="check afstracker DB for reported errors and stuck workflows")
    parser.add_argument("--debug",
                        help="more verbose output",
                        action='store_true',
                    )
    parser.add_argument("--maxstagesecs",
                        type=int,
                        default=MAX_STAGE_DURATION,
                        help="how long can a single workflow action take before considered STUCK [seconds]",
                    )

    args = parser.parse_args()
    best_before = datetime.now() -  timedelta(seconds=int(args.maxstagesecs))

    import afstracker.models as am

    ret = check_error(args=args, am=am)
    if ret:
        print("ERRORs found in the 'afstracker' state database:")
        print("\n".join(ret))
    elif args.debug:
        print("no ERRORs found in the 'afstracker' state database")

    ret = check_stuck(args=args, am=am)
    if ret:
        print("STUCK workflows found  in the 'afstracker' state database:")
        print("\n".join(ret))
    elif args.debug:
        print("no STUCK workflows found in the 'afstracker' state database")
