# afstracker

Tool to track slow per-volume actions (AFS archival, copy to EOS, remove spurious backup volume etc)

Does not actually contain a website :-/ , but is functional.
* Database: dbod-afstrack.cern.ch
* `afssrv/client/afstrack/web` : add new work via scripts in `/opt/afstracker/input_scripts/`
* `afssrv/client/afstrack/work`: machines are running those workflows marked as production-ready, and will pick up new work from the DB. See `# systemctl status afstrack_workflow@\*`

## Testing

(Note: the tests have baked-in assumptions on certain AFS areas/homedires being present and accessible..)
```
afstracker/$ python3 -m unittest discover
```
