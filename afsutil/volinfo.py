#!/usr/bin/python3
# AFS volume status, based on the "vos" command
import subprocess
import time
import os
import re

def homedir_empty_except_for_known_files(directory):
    """true if directory only contains files known to be auto-created:
    private/ (empty)
    public/.forward
    .bashrc
    .bash_profile
    """
    is_empty = True
    try:
        for name in os.listdir(directory):
            fullpath = os.path.join(directory, name)
            if os.path.isdir(fullpath):
                if name == 'private' and not os.listdir(fullpath):
                    pass
                elif name == 'public':
                    publicnames = os.listdir(fullpath)
                    if not publicnames:
                        pass
                    elif publicnames == ['.forward']:
                        pass
                    else:
                        is_empty = False
                else:
                    is_empty = False
            else:
                if os.path.isfile(fullpath) and name in ['.bashrc', '.bash_profile', '.uco-newuser.init']:
                    pass
                elif os.path.islink(fullpath) and name in ['.forward']:
                    pass
                else:
                    is_empty = False
            if not is_empty:
                # first unexpected file is sufficient
                break
    except OSError as e:
        print("ERROR: cannot look at {}:{}".format(directory,str(e)))
        is_empty = False
    return is_empty


class Volinfo(object):
    _statuscache = dict();  # cache

    _afs_tmp_mounts = "/afs/cern.ch/project/afs/var/tmp/find-empty-vols"

    _ignored_filepatterns_for_volpattern = [
        ('^s\.cms\.', '^.afs-phaseout-reclaim'), # per https://its.cern.ch/jira/browse/NOAFS-534
        ('^user\.|^s.afs.testemptyhome$',  homedir_empty_except_for_known_files),  # function, defined before
    ]
       

    # try to run 'vos' only once per volume, cache result. Allow to update mountpoint!
    def __new__(cls, volname, initial_mountpoints=[]):
        if volname in Volinfo._statuscache:
            oldvol =  Volinfo._statuscache[volname]
            if oldvol._initial_mounts != initial_mountpoints:
                oldvol._initial_mounts = initial_mountpoints
                oldvol.mounts = []
            return oldvol
        vol = super(Volinfo, cls).__new__(cls)
        vol._initial_mounts = initial_mountpoints
        data = Volinfo.get_vos_status(volname)
        if data:
            for key, val in list(data.items()):
                setattr(vol, key, val)
            Volinfo._statuscache[volname] = vol
            setattr(vol, 'mounts', [])  # placeholder for mountpoints
            return vol
        return None

    @classmethod
    def get_vos_status(cls,volname):
        vos_status = dict()
        try:
            out = subprocess.check_output(['vos',
                                           'examine',
                                           '-format',
                                           '-noauth',
                                           '-id', volname],
                                          universal_newlines=True)
            for line in out.splitlines():
                m = re.search('^(\w+)\s+(\S+)', line)
                if not m:
                    continue
                k = m.group(1)
                v = m.group(2)
                if v.isdigit():
                    v = int(v)
                if k.endswith('Date'):
                    v = time.localtime(v)
                vos_status[k] = v

        except subprocess.CalledProcessError:
            pass
        return vos_status


    @classmethod
    def empty_except_for_pattern(cls, topdirectory, ignore_pat):
        """check directory against non-empty to-ignore pattern"""
        topdirlen = len(topdirectory)
        for root, dirs, files in os.walk(topdirectory):
            relative_root = root[topdirlen:]
            if not ignore_pat and ( files or dirs ):
                return False
            nonmatching = [x for x in files + dirs if not re.search(ignore_pat, os.path.join(relative_root, x))]
            if nonmatching:
                return False
        return True

    # fast test, anon. 'filecount' is somewhat unreliable
    def is_probably_empty(self):
        if(self.diskused <= 10 and self.filecount <= 10):
            return True
        return False

    # real/slow test. Needs admin powers
    def is_really_empty(self):
        tmpmount = Volinfo._afs_tmp_mounts + '/vol_' + self.name
        # can fail, which means we have a leftover mountpoint
        subprocess.call(['fs',
                         'mkmount',
                         '-dir', tmpmount,
                         '-vol', self.name])
        is_empty = False
            
        # add a catch-all pattern that checks for really empty volumes
        for vol_pattern, func_or_file_pattern in Volinfo._ignored_filepatterns_for_volpattern + [('','')]:
            if re.search(vol_pattern, self.name):
                try:
                    # in complicated cases, we have a function
                    is_empty = func_or_file_pattern(tmpmount)
                except TypeError:
                    # else a regex
                    is_empty = Volinfo.empty_except_for_pattern(tmpmount, func_or_file_pattern)
                break  # only match one pattern

        subprocess.check_call(['fs',
                               'rmmount',
                               '-dir', tmpmount])
        return is_empty

    def is_empty(self):
        return self.is_probably_empty() and self.is_really_empty()

    def get_volscan_mountpoints(self):
        from afsutil.volutil import Volutil
        mounts = Volutil.get_mounts_for_volume(self.name)
        return mounts
                        
    def get_guessed_mountpoints(self):
        """all the name-based guesswork for where a volume *could* be mounted"""
        # input: volname-pattern with group(s), 
        #  -> use lambda, with array as argument (regexp and lambda need to agree on the number)
        #                returns [array of] possible mountpoint[s] that then need checking

        guess_volpat2mount = [
            # home directory
            ('^user\.(\w+)$',
             lambda m: ['/afs/cern.ch/user/'+m[0][0]+'/'+m[0] ] ),
            # work directory
            ('^work\.(\w+)$',
             lambda m: ['/afs/cern.ch/work/'+m[0][0]+'/'+m[0] ]),
            # project / experiment root vol
            ('^p\.([^.]+)\.root$',
             lambda m: ['/afs/cern.ch/project/'+m[0], '/afs/cern.ch/exp/'+m[0], '/afs/cern.ch/'+m[0] ]),
            # generic 'scratch' volume from a project, mounted in homedir (project name not used)
            ('^s\.(\w+)\.([^.]+)\.(\d+)$',
             lambda m: ['/afs/cern.ch/user/'+m[1][0]+'/'+m[1]+'/scratch'+m[2],
                        '/afs/cern.ch/user/'+m[1][0]+'/'+m[1]+'/w'+m[2],
                    ]),
            # generic 'user' volume from a project, mounted in homedir (project name not used)
            ('^u\.(\w+)\.([^.]+)\.(\d+)$',
             lambda m: ['/afs/cern.ch/user/'+m[1][0]+'/'+m[1]+'/w'+m[2] ])]

        candidates_mounts = []
        for volpat, pathlambda in guess_volpat2mount:
            m = re.search(volpat, self.name)
            if m:
                candidates_mounts += pathlambda(m.groups())  # pathlambda returns array
        return candidates_mounts

    def get_verified_mountpoints(self, candidates_mounts=[]):
        if self.mounts:
            if self.mounts[0] == 'CACHED_NO_MOUNTS':
                return []
            else: 
                return self.mounts

        candidates_mounts += self.get_volscan_mountpoints()
        candidates_mounts += self.get_guessed_mountpoints()
        candidates_mounts += self._initial_mounts
        candidates_mounts = list(set(candidates_mounts)) # uniqify
        
        found_mounts = []
        for candidate in candidates_mounts:
            try:
                out = subprocess.check_output(['fs',
                                               'lsmount',
                                               '-dir',
                                               candidate],
                                              stderr=subprocess.STDOUT,
                                              universal_newlines=True)
                m = re.search("'([^']+)' is a mount point for volume '#(\S+)'", out)
                if m:
                    if m.group(1) == candidate and\
                       m.group(2) == self.name:
                        found_mounts.append(candidate)
            except  subprocess.CalledProcessError:
                pass
        self.mounts = list ( set(found_mounts) | (set(self.mounts)))  # merge new with existing(?) mounts
        return found_mounts

    def is_unmounted(self):
        return not bool(self.get_verified_mountpoints())
        
