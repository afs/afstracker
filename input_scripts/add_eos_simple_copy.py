#!/usr/bin/python3

import site
site.addsitedir('..')

from datetime import datetime, timedelta
import afstracker.models as am
from argparse import ArgumentParser
import sys
import re

workflow_name='eos_simple_copy'
workflow_state='initial'

initial_wait = 60
if __name__ == "__main__":
    parser = ArgumentParser(usage="add a volume to be copied to EOS. Don't change on AFS")
    parser.add_argument("--debug",
                        help="more verbose output",
                        action='store_true',
                    )
    parser.add_argument("--waitsecs",
                        type=int,
                        help="how long [seconds] to wait between actions for a single volume",
                    )
    # no --volscan - does not make sense, need one-to-one correspondance for AFS+EOS
    parser.add_argument("--afssource",
                        required=False,
                        help="the AFS source directory",
                    )
    parser.add_argument("--eosdest",
                        required=True,
                        help="the EOS destination directory",
                    )
    parser.add_argument("--volname",
                        required=False,
                        help="AFS volume name",
                    )
    parser.add_argument("--owner",
                        required=True,
                        help="service account that will own the data in EOS",
                    )
    parser.add_argument("--project",
                        required=True,
                        help="AFS project that had allocated this space",
                    )
    args = parser.parse_args()

    now = datetime.now()

    next = now + timedelta(seconds=int(initial_wait))

    if args.owner.lower() != args.owner:
        print("Error: owner needs to be lowercase, otherwise will not match LDAP")
        sys.exit(1)

    from afsutil.phonebook import phonebook as Phonebook
    gid = Phonebook.get_gid(args.owner)
    if not gid:
        # cannot runuser
        print("Error: owner needs to exist in LDAP and have a GID")
        sys.exit(1)

    if not ( args.owner and args.afssource and args.eosdest and args.project):
        print("Error: need owner, AFS mountpoint, EOS destination, and volume name")
        sys.exit(1)

    if not args.afssource.startswith('/afs/cern.ch/'):
        print("Error: AFS source does not start with '/afs/cern.ch/'")
        sys.exit(1)

    if not args.eosdest.startswith('/eos/'):
        print("Error: EOS destination does not start with '/eos/'")
        sys.exit(1)

    if  args.eosdest.startswith('/eos/user/') or args.eosdest.startswith('/eos/project/'):
        print("Error: EOS destination for USER, PROJECT needs to be directly to /eos/{user|project}-LETTER, not redirector")
        sys.exit(1)

    if not args.volname:
        # not actually used but required for DB, lets make up something..
        # based on the convention that the project name is actually part of the path
        g=re.match('(^/afs/cern.ch/.*/'+ args.project +'/)' , args.afssource)
        maxvolnamelength=15
        if g:
            subdir = args.afssource[len(g.groups()[0]):]
            sanitized_subdir= re.sub('\W', '', subdir)
            volname = 'F.'+args.project+'.'+sanitized_subdir  # 'F'ake volname
            volname=volname[: maxvolnamelength]
        else:
            sanitized_path= re.sub('\W', '', args.afssource)
            volname = 'F.' + args.project + '.'
            volname = volname + sanitized_path[-( maxvolnamelength - len(volname) ):]
        args.volname = volname
    dbvol = am.VolumeState(volname=args.volname,
                           workflow_name = workflow_name,
                           workflow_state = workflow_state,
                           workflow_since = now,
                           workflow_next = next,
                           workflow_defaultnextsec = args.waitsecs,
                           pathname = args.afssource,
                           destname = args.eosdest,
                           workflow_params = args.project,
                           owner = args.owner,
                       )
    am.db.session.add(dbvol)
    am.db.session.commit()
