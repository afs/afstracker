#!/usr/bin/python3
#
# copy a AFS mountpoint+subtree to EOS - and then stop. no further action.
## size limits, submounts need to be resolved 'before'
##
import os
import socket
from datetime import datetime, timedelta
import time
import random
import re
import subprocess
import sys

import afsutil.volinfo as Volinfo

default_wait = timedelta(days=30)
max_sleep = 5*60

this_workflow = 'eos_simple_copy'
this_host = socket.gethostname()
this_process = os.getpid()
soft_exit_file = '/var/run/'+this_workflow+'.soft_exit'

class WarnNothingToDo (subprocess.CalledProcessError):
    pass

def soft_exit_perhaps(log):
    if os.path.exists(soft_exit_file):
        log.info("soft exit for workflow processor {}".format(this_workflow))
        try:
            os.remove(soft_exit_file)
        except OSError:
            pass
        sys.exit(33)  # error, so that systemd restarts us

def run_periodic(log, args, am):
    cur_sleep = 0
    log.info("workflow processor for {} started".format(this_workflow))
    if not Volinfo.Volinfo('root.cell'):
        log.error('error on startup - cannot look at "root.cell", bailing out')
        sys.exit(1)

    while True:
        soft_exit_perhaps(log)
        now = datetime.now()
        am.db.session.commit()  # to get fresh objects
        volstate = am.VolumeState.query.filter(am.VolumeState.workflow_name == this_workflow,
                                               am.VolumeState.workflow_activehost == None,
                                               am.VolumeState.workflow_next <= now).first()
        if not volstate:
            if args.nowaitloop:
                return

            cur_sleep = min(max_sleep, cur_sleep * 2) + random.randint(0, 5)
            log.debug('no data found, sleeping for {}secs'.format(cur_sleep))
            time.sleep(cur_sleep)
            continue

        cur_sleep = 0

        # lock the record for us. Need to push to DB, and check for races
        identifier = ':'.join([this_host, str(this_process)])
        volname = volstate.volname # might be gone afterwards..
        volstate.workflow_activehost = identifier
        am.db.session.commit()
        volstate = am.VolumeState.query.filter(am.VolumeState.workflow_name == this_workflow,
                                               am.VolumeState.volname == volname,
                                               am.VolumeState.workflow_activehost == identifier).first()
        if not volstate:
            log.warning("could not lock record for {}".format(volname))
            continue
        log.debug("record for {} now locked by {}.".format(volstate.volname, identifier))

        project = volstate.workflow_params
        log.info("looking at volume '{}' from project '{}' in state '{}'".format(volstate.volname, project, volstate.workflow_state))

        next_actiondate = now + default_wait
        if volstate.workflow_defaultnextsec:
            next_actiondate = now + timedelta(seconds=volstate.workflow_defaultnextsec)
        if args.waitsecs >= 0:
            next_actiondate = now + timedelta(seconds=int(args.waitsecs))

        # see whether there are still outstanding workflows for any still-mounted subdirectories.
        # if so, push back and wait (for a few hours)
        volstate_child = am.VolumeState.query.filter(am.VolumeState.workflow_name == this_workflow,
                                                     am.VolumeState.pathname.like(volstate.pathname+'/%'),
                                                     ~am.VolumeState.workflow_state.in_(['delete_subtree_after_wait','unmount_wait'])).first()
        if volstate_child:
            log.info("{} is blocked by '{}' in state '{}, will wait'".format(volstate.volname, volstate_child.volname, volstate_child.workflow_state))
            volstate.workflow_next = now + timedelta(seconds=int(args.waitblocksecs))
            volstate.workflow_activehost = None
            continue

        if volstate.workflow_state == 'initial':
            if not volstate.destname or not volstate.destname.startswith('/eos'):
                volstate.workflow_state = 'ERROR:' + volstate.workflow_state
                volstate.workflow_next = None
                log.error('destinaton {} is not on EOS for {}'.format(str(volstate.destname), volstate.volname))
                continue

            log.debug("volume '{}' will be copied as {} to {} .".format(volstate.volname, str(volstate.owner), volstate.destname))
            action_copy_data_to_eos(log=log, args=args, volstate=volstate, next_actiondate=next_actiondate)
            # goes to ERROR or 'copy_done'
        elif ( volstate.workflow_state == 'copy_done'):
            log.info("INFO: copy for {} to {} done. Over.".format(volstate.volname, volstate.destname ))
            am.db.session.delete(volstate)
            continue
        else:
            assert(False),"unexpected workflow state {} for volume {}".format(volstate.workflow_state, volstate.volname)


        # push back changed data for this volume to DB
        am.db.session.commit()


#################################################

# main function - no check for mountpoints, can do in one go. Else submit several entries
def action_copy_data_to_eos (log, args, volstate, next_actiondate):
    #project = volstate.workflow_params
    eosuser = volstate.owner
    afsroot = str(volstate.pathname) # mark as non-unicode, we might append random junk later
    eospath = str(volstate.destname)
    step = 'initializing'
    out = ''
    groupname = None
    exclusion_relative_dirs = []
    try:
        import pwd
        import grp
        me = eosuser
        if not me:
            me = os.environ.get('USER')  # don't want to set 'eosuser' since this mean we'll use 'runuser' later
        gid=pwd.getpwnam(me).pw_gid
        groupname=grp.getgrgid(gid).gr_name
    except (KeyError):
        pass

    try:
        # quick checks for 'nothing to do' - no errors, will finish ok
        step='checking AFS source'
        if not os.path.exists(afsroot):
            raise WarnNothingToDo('AFS source directory {} does not exist'.format(afsroot))
        if not os.path.isdir(afsroot):
            raise WarnNothingToDo('AFS source  {} is not a directory'.format(afsroot))
        if not os.listdir(afsroot):
            raise WarnNothingToDo('AFS source directory {} is empty'.format(afsroot))

        # fixup files without permissions (as these cause rsync to fail). Slow.
        # assume that we have none or few
        step="finding unreadable files"
        find_cmd = ['find', afsroot]
        if exclusion_relative_dirs:
            find_cmd.append('(')
            for arg in exclusion_relative_dirs:
                find_cmd += ['-path', arg, '-prune', '-o']
            find_cmd[-1] = ')'
        find_cmd += ['-type', 'f', '!', '-perm', '-400', '-print0']
        out = subprocess.check_output(find_cmd, stderr=subprocess.STDOUT,
                                      universal_newlines=True)
        step="fixing unreadable files"
        for unreadable in out.split('\000'):
            if unreadable:
                subprocess.check_call(['chmod', 'u+r', unreadable], stderr=subprocess.STDOUT)
                log.warning("made file '{}' readable".format(unreadable))

        # create target directory, OK if already-existing
        # Pre-check should trigger the mount, even if it fails...
        if not os.path.isdir(eospath):
            step="creating EOS target directory"
            mkdir_cmd = ['mkdir', '--mode=0700', '--parent', eospath]
            if eosuser:
                mkdir_cmd = ['runuser', eosuser, '-c', ' '.join(mkdir_cmd)]
            out = subprocess.check_output(mkdir_cmd, stderr=subprocess.STDOUT,
                                          universal_newlines=True)

        # actual data move
        # we remove 'G'roup and 'O'ther permissions, since AFS didnt care but EOS would evaluate them.
        rsync_cmd = ['rsync',
                     '--recursive', '--links', '--perms', '--chmod=go=', '--times', '--whole-file',
                     '--update', '--quiet', '--numeric-ids',
                     '--one-file-system', '--stats', '--timeout=1800']
        rsync_cmd += [afsroot+'/', eospath+'/']
        if eosuser:
            rsync_cmd = ['runuser', eosuser, '-c', ' '.join(rsync_cmd)]

        # copy the data, first attempt
        step="copying the data"
        try:
            out = subprocess.check_output(rsync_cmd, stderr=subprocess.STDOUT,
                                          universal_newlines=True)
        except subprocess.CalledProcessError as e:
        # copy the data, second attempt
            if 'some files/attrs were not transferred' in e.output:
                # workaround for temporary read errors on AFS
                step="copying the data, second attempt"
                out = subprocess.check_output(rsync_cmd, stderr=subprocess.STDOUT,
                                              universal_newlines=True)
            else:
                raise

        log.info("data copy for {} from {} to {} done".format(volstate.volname, afsroot, eospath))

        # ACL copy for directories
        # "system:anyuser" or "cern:nodes" "rl" becomes UNIX "Other" read
        # "cern:GROUP": if owning 'eosuser' is in GROUP,  "rl" becomes "Group" read
        # try: if 'webserver:afs rl', set attribs on EOS? No.

        # AFS prevents access to hidden-but-open child dirs. Don't open up anon read by accident?
        
        if groupname:
            afscerngroup = 'cern:'+groupname
            all_non_anon_read_dirs = []
            for afspath, _, _ in os.walk(afsroot, topdown=True):  # afspath is absolute path on AFS
                group_read = group_write = other_read = False
                anon_read = True
                for d in all_non_anon_read_dirs:   # this could become expensive if we have many leaf nodes with disabled access?
                    if afspath.startswith(d):
                        anon_read = False
                fs_la_cmd=['fs','listacl','-path', afspath]
                out = subprocess.check_output(fs_la_cmd, stderr=subprocess.STDOUT,
                                              universal_newlines=True)
                if re.search(afscerngroup+' rl', out):
                    group_read=True
                if re.search(afscerngroup+' .*idw', out):
                    group_write=True
                if anon_read:   # parent tree was not non-readable
                    if re.search('system:anyuser rl', out):
                        other_read=True
                    if re.search('cern:nodes rl', out):
                        other_read=True
                    if not other_read and not re.search('system:anyuser l', out):
                        log.debug('permissions: no anon read below {}'.format(afspath))
                        anon_read=False
                        all_non_anon_read_dirs.append(afspath)

                #if re.search('webserver:afs rl', out):
                #    web_read=True
                log.debug('permission: {} anon_read={},other_read={},group_read={},group_write={}'.format(
                    afspath, anon_read, other_read, group_read, group_write))
                perm=None
                if group_read and group_write:
                    perm='g+rwX'
                elif group_read:
                    perm='g+rX'
                elif group_write:
                    perm='g+wX'

                if other_read:
                    if perm:
                        perm = perm + ',o+rX'
                    else:
                        perm = 'g+rX,o+rX'  # give group also access if anon has access
                if perm:                   
                    eosdir = eospath + afspath[len(afsroot):]    # make into absolute path on EOS
                    chmod_cmd = ['chmod', '--silent', perm, eosdir]
                    if eosuser:
                        chmod_cmd = ['runuser', eosuser, '-c', ' '.join(chmod_cmd)]
                    try:
                        log.debug('will set permissions on {} to {}'.format(eosdir,perm))
                        out = subprocess.check_output(chmod_cmd, stderr=subprocess.STDOUT,
                                                      universal_newlines=True)
                    except subprocess.CalledProcessError:
                        log.warning("failed to set UNIX permissions on {} to {}".format(eosdir, perm))
        log.info("group+other permission copy for {} from {} to {} done".format(volstate.volname, afsroot, eospath))


    except subprocess.CalledProcessError as e:
        volstate.workflow_state = 'ERROR:' + volstate.workflow_state
        volstate.workflow_next = None
        log.error('error while {} for {}: {}'.format(step, volstate.volname, e.output))
        return
    except WarnNothingToDo as e:
        log.warning('warn while {} for {}: {}'.format(step, volstate.volname, e.output))
        pass
    volstate.workflow_state = 'copy_done'
    #keep volstate.workflow_next, no need to delay
    volstate.workflow_activehost = None
    return



if __name__ == "__main__":
    usage = """Looks for work in the DB, copy subtree to EOS"""
    import logging
    import logging.handlers
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    from argparse import ArgumentParser 
    parser = ArgumentParser(usage=usage)
    parser.add_argument("--debug",
                        help="more verbose output",
                        action='store_true',
                    )
    parser.add_argument("--syslog",
                        help="also log to syslog",
                        action='store_true',
                    )
    parser.add_argument("--nowaitloop",
                        help="if set, do not wait for new data to arrive - just exit after processing all",
                        action='store_true',
                    )
    parser.add_argument("--waitsecs",
                        type = int,
                        default = -1,
                        help="how long [seconds] to wait between actions",
                    )
    parser.add_argument("--waitblocksecs",
                        type = int,
                        default = 7200,
                        help="how long [seconds] to wait when being blocked by a sub-volume",
                    )
    parser.add_argument("--ctaprefix",
                        default = '/eos/ctapublicdisk/archive/afsmigration/restricted',
                        help="where on CTA will the files end up",
                    )
    args = parser.parse_args()
    if args.debug:
        console = logging.StreamHandler(sys.stdout)
        console.setLevel(logging.DEBUG)
        logger.addHandler(console)
    else:
        import dblogger
        dblog = dblogger.DBHandler()
        dblog.setLevel(logging.INFO)
        logger.addHandler(dblog)

    if args.syslog:
        syslog = logging.handlers.SysLogHandler(address='/dev/log',
                                            facility=logging.handlers.SysLogHandler.LOG_DAEMON)
        syslog.setLevel(logging.INFO)
        logger.addHandler(syslog)
    
    import afstracker.models as am
    run_periodic(logger, args, am)

