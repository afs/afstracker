#!/usr/bin/python3

import site
import os
import tempfile
import sqlite3
import datetime
import unittest

import check_error_stuck

class TestCron(unittest.TestCase):
    dbfile = None
    class Args(object):
        def __init__(self):
            self.debug = False
            self.maxstagesecs = 86400

    def setUp(self):
        site.addsitedir('..')
        os.environ['PYTHONPATH'] = '..'
        if not TestCron.dbfile or not os.path.exists(TestCron.dbfile):
            _, TestCron.dbfile = tempfile.mkstemp(suffix='.sqlite', prefix='afstrack_cron_')
            os.environ['DATABASE_URL'] = 'sqlite:///'+ TestCron.dbfile
            from afstracker import db
            db.create_all()
            print("DEBUG: needed to recreate DB {}".format(TestCron.dbfile))
        self.conn = sqlite3.connect(TestCron.dbfile)

    def add_vol_to_db(self,
                      vol,
                      pathname=None,
                      workflow=None,
                      params='unset_replaceme',
                      activehost=None,
                      state=None,
                      nextdate=None,
                      user=None):
        if not pathname:
            workflow = '/dummy/the/pathname'
        if not workflow:
            workflow = 'dummy_theworkflow'
        if not user:
            user = 'dummy_theuser'
        if not nextdate:
            nextdate = "'"+str(datetime.datetime.now())+"'"
        curs = self.conn.cursor()
        curs.execute("""
    INSERT OR IGNORE INTO
    volume_state (volname,
        workflow_name,
        workflow_params,
        pathname,
        workflow_state,
        workflow_activehost,
        owner,
        workflow_defaultnextsec)
    VALUES (?,?,?,?,?,?,?,?)""",
                     [vol,
                      workflow,
                      params,
                      pathname,
                      state,
                      activehost,
                      user,
                      0])
        curs.execute("""
        UPDATE volume_state SET workflow_next = {} WHERE volname = '{}'""".format(
            str(nextdate), vol))

        self.conn.commit()
        curs.close()


class TestCheckErrorStuck(TestCron):
    def setUp(self):
        TestCron.setUp(self)
        self.args = TestCron.Args()
        import afstracker.models as am
        self.am=am

    def test_addvols(self):
        self.add_vol_to_db('test.volname_noerror', workflow = 'workflow_error', state = 'somethingelse')
        self.add_vol_to_db('test.volname_error', workflow = 'workflow_error', state = 'ERROR')
        self.add_vol_to_db("test.volname.stuck",
                           workflow = 'workflow_notstuck',
                           activehost = 'otherhost.cern.ch:1234',
                           nextdate = "DATETIME('now','-8 hours')" )
        self.add_vol_to_db("test.volname.stuck",
                           workflow = 'workflow_stuck',
                           activehost = 'otherhost.cern.ch:1234',
                           nextdate = "DATETIME('now','-2 days')" )

    def not_test_error_notfound(self):
        ret=check_error_stuck.check_error(self.args, self.am)
        self.assertFalse(ret) # empty

    def test_error_found(self):
        ret=check_error_stuck.check_error(self.args, self.am)
        import pdb
        pdb.set_trace()
        print(ret)
        self.assertTrue(len(ret) == 1)

    def not_test_stuck_notfound(self):
        ret=check_error_stuck.check_stuck(self.args, self.am)
        self.assertFalse(ret)

    def test_stuck_found(self):
        ret=check_error_stuck.check_stuck(self.args, self.am)
        self.assertTrue(len(ret) == 1)

if __name__ == '__main__':
    unittest.main()
