#!/usr/bin/python3

import unittest
from testworkflow import TestWorkflow

class TestBackupVolumes(TestWorkflow):
    def setUp(self):
        TestWorkflow.setUp(self)
        self.workflow = 'backup_volumes'

    # main entry point
    def run_backup_volumes_periodic(self):
        import logging
        import sys
        logger = logging.getLogger()
        logger.setLevel(logging.DEBUG)
        logger.handlers = []  # remove leftovers from previous runs
        console = logging.StreamHandler(sys.stderr)
        console.setLevel(logging.DEBUG)
        logger.addHandler(console)

        args = TestWorkflow.Args()
        args.nowaitloop=True
        args.debug=True
        args.noadmin=True
        import backup_volumes as mv
        import afstracker.models as am

        mv.run_periodic(logger, args, am)



    def test_add_other_to_db(self):
        self.add_vol_to_db("test.leaveme.other.notmyworkflow", workflow = 'other_workflow')
        self.add_vol_to_db("test.leaveme.other.notmyhost", activehost = 'otherhost.cern.ch:1234')
        self.add_vol_to_db("test.leaveme.other.notyet", nextdate = "DATETIME('now','+7 days')")
        
    def test_exist_mounted_primary(self):
        # should send mail
        testvol='user.iven.backup'
        mntdir='/afs/cern.ch/user/i/iven/tmp/userivenbackup'
        self.delete_vol_from_db_maybe(vol=testvol)
        self.volmount_if_missing(vol=testvol, mnt=mntdir)
        self.add_vol_to_db(vol=testvol, mountpoint=mntdir)

        self.run_backup_volumes_periodic()

        self.assertFalse(self.check_mountpoint_is_present(vol=testvol, mnt=mntdir))
        self.assertTrue(self.check_volume_is_present(vol=testvol))
        self.assertFalse(self.check_vol_is_in_db(vol=testvol))

        self.delete_vol_from_db_maybe(vol=testvol)
        self.volrmmount_if_present(vol=testvol, mnt=mntdir)

    def test_exist_mounted_service(self):
        # should send a mail
        testvol='user.iven.backup'
        mntdir='/afs/cern.ch/user/a/afsspad/tmp/userivenbackup'
        self.delete_vol_from_db_maybe(vol=testvol)
        self.volmount_if_missing(vol=testvol, mnt=mntdir)
        self.add_vol_to_db(vol=testvol, mountpoint=mntdir)

        self.run_backup_volumes_periodic()

        self.assertFalse(self.check_mountpoint_is_present(vol=testvol, mnt=mntdir))
        self.assertTrue(self.check_volume_is_present(vol=testvol))
        self.assertFalse(self.check_vol_is_in_db(vol=testvol))

        self.delete_vol_from_db_maybe(vol=testvol)
        self.volrmmount_if_present(vol=testvol, mnt=mntdir)

    def test_exist_mounted_project_givenproject(self):
        # should send a mail
        testvol='user.iven.backup'
        mntdir='/afs/cern.ch/project/afstest/tmp/userivenbackup'
        self.delete_vol_from_db_maybe(vol=testvol)

        self.volmount_if_missing(vol=testvol, mnt=mntdir)
        self.add_vol_to_db(vol=testvol, mountpoint=mntdir, params='afstest')

        self.run_backup_volumes_periodic()

        self.assertFalse(self.check_mountpoint_is_present(vol=testvol, mnt=mntdir))
        self.assertTrue(self.check_volume_is_present(vol=testvol))
        self.assertFalse(self.check_vol_is_in_db(vol=testvol))

        self.delete_vol_from_db_maybe(vol=testvol)
        self.volrmmount_if_present(vol=testvol, mnt=mntdir)

    def test_exist_mounted_project_guessproject(self):
        # should send a mail
        testvol='user.iven.backup'
        mntdir='/afs/cern.ch/project/afstest/tmp/userivenbackup2'
        self.delete_vol_from_db_maybe(vol=testvol)

        self.volmount_if_missing(vol=testvol, mnt=mntdir)
        self.add_vol_to_db(vol=testvol, mountpoint=mntdir, params=None)

        self.run_backup_volumes_periodic()

        self.assertFalse(self.check_mountpoint_is_present(vol=testvol, mnt=mntdir))
        self.assertTrue(self.check_volume_is_present(vol=testvol))
        self.assertFalse(self.check_vol_is_in_db(vol=testvol))

        self.delete_vol_from_db_maybe(vol=testvol)
        self.volrmmount_if_present(vol=testvol, mnt=mntdir)

    def test_exist_notmountpoint_(self):
        testvol='user.iven.backup'
        mntdir='/afs/cern.ch/user/j'   # no mountpoint here
        self.delete_vol_from_db_maybe(vol=testvol)

        self.add_vol_to_db(vol=testvol, mountpoint=mntdir)

        self.run_backup_volumes_periodic()

        self.assertFalse(self.check_vol_is_in_db(vol=testvol))  # not fatal error

        self.delete_vol_from_db_maybe(vol=testvol)

    def test_exist_wrongmountpoint_(self):
        testvol='user.iven.backup'
        mntdir='/afs/cern.ch/i/iven'   # mountpoint for 'user.iven'
        self.delete_vol_from_db_maybe(vol=testvol)

        self.add_vol_to_db(vol=testvol, mountpoint=mntdir)

        self.run_backup_volumes_periodic()

        self.assertFalse(self.check_vol_is_in_db(vol=testvol)) # not fatal error

        self.delete_vol_from_db_maybe(vol=testvol)

    def test_exist_mounted_in_ubackup(self):
        testvol='user.iven.backup'
        mntdir='/afs/cern.ch/ubackup/i/iven'
        self.delete_vol_from_db_maybe(vol=testvol)

        self.add_vol_to_db(vol=testvol, mountpoint=mntdir)

        self.assertRaises(AssertionError, self.run_backup_volumes_periodic)

        self.assertTrue(self.check_mountpoint_is_present(vol=testvol, mnt=mntdir))
        self.assertTrue(self.check_volume_is_present(vol=testvol))
        self.assertTrue(self.check_vol_is_in_db(vol=testvol, state='ERROR'))

        self.delete_vol_from_db_maybe(vol=testvol)

    def test_check_other_in_db(self):
        self.assertTrue(self.check_vol_is_in_db(vol="test.leaveme.other.notmyworkflow"))
        self.assertTrue(self.check_vol_is_in_db(vol="test.leaveme.other.notmyhost"))
        self.assertTrue(self.check_vol_is_in_db(vol="test.leaveme.other.notyet"))




if __name__ == '__main__':
    unittest.main()
