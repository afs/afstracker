#!/usr/bin/python3

import unittest
import subprocess
from . import projectutil

class TestProjectutil(unittest.TestCase):
  @classmethod
  def setUpClass(cls):
    try:
      subprocess.check_call(['pts', 'help'], stdout=subprocess.DEVNULL)
    except FileNotFoundError:
      raise unittest.SkipTest('this needs the "pts" command')

  def test_check_iven_in_afs(self):
    result = projectutil.Projectutil.afsproject_to_contacts(projectname='afs')
    self.assertIn('iven',result)

  def test_check_nonexisting(self):
    result = projectutil.Projectutil.afsproject_to_contacts(projectname='NoSuchProjectEverIHope')
    self.assertEqual(result,[])

  def test_check_empty(self):
    # at least currently 2019-03 no members
    result = projectutil.Projectutil.afsproject_to_contacts(projectname='services')
    self.assertEqual(result,[])

  def test_check_nopermissionss(self):
    # at least I have no permissions as myself to see the content..
    self.assertRaises(subprocess.CalledProcessError, projectutil.Projectutil.afsproject_to_contacts, projectname='compass')

if __name__ == '__main__':
    unittest.main()
