#!/usr/bin/python3
import subprocess

class phonebook(object):
    @classmethod
    def get_name_email(cls, account):
        data = cls.get_phonebook_data( account)
        return data['firstname'],data['surname'],data['email']
    @classmethod
    def get_gid(cls, account):
        data = cls.get_phonebook_data( account)
        try:
            return int(data['gid'])
        except TypeError:
            return None

    @classmethod
    def _uac_disabled(cls, uac):
        # flags per https://support.microsoft.com/en-us/help/305144
        ACCOUNTDISABLE=0x0002
        # LOCKOUT=0x0010
        # NORMAL_ACCOUNT=0x0200
        return bool(int(uac) & ACCOUNTDISABLE)

    @classmethod
    def is_disabled(cls, account):
        uac = cls.get_phonebook_data( account)['uac']
        return cls._uac_disabled(uac)

    @classmethod
    def get_accounttype(cls, account):
        accounttype = cls.get_phonebook_data(account)['type']
        assert(accounttype in ['Primary','Secondary','Service']), 'Unknown account type {} for user {}'.format(accounttype, account)
        return accounttype

    @classmethod
    def get_phonebook_data(cls, account):
        data = {}
        terse_args = ['firstname','surname','email','type','uac','uid','gid']
        # no 'flatten'..
        cmd_args = ['phonebook'] +\
                   [ x for arg in terse_args for x in ['--terse', arg] ] +\
                   [ '--login', account ]
        try:
            out = subprocess.check_output(cmd_args,
                                          universal_newlines=True)
            if out:
                data = dict(list(zip(terse_args, out.split(';'))))  # trailing ';' will get discarded
            else:
                data = dict(list(zip(terse_args, [None]*len(terse_args))))   # all keys present, always None as value
        except ValueError:
            pass
        except OSError as e:
            print("Error running 'phonebook':"+str(e))
        return data

    @classmethod
    def accounts_to_emails(cls, accounts):
        emails = {}
        for account in accounts:
            data = cls.get_phonebook_data(account)
            if not data or not data['email']:
                continue
            if cls._uac_disabled(data['uac']):
                continue
            emails[data['email']] = 1
        return list(emails.keys())
