#!/usr/bin/python3
#
# generic workflow for missing-but-mounted volumes (dangling mountpoints). No mails, silent remove.

import os
import socket
from datetime import datetime, timedelta
import time
import random
import sys

from argparse import ArgumentParser 
import subprocess
import logging
import logging.handlers
import dblogger

import afsutil.volinfo as Volinfo
from afsutil.volutil import Volutil

default_wait = timedelta(days=30)
override_wait = None

max_sleep = 5*60
this_workflow = 'missing_volumes'
this_host = socket.gethostname()
this_process = os.getpid() 
soft_exit_file = '/var/run/'+this_workflow+'.soft_exit'

def soft_exit_perhaps(log):
    if os.path.exists(soft_exit_file):
        log.info("soft exit for workflow processor {}".format(this_workflow))
        try:
            os.remove(soft_exit_file)
        except OSError:
            pass
        import sys
        sys.exit(33)  # error, so that systemd restarts us

def run_periodic(log, args, am):
    cur_sleep = 0
    log.info("workflow processor for {} started".format(this_workflow))
    while True:
        soft_exit_perhaps(log)
        now = datetime.now()
        am.db.session.commit()  # to get fresh objects
        volstate = am.VolumeState.query.filter(am.VolumeState.workflow_name == this_workflow,
                                               am.VolumeState.workflow_activehost == None,
                                               am.VolumeState.workflow_next <= now).first()
        if not volstate:
            if args.nowaitloop:
                return

            cur_sleep = min(max_sleep, cur_sleep * 2) + random.randint(0, 5)
            log.debug('no data found, sleeping for {}secs'.format(cur_sleep))
            time.sleep(cur_sleep)
            continue

        cur_sleep = 0

        # lock the record for us. Need to push to DB, and check for races
        identifier = ':'.join([this_host, str(this_process)])
        volname = volstate.volname # might be gone afterwards..
        volstate.workflow_activehost = identifier
        am.db.session.commit()
        volstate = am.VolumeState.query.filter(am.VolumeState.workflow_name == this_workflow,
                                               am.VolumeState.volname == volname,
                                               am.VolumeState.workflow_activehost == identifier).first()
        if not volstate:
            log.warning("could not lock record for {}".format(volname))
            continue
        log.debug("record for {} now locked by {}.".format(volstate.volname, identifier))

        logextra={'volname': volstate.volname}  # used by DB logging
        log.info("looking at volume '{}' in state '{}' mounted (perhaps) on '{}'".format(volstate.volname, volstate.workflow_state, volstate.pathname), extra=logextra)

      
        v = Volinfo.Volinfo(volstate.volname, initial_mountpoints=[volstate.pathname])

        # terminal state
        if v:
            log.error("volume {} was still found in VLDB. Not supposed to happen".format(volstate.volname))
            volstate.workflow_state = 'ERROR'
            volstate.workflow_next = None
            continue


        # state machine is super-simple: remove mountpoint.
        if volstate.workflow_state == 'initial':
            try:
                if Volutil.check_mount(dirname=volstate.pathname, volname=volstate.volname):
                    Volutil.delete_mount(volstate.pathname)
                    log.info("removed mountpoint '{}' for '{}'.".format(volstate.pathname, volstate.volname))
                else:
                    log.warning("did not find mountpoint '{}' for '{}'.".format(volstate.pathname, volstate.volname))
                    
                am.db.session.delete(volstate)
                log.info("removed tracking entry for volume '{}'.".format(volstate.volname))
            except subprocess.CalledProcessError as e:
                volstate.workflow_state = 'ERROR'
                volstate.workflow_next = None
                log.error('error while removing mountpoint {} for {}: {}'.format(volstate.pathname, volstate.volname, e.output))
        else:
            assert(False),"unexpected workflow state {} for volume {}".format(volstate.workflow_state, volstate.volname)


        # push back changed data for this volume to DB
        am.db.session.commit()

if __name__ == "__main__":
    usage = """Look at DB, figure out next per-volume actions for scratch spaces."""
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    parser = ArgumentParser(usage=usage)
    parser.add_argument("--debug",
                        help="more verbose output",
                        action='store_true',
                    )
    parser.add_argument("--syslog",
                        help="also log to syslog",
                        action='store_true',
                    )
    parser.add_argument("--nowaitloop",
                        help="if set, do not wait for new data to arrive - just exit after processing all",
                        action='store_true',
                    )
    parser.add_argument("--noaction",
                        help="suppress sending mails, do not modify DB",
                        action='store_true',
                    )
    parser.add_argument("--waitsecs",
                        type=int,
                        help="how long [seconds] to wait between actions for a single volume",
                    )
    parser.add_argument("--noadmin",
                        action='store_true',
                        help="skip steps that need full AFS admin powers, beyond 'afs_admin'",
                    )
    args = parser.parse_args()
    if args.debug:
        console = logging.StreamHandler(sys.stdout)
        console.setLevel(logging.DEBUG)
        logger.addHandler(console)
    else:
        dblog = dblogger.DBHandler()
        dblog.setLevel(logging.INFO)
        logger.addHandler(dblog)

    if args.syslog:
        syslog = logging.handlers.SysLogHandler(address='/dev/log',
                                            facility=logging.handlers.SysLogHandler.LOG_DAEMON)
        syslog.setLevel(logging.INFO)
        logger.addHandler(syslog)
    
    if args.waitsecs:
        override_wait = timedelta(seconds=int(args.waitsecs))

    import afstracker.models as am
    run_periodic(logger, args, am)

