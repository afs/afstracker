import os
import sys
import yaml
basedir = os.path.abspath(os.path.dirname(__file__))
execdir=  os.path.dirname(os.path.realpath(sys.argv[0]))

class Config(object):
    #SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'afstracker.db')
    SQLALCHEMY_DATABASE_URI = 'ERROR - set me to something'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ECHO = False

    if os.environ.get('DATABASE_URL'):
        SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')
    try:
        configfile = yaml.safe_load(open(execdir+"/config.yml"))
        if 'SQLALCHEMY_DATABASE_URI' in configfile:
            SQLALCHEMY_DATABASE_URI = configfile['SQLALCHEMY_DATABASE_URI']
        if 'SQLALCHEMY_TRACK_MODIFICATIONS' in configfile:
            SQLALCHEMY_TRACK_MODIFICATIONS = configfile['SQLALCHEMY_TRACK_MODIFICATIONS']
        if 'SQLALCHEMY_ECHO'  in configfile:
            SQLALCHEMY_ECHO =  configfile['SQLALCHEMY_ECHO']

    except IOError:
        pass

