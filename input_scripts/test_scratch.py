#!/usr/bin/python3

from datetime import datetime
import unittest

class TestPersonalScratchAdd(unittest.TestCase):
    def test_add(self):
        self.skipTest("need to run manually, see README")

def manual_testing():
    import afstracker.models as am
    from sqlalchemy import exists
    testvol = 's.pdpgroup.iven.0'

    (ret, ), = am.db.session.query(exists().where(am.VolumeState.volname==testvol))
    if ret:
        print("# already present:"+str(ret))
    else:
        now = datetime.now()
        test_scratch_vol = am.VolumeState(volname=testvol,
                                          workflow_name='personal_scratch.py',
                                          workflow_params = 'pdpgroup',
                                          workflow_state = 'new',
                                          workflow_since = now,
                                          workflow_next = None,
                                          pathname = '/afs/cern.ch/user/i/iven/scratch0-old',
                                          owner = 'iven',
        )
        am.db.session.add(test_scratch_vol)
        am.db.session.commit()
        print("Added "+testvol)

if __name__ == "__main__":
    manual_testing()
